package cn.shoptnt.framework.openapi;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * openAPI 公共配置
 *
 * @author gy
 * @version 1.0
 * @sinc 7.x
 * @date 2022年12月09日 13:12
 */
@Configuration
public class OpenApiCommonConfig {


    @Bean
    public OpenAPI customOpenBaseAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("Api文档")
                        .version("7.3.0")
                        .description("Api接口描述文档")
                        .termsOfService("https://www.shoptnt.cn")
                        .license(new License()
                                .name("shoptnt")
                                .url("service@shoptnt.cn")));

    }
}
