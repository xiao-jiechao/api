/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数据库操作运行期异常
 * @author 妙贤
 * 2010-1-6下午06:16:47
 */
public class DBRuntimeException extends RuntimeException {

 
	private static final long serialVersionUID = -368646349014580765L;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	
	public DBRuntimeException(String message){
		super(message);
	}
	public DBRuntimeException(Exception e,String sql) {
		
		super("数据库运行期异常");
		e.printStackTrace();
		logger.error("数据库运行期异常，相关sql语句为"+ sql);
	}
	
	
	public DBRuntimeException(String message,String sql) {
		
		super(message);
		logger.error(message+"，相关sql语句为"+ sql);
	}
	
	
}
