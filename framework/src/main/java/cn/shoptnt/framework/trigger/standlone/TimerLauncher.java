package cn.shoptnt.framework.trigger.standlone;

import cn.shoptnt.framework.trigger.Interface.TimerTaskData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.TimerTask;

/**
 * 延迟任务持久化加载
 *
 * @author kingapex
 * @version 1.0
 * @data 2022/11/4 16:25
 **/
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class TimerLauncher implements ApplicationRunner {

    private  final Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private TimerTaskDataSource timerTaskDataSource;

    @Autowired
    private StandaloneTrigger standaloneTrigger;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.debug("加载延迟任务");
        boolean deleteRes = timerTaskDataSource.deleteInvalid();
        if(deleteRes) {
            List<TimerTaskData> enableTasks = timerTaskDataSource.getEnableTasks();

            //触发所有的可用延迟任务
            enableTasks.stream().allMatch(this::trigger);
        }
        logger.debug("加载延迟任务完毕");
    }

    /**
     * 根据一个持久化数据触发任务
     * @param timerTaskData 任务持久化数据
     * @return 是否触发成功
     */
    private boolean trigger(TimerTaskData timerTaskData) {

        TimerTask task = timerTaskData.convertTimerTask();

        try {
            standaloneTrigger.trigger(task, timerTaskData.getTriggerTime(), timerTaskData.getUniqueKey());
            logger.debug("添加延迟任务{}", timerTaskData);
            return true;
        } catch (Exception e) {
            logger.error("触发延迟任务{}出错", timerTaskData.getUniqueKey(),e);
            return false;
        }
    }


}
