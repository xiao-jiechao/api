/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.message.broadcast.redismq;

import cn.shoptnt.framework.message.broadcast.BroadcastMessageReceiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;

import java.util.List;

/**
 * redis敏感词消息订阅
 *
 * @author fk
 * @version 2.0
 * @since 7.1.5
 * 2019-09-07 18：00
 */
@Configuration
@ConditionalOnProperty(value = "shoptnt.runmode",  havingValue = "cluster")
public class RedisListener {

    @Autowired(required = false)
    private List<BroadcastMessageReceiver> receiverList;

    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        if (receiverList != null) {
            for (BroadcastMessageReceiver receiver : receiverList) {
                container.addMessageListener(listenerAdapter(receiver), new PatternTopic(receiver.getChannelName()));
            }
        }

        return container;
    }


    /**
     * 绑定消息监听者和接收监听的方法
     */
    private MessageListenerAdapter listenerAdapter(BroadcastMessageReceiver receiver) {
        MessageListenerAdapter listenerAdapter = new MessageListenerAdapter(receiver, "receiveMsg");
        JdkSerializationRedisSerializer jdkSerializationRedisSerializer = new JdkSerializationRedisSerializer();

        listenerAdapter.setSerializer(jdkSerializationRedisSerializer);
        listenerAdapter.afterPropertiesSet();
        return listenerAdapter;
    }

}
