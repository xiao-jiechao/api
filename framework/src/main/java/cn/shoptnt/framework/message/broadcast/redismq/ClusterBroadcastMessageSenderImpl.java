package cn.shoptnt.framework.message.broadcast.redismq;

import cn.shoptnt.framework.message.broadcast.BroadcastMessage;
import cn.shoptnt.framework.message.broadcast.BroadcastMessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * 集群版的广播消息实现
 * @author kingapex
 * @version 1.0
 * @data 2022/10/24 16:50
 **/
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "cluster")
public class ClusterBroadcastMessageSenderImpl implements BroadcastMessageSender {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void send(BroadcastMessage message) {
        redisTemplate.convertAndSend(message.getChannel(), message);
    }
}
