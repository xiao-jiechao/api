package cn.shoptnt.framework.message.broadcast.standalone;

import cn.shoptnt.framework.message.broadcast.BroadcastMessage;
import cn.shoptnt.framework.message.broadcast.BroadcastMessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * 单机版的广播消息实现
 * @author kingaepx
 * @version 1.0
 * @data 2022/10/24 16:50
 **/
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class SaBroadcastMessageSenderImpl implements BroadcastMessageSender {

    @Autowired
    private ApplicationEventPublisher publisher;

    @Override
    public void send(BroadcastMessage message) {
        publisher.publishEvent(message);
    }
}
