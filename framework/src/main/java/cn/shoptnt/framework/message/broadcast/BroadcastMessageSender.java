package cn.shoptnt.framework.message.broadcast;

/**
 * 广播消息发送
 * @author miaoxian
 * @version 1.0
 * @data 2022/10/24 16:25
 **/
public interface BroadcastMessageSender {

    void send(BroadcastMessage message);

}
