package cn.shoptnt.framework.lock.cluster;

import cn.shoptnt.framework.lock.Lock;
import cn.shoptnt.framework.lock.LockFactory;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 集群模式下，分布式锁的实现
 * @author kingapex
 * @version 1.0
 * @data 2022/10/21 19:00
 **/
@Service
@ConditionalOnProperty(value = "shoptnt.runmode",  havingValue = "cluster")
public class RedissonLockFactory implements LockFactory {

    @Autowired
    private RedissonClient redissonClient;

    @Override
    public Lock getLock(String lockName) {
        RLock rLock= redissonClient.getLock(lockName);
        return new RedissonLock(rLock);
    }

}
