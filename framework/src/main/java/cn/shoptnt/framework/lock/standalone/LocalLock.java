package cn.shoptnt.framework.lock.standalone;


import cn.shoptnt.framework.lock.Lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 本地锁实现
 * 通过 ReentrantLock 实现本地锁
 * @author kingapex
 * @version 1.0
 * @data 2022/10/24 15:14
 **/

public class LocalLock implements Lock {


    /**
     * 通过 ReentrantLock 实现本地锁
     */
    private ReentrantLock reentrantLock;

    public LocalLock(ReentrantLock reentrantLock) {
        this.reentrantLock = reentrantLock;
    }

    @Override
    public void lock() {
        this.reentrantLock.lock();
    }

    @Override
    public void unlock() {
        this.reentrantLock.unlock();
    }
}
