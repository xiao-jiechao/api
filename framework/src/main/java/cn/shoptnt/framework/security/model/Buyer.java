/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.security.model;

/**
 * Created by 妙贤 on 2018/3/11.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/11
 */
public class Buyer extends User {


    /**
     * 定义买家的角色
     */
    public Buyer() {
        this.add(Role.BUYER.name());
    }


}
