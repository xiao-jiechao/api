/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.base;

import cn.shoptnt.model.pagedata.Article;
import cn.shoptnt.model.pagedata.vo.ArticleDetail;
import cn.shoptnt.service.pagedata.ArticleManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

/**
 * 文章控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-12 10:43:18
 */
@RestController
@RequestMapping("/base/pages")
@Tag(name = "文章相关API")
public class ArticleBuyerController {

    @Autowired
    private ArticleManager articleManager;

    //, allowableValues = "REGISTRATION_AGREEMENT,COOPERATION_AGREEMENT,CONTACT_INFORMATION,GROUP_BUY_AGREEMENT,OTHER"
    @Operation(summary = "查询某个位置的文章列表")
    @Parameters({
            @Parameter(name = "position", description = "文章显示位置:注册协议，入驻协议，平台联系方式，团购活动协议,其他", required = true, in = ParameterIn.QUERY)
    })
    @GetMapping("/articles")
    public List<Article> list(String position) {

        return this.articleManager.listByPosition(position);
    }

    @GetMapping(value = "/articles/{id}")
    @Operation(summary = "查询一个文章")
    @Parameters({
            @Parameter(name = "id", description = "要查询的文章主键", required = true, in = ParameterIn.PATH)
    })
    public Article get(@PathVariable Long id) {

        Article article = this.articleManager.getModel(id);

        return article;
    }

    @Operation(summary = "查询某个位置的一个文章")
    @Parameters({
            @Parameter(name = "position", description = "文章显示位置,注册协议，入驻协议，平台联系方式，其他", required = true, in = ParameterIn.PATH)
    })
    @GetMapping("/{position}/articles")
    public Article getOne(@PathVariable String position) {

        List<Article> list = this.articleManager.listByPosition(position);
        return list.get(0);
    }

    @Operation(summary = "查询某个分类类型下的文章列表")
    @Parameters({
            @Parameter(name = "category_type", description = "分类类型,帮助中心，商城公告，固定位置，商城促销，其他", required = true, in = ParameterIn.PATH),
    })
    @GetMapping("/article-categories/{category_type}/articles")
    public List<Article> listByCategoryType(@PathVariable("category_type") String categoryType) {

        return this.articleManager.listByCategoryType(categoryType);
    }

}
