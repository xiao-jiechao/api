/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.configserver.resource;


import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.config.server.config.ConfigServerProperties;
import org.springframework.cloud.config.server.environment.SearchPathLocator;
import org.springframework.cloud.config.server.resource.ResourceRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


/**
 *
 * 2021/4/19  12:08
 * @author liuyulei
 * @version 1.0
 * @since  7.2.2
 **/

@Configuration
@EnableConfigurationProperties(ConfigServerProperties.class)
public class ResourceRepositoryNewConfiguration {

    @Bean
    @Primary
    public ResourceRepository resourceRepository(SearchPathLocator service) {
        return new GenericResourceNewRepository(service);
    }
}
