/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.trade;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyActiveVO;
import cn.shoptnt.model.promotion.pintuan.PinTuanGoodsVO;
import cn.shoptnt.model.promotion.pintuan.Pintuan;
import cn.shoptnt.model.promotion.pintuan.PintuanGoodsDO;
import cn.shoptnt.model.promotion.pintuan.PintuanQueryParam;
import cn.shoptnt.service.trade.pintuan.PintuanGoodsManager;
import cn.shoptnt.service.trade.pintuan.PintuanManager;
import cn.shoptnt.framework.context.user.UserContext;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;


/**
 * 拼团卖家控制器
 *
 * @author liushuai
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2019/1/28 下午2:41
 */
@RestController
@RequestMapping("/seller/promotion/pintuan")
@Tag(name = "拼团")
@Validated
public class PintuanSellerController {

    @Autowired
    private PintuanManager pintuanManager;

    @Autowired
    private PintuanGoodsManager pintuanGoodsManager;

    @Operation(summary = "查询活动表列")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY),
            @Parameter(name = "name", description = "拼团活动名称",   in = ParameterIn.QUERY),
            @Parameter(name = "status", description = "拼团活动名称",   in = ParameterIn.QUERY, example = "WAIT:待开始，UNDERWAY：进行中，END：已结束"),
            @Parameter(name	= "start_time",	description =	"拼团日期-开始日期", 	in = ParameterIn.QUERY),
            @Parameter(name	= "end_time",	description =	"拼团日期-结束日期", 	in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String name,
                        @Parameter(hidden = true) String status, @Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {
        PintuanQueryParam param = new PintuanQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setName(name);
        param.setSellerId(UserContext.getSeller().getSellerId());
        param.setStatus(status);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        WebPage<GroupbuyActiveVO> page = this.pintuanManager.list(param);
        return page;
    }


    @Operation(summary = "添加活动")
    @PostMapping
    public Pintuan add(@Valid Pintuan pintuan) {
        return this.pintuanManager.add(pintuan);
    }


    @Operation(summary = "修改活动参与的商品")
    @PostMapping("/goods/{id}")
    @Parameters({
            @Parameter(name = "id", description = "活动ID", required = true,  in = ParameterIn.PATH)
    })
    public void savePromotionGoods(@RequestBody @Valid List<PintuanGoodsDO> pintuanGoodsDOS, @Parameter(hidden = true) @PathVariable Long id) {
        this.pintuanGoodsManager.save(id, pintuanGoodsDOS);
    }


    @Operation(summary = "获取活动参与的商品")
    @GetMapping("/goods/{id}")
    @Parameters({
            @Parameter(name = "id", description = "活动ID", required = true,  in = ParameterIn.PATH)
    })
    public List<PinTuanGoodsVO> promotionGoods(@Parameter(hidden = true) @PathVariable Long id) {
        return this.pintuanGoodsManager.all(id);
    }


    @Operation(summary = "修改活动")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    @PutMapping(value = "/{id}")
    public Pintuan edit(@Valid Pintuan activeDO, @PathVariable Long id) {
        this.pintuanManager.edit(activeDO, id);
        return activeDO;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除活动")
    @Parameters({
            @Parameter(name = "id", description = "要删除的活动表主键", required = true,  in = ParameterIn.PATH)
    })
    public void delete(@PathVariable Long id) {
        this.pintuanManager.delete(id);

    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个活动")
    @Parameters({
            @Parameter(name = "id", description = "要查询的活动表主键", required = true,  in = ParameterIn.PATH)
    })
    public Pintuan get(@PathVariable Long id) {
        return this.pintuanManager.getModel(id);
    }


}
