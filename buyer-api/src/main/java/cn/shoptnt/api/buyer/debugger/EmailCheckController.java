/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.debugger;

import cn.shoptnt.model.base.vo.EmailVO;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-04-24
 */
@RestController
@RequestMapping("/debugger/email")
@ConditionalOnProperty(value = "shoptnt.debugger", havingValue = "true")
public class EmailCheckController {

    @Autowired
    private DirectMessageSender messageSender;

    @GetMapping(value = "/test")
    public String test( String email ) {

        EmailVO emailVO = new EmailVO();
        emailVO.setContent("测试邮件");
        emailVO.setTitle("测试邮件");
        emailVO.setEmail(email);

        this.messageSender.send(emailVO);


        return "ok";
    }

}
