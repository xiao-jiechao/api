/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.framework.util.FileUtil;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统状态心跳功能
 * @Author shen
 * @Date 2021/6/30 15:49
 */

@Tag(name = "系统状态心跳功能")
@RestController
@RequestMapping("/buyer/webjars/system/buyer/state")
@Validated
public class MemberStateController {

    @GetMapping
    public String  live() {
        return  FileUtil.readFile("Heartbeat.ftl");
    }
}
