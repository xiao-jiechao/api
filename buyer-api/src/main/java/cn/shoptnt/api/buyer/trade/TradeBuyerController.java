/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.model.trade.cart.enums.CheckedWay;
import cn.shoptnt.model.trade.order.vo.OrderSettingVO;
import cn.shoptnt.model.trade.order.vo.TradeVO;
import cn.shoptnt.service.trade.order.TradeManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
/**
 * 交易控制器
 *
 * @author Snow create in 2018/5/8
 * @version v2.0
 * @since v7.0.0
 */
@Tag(name = "交易接口模块")
@RestController
@RequestMapping("/buyer/trade")
@Validated
public class TradeBuyerController {

    @Autowired
    @Qualifier("tradeManagerImpl")
    private TradeManager tradeManager;
    @Autowired
    private SettingClient settingClient;


    @Operation(summary = "创建交易")
    @PostMapping(value = "/create")
    @Parameters({
            @Parameter(name = "client", description = "客户端类型",   in = ParameterIn.QUERY),
            @Parameter(name = "way", description = "检查获取方式，购物车还是立即购买", required = true,   in = ParameterIn.QUERY),
    })
    public TradeVO create(String client, String way) {
        return this.tradeManager.createTrade(client, CheckedWay.valueOf(way));
    }

    @GetMapping("/setting")
    @Operation(summary = "获取订单任务设置信息")
    public OrderSettingVO getOrderSetting(){

        String json = this.settingClient.get(SettingGroup.TRADE);

        return JsonUtil.jsonToObject(json,OrderSettingVO.class);
    }



}
