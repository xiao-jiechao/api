/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.passport;

import cn.shoptnt.model.base.SceneType;
import cn.shoptnt.client.system.CaptchaClient;
import cn.shoptnt.client.system.SmsClient;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.service.member.ConnectManager;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.framework.exception.ServiceException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.Map;

/**
 * @author fk
 * @version v2.0
 * @Description: app信任登录相关API
 * @date 2018/11/6 10:28
 * @since v7.0.0
 */
@Tag(name = "app信任登录相关API")
@RestController
@RequestMapping("/buyer/passport")
@Validated
public class PassportConnectAppBuyerController {

    @Autowired
    private ConnectManager connectManager;
    @Autowired
    private SmsClient smsClient;
    @Autowired
    private CaptchaClient captchaClient;
    @Autowired
    private MemberManager memberManager;

    @Operation(summary = "获取app联合登录所需参数")
    @GetMapping("/connect/app/{type}/param")
    @Parameter(name = "type", description = "登录类型", required = true,   in = ParameterIn.PATH)
    public String getParam(@PathVariable("type") @Parameter(hidden = true) String type) {
        return connectManager.getParam(type);
    }

    @Operation(summary = "检测openid是否绑定")
    @GetMapping("/connect/app/{type}/openid")
    @Parameter(name = "openid", description = "openid", required = true,   in = ParameterIn.QUERY)
    public Map checkOpenid(@PathVariable("type") @Parameter(hidden = true) String type, @Parameter(hidden = true) String openid) {
        return connectManager.checkOpenid(type, openid);
    }

    @Operation(summary = "APP获取支付宝登录授权SDK")
    @GetMapping("/login-binder/ali/info")
    public String getAppInfoStr() {
        return connectManager.getAliInfo();
    }


    @Operation(summary = "app手机短信登录绑定")
    @PostMapping("/sms-binder/app")
    @Parameters({
            @Parameter(name = "openid", description = "第三方平的openid", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "type", description = "登录方式，可选有：qq、weixin、weibo、alipay", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "mobile", description = "手机号码", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "sms_code", description = "短信码", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "uuid", description = "唯一标识", required = true,   in = ParameterIn.QUERY),
    })
    public Map smsBinder(String openid, String type, String mobile, String smsCode, String uuid) {

        //验证手机验证码
        boolean isPass = smsClient.valid(SceneType.VALIDATE_MOBILE.name(), mobile, smsCode);
        if (!isPass) {
            throw new ServiceException(MemberErrorCode.E107.code(), "短信验证码错误");
        }

        //校验会员是否存在
        Member member = memberManager.getMemberByMobile(mobile);
        //校验当前会员是否存在
        if (member == null) {
            throw new ServiceException(MemberErrorCode.E123.code(), "当前会员不存在");
        }

        return connectManager.appBind(member, openid, type, uuid);
    }

    @Operation(summary = "app用户名密码登录绑定")
    @PostMapping("/login-binder/app")
    @Parameters({
            @Parameter(name = "openid", description = "第三方平的openid", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "type", description = "登录方式，可选有：qq、weixin、weibo、alipay", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "username", description = "用户名", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "password", description = "密码", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "captcha", description = "图片验证码", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "uuid", description = "唯一标识", required = true,   in = ParameterIn.QUERY),
    })
    public Map loginBinder(String openid, String type, String username, String password, String captcha, String uuid) {

        //验证图片验证码
        boolean isPass = captchaClient.valid(uuid, captcha, SceneType.LOGIN.name());
        if (!isPass) {
            throw new ServiceException(MemberErrorCode.E107.code(), "图片验证码错误！");
        }

        //校验会员是否存在
        Member member = memberManager.validation(username, password);

        return connectManager.appBind(member, openid, type, uuid);
    }

}
