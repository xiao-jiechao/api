/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import cn.shoptnt.model.member.dos.MemberAddress;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;



import javax.validation.Valid;

import cn.shoptnt.service.member.MemberAddressManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 会员地址控制器
 *
 * @author zh
 * @version v2.0
 * @since v7.0.0
 * 2018-03-18 15:37:00
 */
@RestController
@RequestMapping("/buyer/members")
@Tag(name = "会员地址相关API")
public class MemberAddressBuyerController {

    @Autowired
    private MemberAddressManager memberAddressManager;

    @Operation(summary = "查询当前会员地址列表")
    @GetMapping(value = "/addresses")
    public List<MemberAddress> list() {
        return this.memberAddressManager.list();
    }

    @Operation(summary = "添加会员地址")
    @PostMapping(value = "/address")
    public MemberAddress add(@Valid MemberAddress memberAddress) {
        this.memberAddressManager.add(memberAddress);
        return memberAddress;
    }

    @PutMapping(value = "/address/{id}")
    @Operation(summary = "修改会员地址")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public MemberAddress edit(@Valid MemberAddress memberAddress, @PathVariable Long id) {
        return this.memberAddressManager.edit(memberAddress, id);
    }


    @PutMapping(value = "/address/{id}/default")
    @Operation(summary = "设置地址为默认")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public String editDefault(@PathVariable Long id) {
        this.memberAddressManager.editDefault(id);
        return null;
    }


    @DeleteMapping(value = "/address/{id}")
    @Operation(summary = "删除会员地址")
    @Parameters({
            @Parameter(name = "id", description = "要删除的会员地址id", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.memberAddressManager.delete(id);
        return "";
    }

    @GetMapping(value = "/address/{id}")
    @Operation(summary = "查询当前会员的某个地址")
    @Parameters({
            @Parameter(name = "id", description = "要查询的地址id", required = true,  in = ParameterIn.PATH)
    })
    public MemberAddress get(@PathVariable Long id) {
        MemberAddress memberAddress = this.memberAddressManager.getModel(id);
        return memberAddress;
    }

}
