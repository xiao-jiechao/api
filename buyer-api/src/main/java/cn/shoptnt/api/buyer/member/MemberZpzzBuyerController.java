/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.member.dos.MemberZpzzDO;
import cn.shoptnt.service.member.MemberZpzzManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.Valid;

/**
 * 会员增票资质API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
@Tag(name = "会员增票资质API")
@RestController
@RequestMapping("/buyer/members/zpzz")
@Validated
public class MemberZpzzBuyerController {

    @Autowired
    private MemberZpzzManager memberZpzzManager;

    @Operation(summary = "会员增票资质申请")
    @PostMapping
    public MemberZpzzDO add(@Valid  MemberZpzzDO memberZpzzDO) {

        return memberZpzzManager.add(memberZpzzDO);
    }

    @Operation(summary = "会员修改增票资质申请")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    @PutMapping(value = "/{id}")
    public MemberZpzzDO edit(@Valid MemberZpzzDO memberZpzzDO, @PathVariable Long id) {

        return memberZpzzManager.edit(memberZpzzDO, id);
    }

    @Operation(summary = "查询会员增票资质详细")
    @GetMapping(value = "/detail")
    public MemberZpzzDO get() {
        return this.memberZpzzManager.get();
    }

    @Operation(summary = "删除会员增票资质信息")
    @Parameters({
            @Parameter(name = "id", description = "主键ID", required = true,  in = ParameterIn.PATH)
    })
    @DeleteMapping(value = "/{id}")
    public String delete(@PathVariable Long id) {
        this.memberZpzzManager.delete(id);
        return "";
    }
}
