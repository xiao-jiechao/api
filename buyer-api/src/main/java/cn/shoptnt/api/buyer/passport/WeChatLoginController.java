/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.passport;


import cn.shoptnt.model.member.dto.WeChatMiniLoginDTO;
import cn.shoptnt.model.member.dto.WeChatUserDTO;
import cn.shoptnt.service.passport.LoginWeChatManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 微信统一登陆
 *
 * @author cs
 * @version v1.0
 * @since v7.2.2
 * 2019-09-22
 */
@Tag(name = "微信统一登陆")
@RestController
@RequestMapping("/buyer/wechat")
@Validated
public class WeChatLoginController {

    @Autowired
    private LoginWeChatManager loginWeChatManager;

    @Operation(summary = "获取授权页地址")
    @Parameter(name	= "redirectUri",	description =	"授权成功跳转地址(需要urlEncode整体加密)",	required = true,  	in = ParameterIn.QUERY)
    @GetMapping("/wap/getLoginUrl")
    public String getLoginUrl(@RequestParam("redirectUri") String  redirectUri){
        return  loginWeChatManager.getLoginUrl(redirectUri);
    }

    @Operation(summary = "网页登陆")
    @GetMapping("/wap/login")
    public Map h5Login(String code,String uuid,@RequestParam(value = "oldUuid") String oldUuid){
        return loginWeChatManager.wxWapLogin(code,uuid,oldUuid);
    }

    @Operation(summary = "app登陆")
    @PostMapping("/app/login/{uuid}")
    public Map appLogin(@PathVariable String uuid, WeChatUserDTO weChatUserDTO){
        return loginWeChatManager.wxAppLogin(uuid,weChatUserDTO);
    }

    @Operation(summary = "小程序登陆")
    @PostMapping("/mini/login")
    public Map miniLogin(WeChatMiniLoginDTO weChatMiniLoginDTO){
        return loginWeChatManager.miniLogin(weChatMiniLoginDTO);
    }

    @Operation(summary = "小程序绑定手机号")
    @PostMapping("/mini/bind/phone")
    public Map miniBindPhone( String encrypted,String iv){
        return loginWeChatManager.miniBindPhone(encrypted,iv);
    }

    @GetMapping("/h5/openid")
    public String getH5Openid(String code) {
        JSONObject accessTokenJson =  loginWeChatManager.getAccessToken(code);
        String openid = accessTokenJson.getString("openid");
        return openid;
    }

    @GetMapping("/mini/openid")
    public String getMiniOpenid(String code) {
        String openid = loginWeChatManager.getMiniOpenid(code);
        return openid;
    }

}
