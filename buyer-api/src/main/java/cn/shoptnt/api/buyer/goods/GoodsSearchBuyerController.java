/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.goods;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.vo.GoodsSearchResultVO;
import cn.shoptnt.model.goodssearch.GoodsSearchDTO;
import cn.shoptnt.model.goodssearch.GoodsWords;
import cn.shoptnt.service.goodssearch.GoodsSearchManager;
import cn.shoptnt.service.goodssearch.GoodsWordsManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author fk
 * @version v2.0
 * @Description: 商品全文检索
 * @date 2018/6/1915:55
 * @since v7.0.0
 */
@RestController
@RequestMapping("/buyer/goods/search")
@Tag(name = "商品检索相关API")
public class GoodsSearchBuyerController {

    @Autowired
    private GoodsSearchManager goodsSearchManager;

    @Autowired
    private GoodsWordsManager goodsWordsManager;

    @Operation(summary = "查询商品列表和商品选择器")
    @Parameter(name="brand_ids",description="品牌ID集合",in = ParameterIn.QUERY)
    @GetMapping
    public GoodsSearchResultVO searchGoodsAndSelector(@Parameter Long pageNo, @Parameter Long pageSize, @Parameter GoodsSearchDTO goodsSearch) throws IOException {

        goodsSearch.setPageNo(pageNo);
        goodsSearch.setPageSize(pageSize);

        return goodsSearchManager.searchGoodsAndSelector(goodsSearch);
    }

    @Operation(summary = "查询商品分词对应数量")
    @Parameter(name = "keyword", description = "搜索关键字", required = true,   in = ParameterIn.QUERY)
    @GetMapping("/words")
    public List<GoodsWords> searchGoodsWords(String keyword){

        return goodsWordsManager.getGoodsWords(keyword);
    }

    @Operation(summary = "获取'为你推荐'商品列表")
    @GetMapping("/recommend")
    public WebPage recommendGoodsList(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) throws IOException{
        GoodsSearchDTO goodsSearch = new GoodsSearchDTO();
        goodsSearch.setPageNo(pageNo);
        goodsSearch.setPageSize(pageSize);

        return goodsSearchManager.recommendGoodsList(goodsSearch);
    }
}
