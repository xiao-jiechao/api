/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.goods.model;

import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * @description: 测试子类
 * @author: liuyulei
 * @create: 2019-12-12 14:30
 * @version:1.0
 * @since:7.1.4
 **/
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TestB extends TestA implements Serializable {

    @Column()
    @Schema(description="排序",required=true)
    private Integer sort;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestB that = (TestB) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(sort, that.sort)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(sort)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "TestB{" +
                "sort=" + sort +
                "} " + super.toString();
    }
}
