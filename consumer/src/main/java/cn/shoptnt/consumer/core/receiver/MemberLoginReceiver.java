/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.member.MemberLoginDispatcher;
import cn.shoptnt.model.member.vo.MemberLoginMsg;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 会员登录消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:31:06
 */
@Component
public class MemberLoginReceiver {

    @Autowired
    private MemberLoginDispatcher dispatcher;

    /**
     * 会员登录
     *
     * @param memberLoginMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MEMEBER_LOGIN + "LOGIN_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MEMEBER_LOGIN, type = ExchangeTypes.FANOUT)
    ))
    public void memberLogin(MemberLoginMsg memberLoginMsg) {
        dispatcher.dispatch(memberLoginMsg);
    }
}
