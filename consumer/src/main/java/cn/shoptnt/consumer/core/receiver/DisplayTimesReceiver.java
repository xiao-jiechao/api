/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.goods.DisplayTimesGoodsDispatcher;
import cn.shoptnt.message.dispatcher.goods.DisplayTimesPlatformDispatcher;
import cn.shoptnt.message.dispatcher.member.DisplayTimesShopDispatcher;
import cn.shoptnt.model.base.message.GoodsViewMessage;
import cn.shoptnt.model.base.message.PlatformViewMessage;
import cn.shoptnt.model.base.message.ShopViewMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 流量统计amqp 消费
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-07 下午4:34
 */
@Component
public class DisplayTimesReceiver {

    @Autowired
    private DisplayTimesShopDispatcher shopDispatcher;

    @Autowired
    private DisplayTimesGoodsDispatcher goodsDispatcher;

    @Autowired
    private DisplayTimesPlatformDispatcher platformDispatcher;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SHOP_VIEW_COUNT + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.SHOP_VIEW_COUNT, type = ExchangeTypes.FANOUT)
    ))
    public void viewShop(ShopViewMessage shopViewMessage) {
        shopDispatcher.dispatch(shopViewMessage);
    }


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.GOODS_VIEW_COUNT + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.GOODS_VIEW_COUNT, type = ExchangeTypes.FANOUT)
    ))
    public void viewGoods(GoodsViewMessage goodsViewMessage) {
        goodsDispatcher.dispatch(goodsViewMessage);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.PLATFORM_VIEW_COUNT + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.PLATFORM_VIEW_COUNT, type = ExchangeTypes.FANOUT)
    ))
    public void viewPlatform(PlatformViewMessage platformViewMessage) {
        platformDispatcher.dispatch(platformViewMessage);
    }


}
