/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.job.EveryDayExecute;
import cn.shoptnt.client.statistics.DisplayTimesClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 流量如果不能达到阙值，则每天消费掉积攒掉流量
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-08 上午8:48
 */
@Component
public class DisplayTimesEveryDayReceiver implements EveryDayExecute {

    @Autowired
    private DisplayTimesClient displayTimesClient;

    /**
     * 每日执行
     */
    @Override
    public void everyDay() {
        displayTimesClient.countNow();
    }
}
