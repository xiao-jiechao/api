/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.member.AskReplyDispatcher;
import cn.shoptnt.model.base.message.AskReplyMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 会员商品咨询回复消息接收者
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-17
 */
@Component
public class AskReplyReceiver {

    @Autowired
    private AskReplyDispatcher dispatcher;

    /**
     * 会员回复商品咨询
     *
     * @param askReplyMessage
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MEMBER_GOODS_ASK_REPLY + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MEMBER_GOODS_ASK_REPLY, type = ExchangeTypes.FANOUT)
    ))
    public void goodsAsk(AskReplyMessage askReplyMessage) {
        dispatcher.dispatch(askReplyMessage);
    }
}
