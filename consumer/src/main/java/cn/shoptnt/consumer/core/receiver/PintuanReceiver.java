/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.trade.pintuan.PintuanDispatcher;
import cn.shoptnt.model.base.message.PinTuanSuccessMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 拼团消息
 *
 * @author fk
 * @version v2.0
 * @since v7.1.4 2019年6月20日 上午10:31:49
 */
@Component
public class PintuanReceiver {

    @Autowired
    private PintuanDispatcher dispatcher;

    /**
     * 拼团成功消息
     *
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.PINTUAN_SUCCESS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.PINTUAN_SUCCESS, type = ExchangeTypes.FANOUT)
    ))
    public void receiveMessage(PinTuanSuccessMessage message) {
        dispatcher.dispatch(message);
    }

}
