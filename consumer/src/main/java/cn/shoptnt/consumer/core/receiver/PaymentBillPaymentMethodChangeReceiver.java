/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.payment.PaymentBillPaymentMethodChangeDispatcher;
import cn.shoptnt.model.base.message.PaymentBillPaymentMethodChangeMsg;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 支付账单支付方式改变消费者
 *
 * @author zh
 * @version v2.0
 * @since v7.2.1 2020年3月11日 上午10:31:42
 */
@Component
public class PaymentBillPaymentMethodChangeReceiver {

    @Autowired
    private PaymentBillPaymentMethodChangeDispatcher dispatcher;

    /**
     * 订单状态改变
     *
     * @param paymentBillPaymentMethodChangeMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.PAYMENT_BILL_PAYMENT_METHOD_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.PAYMENT_BILL_PAYMENT_METHOD_CHANGE,
                    type = ExchangeTypes.FANOUT)
    ))
    public void billPaymentMethodChange(PaymentBillPaymentMethodChangeMsg paymentBillPaymentMethodChangeMsg) {
        dispatcher.dispatch(paymentBillPaymentMethodChangeMsg);
    }

}
