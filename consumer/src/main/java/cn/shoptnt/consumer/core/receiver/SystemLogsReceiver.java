/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.system.SystemLogsDispatcher;
import cn.shoptnt.model.system.dos.SystemLogs;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统日志消息接受者
 *
 * @author fk
 * @version v2.0
 * @since v7.3.0
 * 2021年03月23日17:01:09
 */
@Component
public class SystemLogsReceiver {

    @Autowired
    private SystemLogsDispatcher dispatcher;

    /**
     * 系统日志
     *
     * @param log
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.LOGS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.LOGS, type = ExchangeTypes.FANOUT)
    ))
    public void add(SystemLogs log) {
        dispatcher.dispatch(log);
    }
}
