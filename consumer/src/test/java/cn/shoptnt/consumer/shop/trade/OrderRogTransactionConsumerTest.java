/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.trade;

import cn.shoptnt.message.consumer.trade.OrderRogTransactionConsumer;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.model.trade.order.vo.OrderSkuVO;
import cn.shoptnt.service.trade.order.TransactionRecordManager;
import cn.shoptnt.framework.database.DaoSupport;
import cn.shoptnt.framework.test.BaseTest;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.JsonUtil;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单确认收货增加交易记录消费者测试
 * @author Snow create in 2018/7/2
 * @version v2.0
 * @since v7.0.0
 */
public class OrderRogTransactionConsumerTest extends BaseTest {

    @Autowired
    @Qualifier("tradeDaoSupport")
    private DaoSupport daoSupport;

    private OrderStatusChangeMsg changeMsg;

    @Autowired
    private OrderRogTransactionConsumer consumer;

    @Autowired
    private TransactionRecordManager transactionRecordManager;

    @Before
    public void testData(){

        long memberId = 99;
        changeMsg = new OrderStatusChangeMsg();
        changeMsg.setOldStatus(OrderStatusEnum.SHIPPED);
        changeMsg.setNewStatus(OrderStatusEnum.ROG);

        OrderDO orderDO = new OrderDO();
        orderDO.setSn(DateUtil.getDateline()+"");
        orderDO.setMemberId(memberId);
        orderDO.setMemberName("测试会员名称");

        List<OrderSkuVO> skuVOList = new ArrayList<>();
        OrderSkuVO skuVO = new OrderSkuVO();
        skuVO.setGoodsId(1L);
        skuVO.setNum(50);
        skuVO.setPurchasePrice(10.0);
        skuVOList.add(skuVO);
        orderDO.setItemsJson(JsonUtil.objectToJson(skuVOList));

        changeMsg.setOrderDO(orderDO);

    }

    @Test
    public void test(){
        this.consumer.orderChange(changeMsg);
        List list = this.transactionRecordManager.listAll(changeMsg.getOrderDO().getSn());
        if(list == null && list.isEmpty()){
            throw new RuntimeException("订单确认收货，增加交易记录消费者测试出错");
        }
    }

}
