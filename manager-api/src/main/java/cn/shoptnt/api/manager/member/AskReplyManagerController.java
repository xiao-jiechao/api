/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.model.member.dos.AskReplyDO;
import cn.shoptnt.model.member.dos.MemberAsk;
import cn.shoptnt.model.member.dto.ReplyQueryParam;
import cn.shoptnt.model.member.enums.CommonStatusEnum;
import cn.shoptnt.model.member.vo.BatchAuditVO;
import cn.shoptnt.service.member.AskReplyManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 会员商品咨询回复API
 *
 * @author duanmingyu
 * @version v2.0
 * @since v7.1.5
 * 2019-09-17
 */
@RestController
@RequestMapping("/admin/members/reply")
@Tag(name = "会员商品咨询回复API")
@Validated
public class AskReplyManagerController {

    @Autowired
    private AskReplyManager askReplyManager;

    @Operation(summary = "查询会员商品咨询回复列表")
    @GetMapping
    public WebPage list(@Valid ReplyQueryParam param) {
        //是否已回复 YES:是，NO:否
        param.setReplyStatus(CommonStatusEnum.YES.value());

        return this.askReplyManager.list(param);
    }

    @Operation(summary = "批量审核会员商品咨询回复")
    @PostMapping("/batch/audit")
    public String batchAuditReply(@Valid @RequestBody BatchAuditVO batchAuditVO) {

        this.askReplyManager.batchAudit(batchAuditVO);

        return "";
    }

    @Operation(summary = "删除会员商品咨询回复")
    @Parameters({
            @Parameter(name = "id", description = "会员商品咨询回复主键id",  in = ParameterIn.PATH),
    })
    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") Long id) {

        this.askReplyManager.delete(id);

        return "";
    }
}
