/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.system.dos.Message;
import cn.shoptnt.model.system.dto.MessageQueryParam;
import cn.shoptnt.model.system.vo.MessageVO;
import cn.shoptnt.service.system.MessageManager;
import cn.shoptnt.framework.database.WebPage;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 站内消息控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-04 21:50:52
 */
@RestController
@RequestMapping("/admin/systems/messages")
@Tag(name = "站内消息相关API")
public class MessageManagerController {

    @Autowired
    private MessageManager messageManager;


    @Operation(summary = "查询站内消息列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, MessageQueryParam param) {
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        return this.messageManager.list(param);
    }


    @Operation(summary = "添加站内消息")
    @PostMapping
    public Message add(@Valid MessageVO messageVO) {
        return this.messageManager.add(messageVO);
    }

}
