/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.pagedata;

import cn.shoptnt.model.pagedata.vo.ArticleCategoryVO;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;




import javax.validation.Valid;

import cn.shoptnt.model.pagedata.ArticleCategory;
import cn.shoptnt.service.pagedata.ArticleCategoryManager;

import java.util.List;

/**
 * 文章分类控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-11 15:01:32
 */
@RestController
@RequestMapping("/admin/pages/article-categories")
@Tag(name = "文章分类相关API")
public class ArticleCategoryManagerController {

    @Autowired
    private ArticleCategoryManager articleCategoryManager;


    @Operation(summary = "查询文章一级分类列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY),
            @Parameter(name = "name", description = "分类名称",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, String name) {

        return this.articleCategoryManager.list(pageNo, pageSize, name);
    }

    @Operation(summary = "查询文章二级分类列表")
    @Parameters({
            @Parameter(name = "id", description = "一级文章分类id", required = true,  in = ParameterIn.PATH),
    })
    @GetMapping("/{id}/children")
    public List<ArticleCategory> list(@PathVariable Long id) {

        return this.articleCategoryManager.listChildren(id);
    }


    @Operation(summary = "添加文章分类")
    @PostMapping
    public ArticleCategory add(@Valid ArticleCategory articleCategory) {

        this.articleCategoryManager.add(articleCategory);

        return articleCategory;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改文章分类")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public ArticleCategory edit(@Valid ArticleCategory articleCategory, @PathVariable Long id) {

        this.articleCategoryManager.edit(articleCategory, id);

        return articleCategory;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除文章分类")
    @Parameters({
            @Parameter(name = "id", description = "要删除的文章分类主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.articleCategoryManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个文章分类")
    @Parameters({
            @Parameter(name = "id", description = "要查询的文章分类主键", required = true,  in = ParameterIn.PATH)
    })
    public ArticleCategory get(@PathVariable Long id) {

        ArticleCategory articleCategory = this.articleCategoryManager.getModel(id);

        return articleCategory;
    }


    @Operation(summary = "查询文章分类树")
    @GetMapping("/childrens")
    public List<ArticleCategoryVO> getMenuTree() {
        return this.articleCategoryManager.getCategoryTree();
    }

}
