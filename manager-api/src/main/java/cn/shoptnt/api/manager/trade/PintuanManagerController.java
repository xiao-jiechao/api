/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.trade;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.pintuan.Pintuan;
import cn.shoptnt.model.promotion.pintuan.PintuanQueryParam;
import cn.shoptnt.service.trade.pintuan.PintuanGoodsManager;
import cn.shoptnt.service.trade.pintuan.PintuanManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 拼团活动控制器
 *
 * @author liushuai
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2019/2/12 下午5:34
 */

@RestController
@RequestMapping("/admin/promotion/pintuan")
@Tag(name = "拼团API")
@Validated
public class PintuanManagerController {

    @Autowired
    private PintuanManager pintuanManager;

    @Autowired
    private PintuanGoodsManager pintuanGoodsManager;

    @Operation(summary = "查询拼团列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY),
            @Parameter(name = "name", description = "拼团活动名称",   in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "商家ID",  in = ParameterIn.QUERY),
            @Parameter(name = "status", description = "拼团活动名称",   in = ParameterIn.QUERY,
                   example = "WAIT:待开始，UNDERWAY：进行中，END：已结束"),
            @Parameter(name = "start_time", description = "拼团日期-开始日期",  in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "拼团日期-结束日期",  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage<Pintuan> list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String name,
                                 @Parameter(hidden = true) Long sellerId, @Parameter(hidden = true) String status, @Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {
        PintuanQueryParam param = new PintuanQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setName(name);
        param.setSellerId(sellerId);
        param.setStatus(status);
        param.setStartTime(startTime);
        param.setEndTime(endTime);

        return this.pintuanManager.list(param);
    }


    @Operation(summary = "获取活动参与的商品")
    @GetMapping("/goods/{id}")
    @Parameters({
            @Parameter(name = "id", description = "活动ID", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量",  in = ParameterIn.QUERY),
            @Parameter(name = "name", description = "名称",   in = ParameterIn.QUERY)

    })
    public WebPage promotionGoods(@Parameter(hidden = true) @PathVariable Long id, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String name) {
        return this.pintuanGoodsManager.page(pageNo, pageSize, id, name);
    }


    @PutMapping(value = "/{id}/close")
    @Operation(summary = "关闭拼团")
    @Parameters({
            @Parameter(name = "id", description = "要关闭的拼团入库主键", required = true,  in = ParameterIn.PATH)
    })
    public void close(@PathVariable Long id) {
        this.pintuanManager.manualClosePromotion(id);

    }

    @PutMapping(value = "/{id}/open")
    @Operation(summary = "开启拼团")
    @Parameters({
            @Parameter(name = "id", description = "要关闭的拼团入库主键", required = true,  in = ParameterIn.PATH)
    })
    public void open(@PathVariable Long id) {

        this.pintuanManager.manualOpenPromotion(id);

    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个拼团入库")
    @Parameters({
            @Parameter(name = "id", description = "要查询的拼团主键", required = true,  in = ParameterIn.PATH)
    })
    public Pintuan get(@PathVariable Long id) {
        return this.pintuanManager.getModel(id);
    }


}
