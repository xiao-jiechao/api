/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goodssearch;

import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.model.goodssearch.CustomWords;
import cn.shoptnt.model.goodssearch.EsSecretSetting;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.service.goodssearch.CustomWordsManager;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.StringUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 自定义分词控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-20 16:08:07
 *
 * update by liuyulei 2019-05-27
 */
@RestController
@RequestMapping("/admin/goodssearch/custom-words")
@Tag(name = "自定义分词相关API")
public class CustomWordsManagerController {

    @Autowired
    private CustomWordsManager customWordsManager;

    @Autowired
    private SettingClient settingClient;


    @Operation(summary = "查询自定义分词列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "keywords", description = "关键字",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String keywords) {

        return this.customWordsManager.list(pageNo, pageSize,keywords);
    }


    @Operation(summary = "添加自定义分词")
    @PostMapping
    public CustomWords add(@Valid CustomWords customWords) {

        this.customWordsManager.add(customWords);

        return customWords;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改自定义分词")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public CustomWords edit(@Valid CustomWords customWords, @PathVariable Long id) {

        this.customWordsManager.edit(customWords, id);

        return customWords;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除自定义分词")
    @Parameters({
            @Parameter(name = "id", description = "要删除的自定义分词主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.customWordsManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个自定义分词")
    @Parameters({
            @Parameter(name = "id", description = "要查询的自定义分词主键", required = true,  in = ParameterIn.PATH)
    })
    public CustomWords get(@PathVariable Long id) {

        CustomWords customWords = this.customWordsManager.getModel(id);

        return customWords;
    }

    /**
     * add by liuyulei 2019-05-26
     * @param password
     * @return
     */
    @Operation(summary = "设置秘钥")
    @PutMapping("/secret-key")
    @Parameters({
            @Parameter(name = "password",description = "密码",required = true, in = ParameterIn.QUERY)
    })
    @Log(client = LogClient.admin,detail = "修改搜索秘钥参数",level = LogLevel.important)
    public EsSecretSetting setSecretKey(@Parameter(hidden = true) String password){
        EsSecretSetting secretSetting = new EsSecretSetting();

        secretSetting.setPassword(password);

        secretSetting.setSecretKey(password);
        settingClient.save(SettingGroup.ES_SIGN,secretSetting);

        return secretSetting;

    }

    @Operation(summary = "获取秘钥")
    @GetMapping("/secret-key")
    public EsSecretSetting getSecretKey(){
        String value = settingClient.get(SettingGroup.ES_SIGN);
        if(StringUtil.isEmpty(value)){
            return new EsSecretSetting();
        }
        EsSecretSetting secretSetting = JsonUtil.jsonToObject(value,EsSecretSetting.class);
        return secretSetting;

    }


}
