/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.pagedata;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import cn.shoptnt.framework.database.WebPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;



import javax.validation.Valid;

import cn.shoptnt.model.pagedata.HotKeyword;
import cn.shoptnt.service.pagedata.HotKeywordManager;

/**
 * 热门关键字控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-04 10:43:23
 */
@RestController
@RequestMapping("/admin/pages/hot-keywords")
@Tag(name = "热门关键字相关API")
public class HotKeywordManagerController {

    @Autowired
    private HotKeywordManager hotKeywordManager;


    @Operation(summary = "查询热门关键字列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        return this.hotKeywordManager.list(pageNo, pageSize);
    }


    @Operation(summary = "添加热门关键字")
    @PostMapping
    public HotKeyword add(@Valid HotKeyword hotKeyword) {

        this.hotKeywordManager.add(hotKeyword);

        return hotKeyword;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改热门关键字")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public HotKeyword edit(@Valid HotKeyword hotKeyword, @PathVariable Long id) {

        this.hotKeywordManager.edit(hotKeyword, id);

        return hotKeyword;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除热门关键字")
    @Parameters({
            @Parameter(name = "id", description = "要删除的热门关键字主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.hotKeywordManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个热门关键字")
    @Parameters({
            @Parameter(name = "id", description = "要查询的热门关键字主键", required = true,  in = ParameterIn.PATH)
    })
    public HotKeyword get(@PathVariable Long id) {

        HotKeyword hotKeyword = this.hotKeywordManager.getModel(id);

        return hotKeyword;
    }

}
