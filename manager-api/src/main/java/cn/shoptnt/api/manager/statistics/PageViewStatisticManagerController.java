/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.statistics;

import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.PageViewStatisticManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.validation.Valid;

/**
 * 平台后台 流量分析
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018年3月19日上午8:35:47
 */

@Tag(name = "平台统计 流量分析")
@RestController
@RequestMapping("/admin/statistics/page_view")
public class PageViewStatisticManagerController {

    @Autowired
    private PageViewStatisticManager pageViewStatisticManager;

    @Operation(summary = "获取店铺访问量数据")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份，空则查询当前年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份，空则查询当前月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id，为空则返回所有店铺数据", in = ParameterIn.QUERY)})
    @GetMapping("/shop")
    public SimpleChart getShop(@Valid @Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.pageViewStatisticManager.countShop(searchCriteria);
    }

    @Operation(summary = "获取商品访问量数据，只取前30")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份，空则查询当前年份", in = ParameterIn.QUERY),
            @Parameter(name = "month", description = "月份，空则查询当前月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id，为空则返回所有店铺数据", in = ParameterIn.QUERY)})
    @GetMapping("/goods")
    public SimpleChart getGoods(@Valid @Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.pageViewStatisticManager.countGoods(searchCriteria);
    }

}
