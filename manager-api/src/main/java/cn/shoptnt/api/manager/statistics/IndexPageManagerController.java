/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.statistics;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.base.vo.BackendIndexModelVO;
import cn.shoptnt.model.statistics.enums.QueryDateType;
import cn.shoptnt.model.statistics.vo.MultipleChart;
import cn.shoptnt.service.statistics.GoodsStatisticManager;
import cn.shoptnt.service.statistics.IndexPageStatisticManager;
import cn.shoptnt.service.statistics.OrderStatisticManager;
import cn.shoptnt.service.statistics.ShopStatisticManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 后台首页api
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-06-29 上午8:13
 */
@RestController
@RequestMapping("/admin/index/page")
@Tag(name = "后台首页api")
@Validated
public class IndexPageManagerController {

    @Autowired
    private OrderStatisticManager orderStatisticManager;

    @Autowired
    private ShopStatisticManager shopStatisticManager;

    @Autowired
    private GoodsStatisticManager goodsStatisticManager;

    @Autowired
    private IndexPageStatisticManager indexPageStatisticManager;

    @GetMapping
    @Operation(summary = "首页响应")
    public BackendIndexModelVO index() {
        return indexPageStatisticManager.index();
    }

    @Operation(summary = "交易额图表")
    @Parameters({
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY, required = true),
    })
    @GetMapping(value = "/turnoverChart")
    public MultipleChart turnoverChart(@Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {
        return this.orderStatisticManager.getIndexTurnoverChart(startTime, endTime, null);
    }

    @Operation(summary = "流量走势图表")
    @Parameters({
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY, required = true),
    })
    @GetMapping(value = "/pvChart")
    public MultipleChart pvChart(@Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {
        return this.indexPageStatisticManager.getPvChart(startTime, endTime);
    }

    @Operation(summary = "热卖店铺TOP10")
    @GetMapping(value = "/hotShop")
    public WebPage hotShop() {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLimit(10);
        searchCriteria.setCycleType(QueryDateType.TIME.value());
        searchCriteria.setStartTime(DateUtil.startOfYear());
        searchCriteria.setEndTime(DateUtil.getDateline());
        return this.shopStatisticManager.getHotSalesMoneyPage(searchCriteria);
    }

    @Operation(summary = "热卖商品TOP10")
    @GetMapping(value = "/hotGoods")
    public WebPage hotGoods() {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLimit(10);
        searchCriteria.setCycleType(QueryDateType.TIME.value());
        searchCriteria.setStartTime(DateUtil.startOfYear());
        searchCriteria.setEndTime(DateUtil.getDateline());
        return this.goodsStatisticManager.getHotSalesMoneyPage(searchCriteria);
    }

}
