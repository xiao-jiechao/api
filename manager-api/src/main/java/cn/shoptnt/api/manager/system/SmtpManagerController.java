/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import cn.shoptnt.service.system.SmtpManager;
import cn.shoptnt.model.system.dos.SmtpDO;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 邮件控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-25 16:16:53
 */
@RestController
@RequestMapping("/admin/systems/smtps")
@Tag(name = "邮件相关API")
public class SmtpManagerController {

    @Autowired
    private SmtpManager smtpManager;


    @Operation(summary = "查询邮件列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) @NotEmpty(message = "页码不能为空") Long pageNo, @Parameter(hidden = true) @NotEmpty(message = "每页数量不能为空") Long pageSize) {
        return this.smtpManager.list(pageNo, pageSize);
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改smtp")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public SmtpDO edit(@Valid SmtpDO smtp, @PathVariable Long id) {

        this.smtpManager.edit(smtp, id);

        return smtp;
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个smtp")
    @Parameters({
            @Parameter(name = "id", description = "要查询的smtp主键", required = true,  in = ParameterIn.PATH)
    })
    public SmtpDO get(@PathVariable Long id) {
        SmtpDO smtp = this.smtpManager.getModel(id);
        return smtp;
    }

    @Operation(summary = "添加smtp")
    @PostMapping
    public SmtpDO add(@Valid SmtpDO smtp) {
        this.smtpManager.add(smtp);
        return smtp;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除smtp")
    @Parameters({
            @Parameter(name = "id", description = "要删除的smtp主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.smtpManager.delete(id);
        return null;
    }

    @PostMapping(value = "/send")
    @Operation(summary = "测试发送邮件")
    @Parameter(name = "email", description = "要发送的邮箱地址", required = true,   in = ParameterIn.QUERY)
    public String send(@Valid String email, SmtpDO smtp) {
        this.smtpManager.send(email, smtp);
        return null;
    }

}
