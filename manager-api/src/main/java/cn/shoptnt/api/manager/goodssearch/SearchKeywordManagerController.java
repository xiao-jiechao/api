/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goodssearch;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goodssearch.SearchKeywordDO;
import cn.shoptnt.service.goodssearch.SearchKeywordManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


/**
* @author liuyulei
 * @version 1.0
 * @Description: 关键字搜索历史相关API
 * @date 2019/5/27 11:33
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/goodssearch/keywords")
@Tag(name = "关键字搜索历史相关API")
public class SearchKeywordManagerController {


    @Autowired
    private SearchKeywordManager searchKeywordManager;

    @Operation(summary = "查询关键字历史列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "keywords", description = "关键字",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String keywords){
        return this.searchKeywordManager.list(pageNo,pageSize,keywords);
    }
}
