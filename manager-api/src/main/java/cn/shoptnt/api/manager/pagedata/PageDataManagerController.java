/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.pagedata;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.pagedata.PageData;
import cn.shoptnt.model.pagedata.PageQueryParam;
import cn.shoptnt.service.pagedata.PageDataManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 楼层控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-21 16:39:22
 */
@RestController
@RequestMapping("/admin/pages")
@Tag(name = "楼层相关API")
@Validated
public class PageDataManagerController {

    @Autowired
    private PageDataManager pageManager;


    @GetMapping()
    @Operation(summary = "查询页面列表")
    @Parameters({
            @Parameter(name = "client_type", description = "PC/MOBILE", required = true,   in = ParameterIn.QUERY)
    })
    public WebPage getList(PageQueryParam param) {
        param.setSellerId(0L);
        WebPage pageList = this.pageManager.getPageList(param);
        return pageList;
    }


    @PostMapping()
    @Operation(summary = "添加微页面")
    @Parameters({
            @Parameter(name = "page_name", description = "微页面标题", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "page_data", description = "页面元素", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "client_type", description = "", required = true,   in = ParameterIn.QUERY)
    })
    public PageData add(@Valid @RequestBody PageData page) {
        page.setSellerId(0L);
        this.pageManager.add(page);
        return page;
    }


    @PutMapping(value = "/{id}")
    @Operation(summary = "修改楼层")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "page_name", description = "微页面标题", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "page_data", description = "PC/MOBILE", required = true,   in = ParameterIn.QUERY)
    })
    public PageData edit(@Valid @RequestBody PageData page, @PathVariable("id") Long id) {
        return this.pageManager.edit(page, id);
    }

    @PutMapping(value = "/{id}/publish")
    @Operation(summary = "修改发布状态")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,   in = ParameterIn.PATH),
    })
    public PageData updatePublish(@PathVariable("id") Long id) {
        return pageManager.updatePublish(id);
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个楼层")
    @Parameters({
            @Parameter(name = "id", description = "要查询的楼层主键", required = true,   in = ParameterIn.PATH)
    })
    public PageData get(@PathVariable("id") Long id) {
        PageData page = this.pageManager.getModel(id);
        return page;
    }


    @DeleteMapping(value = "/{ids}")
    @Operation(summary = "删除微页面")
    @Parameters({@Parameter(name = "ids", description = "id集合", required = true,   in = ParameterIn.PATH)})
    public void delete(@PathVariable("ids") Long[] ids) {
        this.pageManager.delete(ids);
    }


    @PutMapping(value = "/{id}/index")
    @Operation(summary = "设为首页")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,   in = ParameterIn.PATH)
    })
    public PageData editIndex(@PathVariable("id") Long id) {
        PageData pageData = this.pageManager.editIndex(0L, id);
        return pageData;
    }


}
