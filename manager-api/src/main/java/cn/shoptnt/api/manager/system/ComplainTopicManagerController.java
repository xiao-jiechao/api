/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.system.dos.ComplainTopic;
import cn.shoptnt.service.system.ComplainTopicManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 投诉主题控制器
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2019-11-26 16:06:44
 */
@RestController
@RequestMapping("/admin/systems/complain-topics")
@Tag(name = "投诉主题相关API")
public class ComplainTopicManagerController {

    @Autowired
    private ComplainTopicManager complainTopicManager;


    @Operation(summary = "查询投诉主题列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        return this.complainTopicManager.list(pageNo, pageSize);
    }


    @Operation(summary = "添加投诉主题")
    @PostMapping
    public ComplainTopic add(@Valid ComplainTopic complainTopic) {

        this.complainTopicManager.add(complainTopic);

        return complainTopic;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改投诉主题")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public ComplainTopic edit(@Valid ComplainTopic complainTopic, @PathVariable Long id) {

        this.complainTopicManager.edit(complainTopic, id);

        return complainTopic;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除投诉主题")
    @Parameters({
            @Parameter(name = "id", description = "要删除的投诉主题主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {

        this.complainTopicManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个投诉主题")
    @Parameters({
            @Parameter(name = "id", description = "要查询的投诉主题主键", required = true,  in = ParameterIn.PATH)
    })
    public ComplainTopic get(@PathVariable Long id) {

        ComplainTopic complainTopic = this.complainTopicManager.getModel(id);

        return complainTopic;
    }

}
