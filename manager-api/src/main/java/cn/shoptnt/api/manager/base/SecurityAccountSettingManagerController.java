/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.base;

import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.model.system.vo.AccountSetting;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 账号安全设置api
 *
 * @author shenyanwu
 * @version v7.0
 * @date 2021/11/19 下午6:55
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/settings")
@Tag(name = "账号安全设置")
@Validated
public class  SecurityAccountSettingManagerController {
    @Autowired
    private SettingClient settingClient;


    @GetMapping(value = "/account")
    @Operation(summary = "获取账号安全设置")
    public AccountSetting getAccountSetting() {
        String accountSettingJson = settingClient.get( SettingGroup.ACCOUNT);
        AccountSetting accountSetting = JsonUtil.jsonToObject(accountSettingJson, AccountSetting.class);
        if (accountSetting == null) {
            return new AccountSetting();
        }
        return accountSetting;
    }

    @PutMapping(value = "/account")
    @Operation(summary = "修改账号安全设置")
    @Log(client = LogClient.admin,detail = "修改账号安全设置",level = LogLevel.important)
    public AccountSetting editAccountSetting(@Valid AccountSetting accountSetting) {
        settingClient.save(SettingGroup.ACCOUNT, accountSetting);
        return accountSetting;
    }

}
