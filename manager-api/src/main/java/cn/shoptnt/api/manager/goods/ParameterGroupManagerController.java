/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goods;

import cn.shoptnt.model.support.validator.annotation.SortType;
import cn.shoptnt.model.goods.dos.ParameterGroupDO;
import cn.shoptnt.service.goods.ParameterGroupManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 参数组控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-20 16:14:17
 */
@RestController
@RequestMapping("/admin/goods/parameter-groups")
@Tag(name = "参数组相关API")
@Validated
public class ParameterGroupManagerController {

	@Autowired
	private ParameterGroupManager parameterGroupManager;

	@Operation(summary = "添加参数组")
	@PostMapping
	public ParameterGroupDO add(@Valid ParameterGroupDO parameterGroup) {

		this.parameterGroupManager.add(parameterGroup);

		return parameterGroup;
	}

	@PutMapping(value = "/{id}")
	@Operation(summary = "修改参数组")
	@Parameters({
			@Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
			@Parameter(name = "group_name", description = "参数组名称", required = true,   in = ParameterIn.QUERY)
	})
	public ParameterGroupDO edit(@NotEmpty(message = "参数组名称不能为空") String groupName, @PathVariable Long id) {

		ParameterGroupDO parameterGroup = this.parameterGroupManager.edit(groupName, id);

		return parameterGroup;
	}

	@DeleteMapping(value = "/{id}")
	@Operation(summary = "删除参数组")
	@Parameters({
			@Parameter(name = "id", description = "要删除的参数组主键", required = true,  in = ParameterIn.PATH) })
	public String delete(@PathVariable Long id) {

		this.parameterGroupManager.delete(id);

		return "";
	}

	@GetMapping(value = "/{id}")
	@Operation(summary = "查询一个参数组")
	@Parameters({
			@Parameter(name = "id", description = "要查询的参数组主键", required = true,  in = ParameterIn.PATH) })
	public ParameterGroupDO get(@PathVariable Long id) {

		ParameterGroupDO parameterGroup = this.parameterGroupManager.getModel(id);

		return parameterGroup;
	}

	@Operation(summary = "参数组上移或者下移")
	@Parameters({
			@Parameter(name = "group_id", description = "参数组id", required = true, in = ParameterIn.PATH),
			@Parameter(name = "sort_type", description = "排序类型，上移 up，下移down", required = true, in = ParameterIn.QUERY), })
	@PutMapping(value = "/{group_id}/sort")
	public String groupSort(@PathVariable("group_id") Long groupId,
			@Parameter(hidden = true) @SortType String sortType) {

		this.parameterGroupManager.groupSort(groupId, sortType);

		return null;
	}

}
