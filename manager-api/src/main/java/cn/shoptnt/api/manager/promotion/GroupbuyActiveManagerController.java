/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyActiveDO;
import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyActiveVO;
import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyAuditParam;
import cn.shoptnt.model.promotion.groupbuy.vo.GroupbuyQueryParam;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.exception.SystemErrorCodeV1;
import cn.shoptnt.framework.util.DateUtil;

import cn.shoptnt.service.promotion.groupbuy.GroupbuyActiveManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 团购活动表控制器
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 15:46:51
 */
@RestController
@RequestMapping("/admin/promotion/group-buy-actives")
@Tag(name = "团购活动表相关API")
@Validated
public class GroupbuyActiveManagerController {

    @Autowired
    private GroupbuyActiveManager groupbuyActiveManager;

    @Operation(summary = "查询团购活动表列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", in = ParameterIn.QUERY),
            @Parameter(name = "act_name", description = "活动名称", in = ParameterIn.QUERY),
            @Parameter(name = "act_status", description = "活动状态 NOT_STARTED:未开始,STARTED:进行中,OVER:已结束", in = ParameterIn.QUERY),
            @Parameter(name = "start_time", description = "开始时间", in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage<GroupbuyActiveVO> list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String actName,
                                          @Parameter(hidden = true) String actStatus, @Parameter(hidden = true) Long startTime, @Parameter(hidden = true) Long endTime) {
        GroupbuyQueryParam param = new GroupbuyQueryParam();
        param.setPage(pageNo);
        param.setPageSize(pageSize);
        param.setActName(actName);
        param.setActStatus(actStatus);
        param.setStartTime(startTime);
        param.setEndTime(endTime);

        WebPage<GroupbuyActiveVO> page = this.groupbuyActiveManager.list(param);
        return page;

    }


    @Operation(summary = "添加团购活动表")
    @Parameter(name = "activeDO", description = "团购信息", required = true)
    @PostMapping
    public GroupbuyActiveDO add(@Parameter(hidden = true) @Valid @RequestBody GroupbuyActiveDO activeDO) {

        this.verifyParam(activeDO.getStartTime(), activeDO.getEndTime(), activeDO.getJoinEndTime());
        this.groupbuyActiveManager.add(activeDO);
        return activeDO;
    }


    @Operation(summary = "修改团购活动表")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true, in = ParameterIn.PATH)
    })
    @PutMapping(value = "/{id}")
    public GroupbuyActiveDO edit(@Valid @RequestBody GroupbuyActiveDO activeDO, @PathVariable Long id) {
        this.verifyParam(activeDO.getStartTime(), activeDO.getEndTime(), activeDO.getJoinEndTime());
        this.groupbuyActiveManager.edit(activeDO, id);
        return activeDO;
    }


    @PostMapping(value = "/{id}/delete")
    @Operation(summary = "删除团购活动")
    @Parameters({
            @Parameter(name = "id", description = "要删除的团购活动主键", required = true, in = ParameterIn.PATH),
            @Parameter(name = "delete_reason", description = "删除原因", required = true, in = ParameterIn.QUERY)
    })
    public String delete(@PathVariable Long id, @Parameter(hidden = true) String deleteReason) {
        this.groupbuyActiveManager.delete(id, deleteReason, AdminUserContext.getAdmin().getUsername());
        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个团购活动表")
    @Parameters({
            @Parameter(name = "id", description = "要查询的团购活动表主键", required = true, in = ParameterIn.PATH)
    })
    public GroupbuyActiveDO get(@PathVariable Long id) {

        GroupbuyActiveDO groupbuyActive = this.groupbuyActiveManager.getModel(id);

        return groupbuyActive;
    }


    @Operation(summary = "批量审核商品")
    @PostMapping(value = "/batch/audit")
    @Log(client = LogClient.admin, detail = "审核团购商品")
    public void batchAudit(@Valid @RequestBody GroupbuyAuditParam param) {

        this.groupbuyActiveManager.batchAuditGoods(param);
    }

    /**
     * 验证参数
     *
     * @param startTime   活动开始时间
     * @param endTime     活动结束时间
     * @param joinEndTime 报名截止时间
     */
    private void verifyParam(long startTime, long endTime, long joinEndTime) {

        long nowTime = DateUtil.getDateline();

        //如果活动起始时间小于现在时间
        if (joinEndTime < nowTime) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "报名截止时间必须大于当前时间");
        }

        //如果活动开始时间小于 报名截止时间
        if (startTime < joinEndTime) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "活动开始时间必须大于报名截止时间");
        }

        // 开始时间不能大于结束时间
        if (startTime > endTime) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "活动起始时间不能大于活动结束时间");
        }

    }

}
