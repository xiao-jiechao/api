/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;





import javax.validation.constraints.NotEmpty;

import cn.shoptnt.model.system.dos.UploaderDO;
import cn.shoptnt.model.system.vo.UploaderVO;
import cn.shoptnt.service.system.UploaderManager;

/**
 * 存储方案控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-22 09:31:56
 */
@RestController
@RequestMapping("/admin/systems/uploaders")
@Tag(name = "存储方案相关API")
public class UploaderManagerController {

    @Autowired
    private UploaderManager uploaderManager;


    @Operation(summary = "查询存储方案列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) @NotEmpty(message = "页码不能为空") Long pageNo, @Parameter(hidden = true) @NotEmpty(message = "每页数量不能为空") Long pageSize) {
        return this.uploaderManager.list(pageNo, pageSize);
    }

    @Operation(summary = "修改存储方案参数")
    @PutMapping(value = "/{bean}")
    @Parameters({
            @Parameter(name = "bean", description = "存储方案bean id", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "uploader", description = "存储对象", required = true)
    })
    @Log(client = LogClient.admin,detail = "修改存储方案，${uploader.name}",level = LogLevel.important)
    public UploaderVO edit(@PathVariable String bean, @RequestBody @Parameter(hidden = true) UploaderVO uploader) {
        uploader.setBean(bean);
        return this.uploaderManager.edit(uploader);
    }

    @Operation(summary = "开启某个存储方案")
    @PutMapping("/{bean}/open")
    @Parameter(name = "bean", description = "bean", required = true,   in = ParameterIn.PATH)
    public String open(@PathVariable String bean) {
        this.uploaderManager.openUploader(bean);
        return null;
    }


    @Operation(summary = "获取存储方案的配置")
    @GetMapping("/{bean}")
    @Parameter(name = "bean", description = "存储方案bean id", required = true,   in = ParameterIn.PATH)
    public UploaderVO getUploadSetting(@PathVariable String bean) {
        return this.uploaderManager.getUploadConfig(bean);
    }


}
