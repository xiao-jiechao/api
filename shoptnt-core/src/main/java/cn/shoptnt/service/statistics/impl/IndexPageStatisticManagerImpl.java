/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.mapper.statistics.PlatformPageViewMapper;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.base.vo.BackendIndexModelVO;
import cn.shoptnt.model.shop.enums.ShopStatusEnum;
import cn.shoptnt.model.statistics.dos.PlatformPageView;
import cn.shoptnt.model.statistics.enums.QueryDateType;
import cn.shoptnt.model.statistics.vo.ChartSeries;
import cn.shoptnt.model.statistics.vo.MultipleChart;
import cn.shoptnt.service.aftersale.AfterSaleQueryManager;
import cn.shoptnt.service.distribution.WithdrawManager;
import cn.shoptnt.service.goods.GoodsQueryManager;
import cn.shoptnt.service.member.MemberCommentManager;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.shop.ShopManager;
import cn.shoptnt.service.statistics.IndexPageStatisticManager;
import cn.shoptnt.service.statistics.util.StatisticsUtil;
import cn.shoptnt.service.trade.complain.OrderComplainManager;
import cn.shoptnt.service.trade.order.OrderQueryManager;
import cn.shoptnt.util.DataDisplayUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 首页相关统计
 *
 * @author 张崧
 * @since 2024-04-24
 */
@Service
public class IndexPageStatisticManagerImpl implements IndexPageStatisticManager {

    @Autowired
    private GoodsQueryManager goodsQueryManager;

    @Autowired
    private PlatformPageViewMapper platformPageViewMapper;

    @Autowired
    private MemberManager memberManager;

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Autowired
    private ShopManager shopManager;

    @Autowired
    private OrderComplainManager orderComplainManager;

    @Autowired
    private AfterSaleQueryManager afterSaleQueryManager;

    @Autowired
    private WithdrawManager withdrawManager;

    @Autowired
    private MemberCommentManager memberCommentManager;

    private static final long DAY_SECOND = 86400;

    @Override
    public BackendIndexModelVO index() {
        BackendIndexModelVO backendIndexModelVO = new BackendIndexModelVO();

        // 总量统计
        backendIndexModelVO.setGoodsCount(goodsQueryManager.getGoodsCountByParam(null, null, null, null, null));
        backendIndexModelVO.setMemberCount(memberManager.getCount(null, null));
        backendIndexModelVO.setOrderCount(orderQueryManager.getCount(null, null));
        backendIndexModelVO.setShopCount(shopManager.getCount(ShopStatusEnum.OPEN, null, null));

        // 待办统计
        backendIndexModelVO.setWaitAuditGoodsCount(goodsQueryManager.getGoodsCountByParam(null, null, 0, null, null));
        backendIndexModelVO.setWaitAuditShopCount(shopManager.getCount(ShopStatusEnum.APPLY, null, null));
        backendIndexModelVO.setWaitHandleComplainCount(orderComplainManager.getWaitHandleCount());
        backendIndexModelVO.setWaitHandleServiceCount(afterSaleQueryManager.getWaitHandleCount());
        backendIndexModelVO.setWaitAuditWithdrawCount(withdrawManager.getWaitAuditCount());

        // 访问量统计
        setVisitCount(backendIndexModelVO);

        // 今日统计
        long startOfTodDay = DateUtil.startOfTodDay();
        long endOfTodDay = DateUtil.endOfTodDay();
        backendIndexModelVO.setTodayOrderCount(orderQueryManager.getCount(startOfTodDay, endOfTodDay));
        backendIndexModelVO.setTodayOrderAmount(orderQueryManager.getTurnoverAmount(startOfTodDay, endOfTodDay));
        backendIndexModelVO.setTodayShopCount(shopManager.getCount(ShopStatusEnum.OPEN, startOfTodDay, endOfTodDay));
        backendIndexModelVO.setTodayMemberCount(memberManager.getCount(startOfTodDay, endOfTodDay));
        backendIndexModelVO.setTodayGoodsCount(goodsQueryManager.getGoodsCountByParam(null, null, null, startOfTodDay, endOfTodDay));
        backendIndexModelVO.setTodayCommentCount(memberCommentManager.getCount(startOfTodDay, endOfTodDay));

        return backendIndexModelVO;
    }

    @Override
    public MultipleChart getPvChart(Long startTime, Long endTime) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCycleType(QueryDateType.TIME.value());
        searchCriteria.setStartTime(startTime);
        searchCriteria.setEndTime(endTime);

        List<PlatformPageView> list = platformPageViewMapper.selectList(new LambdaQueryWrapper<PlatformPageView>()
                .ge(PlatformPageView::getTime, startTime)
                .le(PlatformPageView::getTime, endTime));

        boolean isSameYear = DataDisplayUtil.getYear(startTime).equals(DataDisplayUtil.getYear(endTime));
        List<Map<String, Object>> mapList = list.stream().map(platformPageView -> {
            Map<String, Object> map = BeanUtil.beanToMap(platformPageView);
            String time = platformPageView.getMonth() + "-" + platformPageView.getDay();
            map.put("time", isSameYear ? time : platformPageView.getYear() + "-" + time);
            return map;
        }).collect(Collectors.toList());

        //生成图表数据
        int resultSize = DataDisplayUtil.getResultSize(searchCriteria);
        String[] data = new String[resultSize];

        String[] xAxis = StatisticsUtil.getLocalName(resultSize, searchCriteria);
        StatisticsUtil.dataSet(mapList, data, resultSize, "num", xAxis);

        ChartSeries chartSeries = new ChartSeries("当前时间段", data, new String[0]);
        List<ChartSeries> chartSeriess = new ArrayList<>();
        chartSeriess.add(chartSeries);
        return new MultipleChart(chartSeriess, xAxis, new String[0]);
    }

    private void setVisitCount(BackendIndexModelVO backendIndexModelVO) {
        // 今日访问量
        Long startOfTodDay = cn.shoptnt.framework.util.DateUtil.startOfTodDay();
        Integer todayVisitCount = platformPageViewMapper.selectVisitCountByTimeRange(startOfTodDay, startOfTodDay);
        backendIndexModelVO.setTodayVisitCount(todayVisitCount);

        // 昨日访问量
        Long startOfYesterday = startOfTodDay - DAY_SECOND;
        Integer yesterdayVisitCount = platformPageViewMapper.selectVisitCountByTimeRange(startOfYesterday, startOfYesterday);
        backendIndexModelVO.setYesterdayVisitCount(yesterdayVisitCount);

        // 近7日访问量
        long startTime = startOfTodDay - 6 * DAY_SECOND;
        Integer sevenVisitCount = platformPageViewMapper.selectVisitCountByTimeRange(startTime, startOfTodDay);
        backendIndexModelVO.setSevenDayVisitCount(sevenVisitCount);

        // 近30日访问量
        startTime = startOfTodDay - 29 * DAY_SECOND;
        Integer thirtyVisitCount = platformPageViewMapper.selectVisitCountByTimeRange(startTime, startOfTodDay);
        backendIndexModelVO.setThirtyDayVisitCount(thirtyVisitCount);
    }
}
