/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics.impl;

import cn.shoptnt.mapper.statistics.MemberRegisterMapper;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.statistics.dto.MemberRegisterData;
import cn.shoptnt.service.statistics.MemberDataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 会员数据注入实现
 *
 * @author chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/6/22 下午10:12
 */
@Service
public class MemberDataManagerImpl implements MemberDataManager {

    @Autowired
    private MemberRegisterMapper memberRegisterMapper;

    /**
     * 会员注册
     *
     * @param member 会员
     */
    @Override
    public void register(Member member) {
        memberRegisterMapper.insert(new MemberRegisterData(member));
    }

}
