/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics.impl;

import cn.shoptnt.mapper.statistics.RefundDataMapper;
import cn.shoptnt.service.statistics.RefundDataManager;
import org.springframework.stereotype.Service;
import cn.shoptnt.model.statistics.dto.RefundData;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 退款变化业务实现类
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018/6/4 9:59
 */
@Service
public class RefundDataManagerImpl implements RefundDataManager {

    @Autowired
    private RefundDataMapper refundDataMapper;

    /**
     * 退款消息写入
     *
     * @param refundData 退货数据
     */
    @Override
    public void put(RefundData refundData) {
        //审核通过
        refundDataMapper.insert(refundData);
    }

}
