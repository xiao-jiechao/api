/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.security.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.client.member.MemberWalletClient;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.member.dos.MemberWalletDO;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.service.security.BaseSignScanTask;
import cn.shoptnt.model.security.ScanResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 会员钱包钱包签名数据扫描
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/22 10:22
 **/
@Service("memberWalletSignScanTask")
public class MemberWalletSignScanTask extends BaseSignScanTask<MemberWalletDO> {

    @Autowired
    private MemberWalletClient memberWalletClient;

    @Override
    protected ScanResult scanModule(String rounds) throws IllegalAccessException {
        QueryWrapper<MemberWalletDO> queryWrapper = createWrapper(rounds);
        ScanModuleDTO<MemberWalletDO> scanModuleDTO = new ScanModuleDTO<>(rounds, queryWrapper, pageSize);
        return memberWalletClient.scanModule(scanModuleDTO);
    }

    @Override
    public void reSign() {
        memberWalletClient.reSign();
    }

}
