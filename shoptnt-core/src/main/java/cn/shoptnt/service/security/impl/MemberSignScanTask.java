/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.security.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.client.member.MemberClient;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.service.security.BaseSignScanTask;
import cn.shoptnt.model.security.ScanResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 会员签名数据扫描
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/22 10:22
 **/
@Service("memberSignScanTask")
public class MemberSignScanTask extends BaseSignScanTask<Member> {

    @Autowired
    private MemberClient memberClient;

    @Override
    protected ScanResult scanModule(String rounds) throws IllegalAccessException {

        QueryWrapper<Member> queryWrapper = createWrapper(rounds);
        ScanModuleDTO<Member> scanModuleDTO = new ScanModuleDTO<>(rounds, queryWrapper, pageSize);
        return memberClient.scanModule(scanModuleDTO);
    }

    @Override
    public void reSign() {
        memberClient.reSign();
    }
}
