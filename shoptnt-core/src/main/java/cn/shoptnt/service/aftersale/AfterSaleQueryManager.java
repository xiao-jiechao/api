/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.aftersale;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.aftersale.dos.AfterSaleExpressDO;
import cn.shoptnt.model.aftersale.dos.AfterSaleServiceDO;
import cn.shoptnt.model.aftersale.dto.AfterSaleOrderDTO;
import cn.shoptnt.model.aftersale.dto.AfterSaleQueryParam;
import cn.shoptnt.model.aftersale.vo.AfterSaleExportVO;
import cn.shoptnt.model.aftersale.vo.ApplyAfterSaleVO;
import cn.shoptnt.model.goods.enums.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 售后服务查询管理接口
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-12-04
 */
public interface AfterSaleQueryManager extends IService<AfterSaleServiceDO> {

    /**
     * 获取申请售后服务记录列表
     * @param param 查询条件
     * @return
     */
    WebPage list(AfterSaleQueryParam param);

    /**
     * 获取申请售后页面订单收货信息和商品信息
     * @param orderSn 订单ID
     * @param skuId 商品skuID
     * @return
     */
    AfterSaleOrderDTO applyOrderInfo(String orderSn, Long skuId);

    /**
     * 获取售后服务详细
     * @param serviceSn 售后服务单号
     * @param permission 权限
     * @return
     */
    ApplyAfterSaleVO detail(String serviceSn, Permission permission);

    /**
     * 根据条件导出售后服务信息
     * @param param 导出条件信息
     * @return
     */
    List<AfterSaleExportVO> exportAfterSale(AfterSaleQueryParam param);

    /**
     * 获取还未完成的售后服务
     * @param memberId 会员id
     * @param sellerId 商家id
     * @return
     */
    Integer getAfterSaleCount(Long memberId, Long sellerId);

    /**
     * 根据售后服务单编号获取售后服务单信息
     * @param serviceSn 售后服务单编号
     * @return
     */
    AfterSaleServiceDO getService(String serviceSn);

    /**
     * 根据订单编号获取取消订单售后服务信息
     * @param orderSn 订单编号
     * @return
     */
    AfterSaleServiceDO getCancelService(String orderSn);

    /**
     * 根据售后服务单号获取物流相关信息
     * @param serviceSn 售后服务单编号
     * @return
     */
    AfterSaleExpressDO getExpress(String serviceSn);

    /**
     * 查询待处理售后数量
     * @return 待处理售后数量
     */
    Integer getWaitHandleCount();
}
