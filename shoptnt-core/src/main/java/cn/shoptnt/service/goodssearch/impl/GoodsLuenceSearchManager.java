package cn.shoptnt.service.goodssearch.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.shoptnt.client.system.HotkeywordClient;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import cn.shoptnt.framework.util.HexUtils;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.model.goods.dos.CategoryDO;
import cn.shoptnt.model.goods.vo.GoodsSearchResultVO;
import cn.shoptnt.model.goodssearch.GoodsIndex;
import cn.shoptnt.model.goodssearch.GoodsSearchDTO;
import cn.shoptnt.model.pagedata.HotKeyword;
import cn.shoptnt.model.shop.dos.ShopCatDO;
import cn.shoptnt.service.goods.CategoryManager;
import cn.shoptnt.service.goods.util.Separator;
import cn.shoptnt.service.goodssearch.GoodsSearchManager;
import cn.shoptnt.service.goodssearch.util.SortContainer;
import cn.shoptnt.service.shop.ShopCatManager;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.DoublePoint;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.util.*;

/**
 * @author kingapex
 * @version 1.0
 * @data 2022/10/19 15:58
 **/
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class GoodsLuenceSearchManager implements GoodsSearchManager {

    @Autowired
    protected CategoryManager categoryManager;
    @Autowired
    private DirectMessageSender messageSender;
    @Autowired
    protected ShopCatManager shopCatManager;
    @Autowired
    protected HotkeywordClient hotkeywordClient;

    @Override
    public WebPage recommendGoodsList(GoodsSearchDTO goodsSearch) throws IOException{
        List<HotKeyword> hotKeywords = hotkeywordClient.listByNum(1);
        String keywords = "";
        if(StringUtil.isNotEmpty(hotKeywords)){
            keywords = hotKeywords.get(0).getHotName();
        }
        goodsSearch.setKeyword(keywords);
        return searchGoodsAndSelector(goodsSearch).getGoodsData();
    }

    @Override
    public GoodsSearchResultVO searchGoodsAndSelector(GoodsSearchDTO goodsSearch) throws IOException {
        // 承载返回结果
        GoodsSearchResultVO goodsSearchResult = new GoodsSearchResultVO();
        // 构建分页条件数据
        int pageNo = goodsSearch.getPageNo().intValue();
        int pageSize = goodsSearch.getPageSize().intValue();
        int start = (pageNo - 1) * pageSize;
        int end = start + pageSize;
        // 获取indexReader
        IndexReader indexReader = LuceneFactory.getIndexReader();
        // 获取indexSearcher
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        // 构建查询条件
        BooleanQuery query = createQuery(goodsSearch);
        //如果不为空 则表示关键词搜索
        if (!StringUtil.isEmpty(goodsSearch.getKeyword())) {
//            //搜索关键字消息
//            GoodsSearchMessage goodsSearchMessage = new GoodsSearchMessage(goodsSearch.getKeyword());
//            this.messageSender.send(goodsSearchMessage);
        }
        // 构建排序条件
        Sort sort = createSort(goodsSearch);
        // 承载查询结果数据
        List<GoodsIndex> resultList = new ArrayList<>();
        // 进行查询
        TopDocs search = indexSearcher.search(query, end, sort);
        // 构建返回结果数据
        for (int i = start; i < end; i++) {
            if (i > search.scoreDocs.length - 1) {
                break;
            }
            Document document = indexReader.document(search.scoreDocs[i].doc);
            GoodsIndex goodsIndex = docmentToGoodsIndex(document);
            resultList.add(goodsIndex);
        }
        WebPage webPage = new WebPage(goodsSearch.getPageNo(), search.totalHits.value, goodsSearch.getPageSize(), resultList);
        goodsSearchResult.setGoodsData(webPage);
        goodsSearchResult.setSelectorData(Collections.emptyMap());
        return goodsSearchResult;
    }

    /**
     * 构建查询条件
     *
     * @param goodsSearch
     * @return
     */
    public BooleanQuery createQuery(GoodsSearchDTO goodsSearch) {
        String keyword = goodsSearch.getKeyword();
        Long brandId = goodsSearch.getBrand();
        Long categoryId = goodsSearch.getCategory();
        Long sellerId = goodsSearch.getSellerId();
        Long shopCatId = goodsSearch.getShopCatId();
        String price = goodsSearch.getPrice();

        BooleanQuery.Builder builder = new BooleanQuery.Builder();

        // 商品名称 查询
        if (ObjectUtil.isNotEmpty(keyword)) {
            Query queryName = new TermQuery(new Term("name", keyword));
            builder.add(queryName, BooleanClause.Occur.MUST);
        }
        // 品牌ID 查询
        if (ObjectUtil.isNotEmpty(brandId)) {
            Query queryBrand = new TermQuery(new Term("brand", brandId.toString()));
            builder.add(queryBrand, BooleanClause.Occur.MUST);
        }
        // 分类ID 查询
        if (ObjectUtil.isNotEmpty(categoryId)) {
            CategoryDO category = categoryManager.getModel(categoryId);
            if (category == null) {
                throw new ServiceException("", "该分类不存在");
            }
            Query queryCategory = new WildcardQuery(new Term("categoryPath", HexUtils.encode(category.getCategoryPath()) + "*"));
            builder.add(queryCategory, BooleanClause.Occur.MUST);
        }
        // 卖家ID 查询
        if (ObjectUtil.isNotEmpty(sellerId)) {
            Query querySellerId = new TermQuery(new Term("sellerId", sellerId.toString()));
            builder.add(querySellerId, BooleanClause.Occur.MUST);
        }
        // 卖家分组 查询
        if (ObjectUtil.isNotEmpty(shopCatId)) {
            ShopCatDO shopCat = shopCatManager.getModel(shopCatId);
            if (shopCat == null) {
                throw new ServiceException("", "该分组不存在");
            }
            Query queryShopCatPath = new WildcardQuery(new Term("shopCatPath", HexUtils.encode(shopCat.getCatPath()) + "*"));
            builder.add(queryShopCatPath, BooleanClause.Occur.MUST);
        }

        if (ObjectUtil.isNotEmpty(price)) {
            String[] prices = price.split(Separator.SEPARATOR_PROP_VLAUE);
            Double min = StringUtil.toDouble(prices[0], 0.0) ;
            Double  max = StringUtil.toDouble(prices[1], 0.0);
            // 价格区间检索
            Query priceQuery = DoublePoint.newRangeQuery("priceRange", min, max);
            builder.add(priceQuery, BooleanClause.Occur.MUST);
        }
        // 删除的商品不显示
        builder.add(new TermQuery(new Term("disabled", "1")), BooleanClause.Occur.MUST);
        // 未上架的商品不显示
        builder.add(new TermQuery(new Term("marketEnable", "1")), BooleanClause.Occur.MUST);
        // 待审核和审核不通过的商品不显示
        builder.add(new TermQuery(new Term("isAuth", "1")), BooleanClause.Occur.MUST);

        return builder.build();
    }

    public Sort createSort(GoodsSearchDTO goodsSearch) {
        Sort sort = new Sort();
        // 排序
        String sortField = goodsSearch.getSort();
        String sortId = "priorityOrder";
        if (!ObjectUtils.isEmpty(sortField)) {
            Map<String, String> sortMap = SortContainer.getSort(sortField);
            // 价格排序时，不需要矫正 sortId 对应字段，即sortMap.getById("id") = "price"
            sortId = sortMap.get("id");
            // 默认排序 - 优先级排序
            if ("def".equals(sortId)) {
                sortId = "priorityOrder";
            }
            // 销量 - 销量排序
            if ("buynum".equals(sortId)) {
                sortId = "buyCountOrder";
            }
            // 正序倒序，new Sort("排序字段", "字段类型", boolean reverse) reverse: true升序/false降序
            boolean sortType = "desc".equals(sortMap.get("def_sort"));
            sort = new Sort(new SortField(sortId, SortField.Type.LONG, sortType));
            // 价格排序
            if ("price".equals(sortId)) {
                sort = new Sort(new SortField("priceOrder", SortField.Type.DOUBLE, sortType));
            }
            // 好评率
            if ("grade".equals(sortId)) {
                sort = new Sort(new SortField("commentNumOrder", SortField.Type.LONG, sortType), new SortField("buyCountOrder", SortField.Type.LONG, sortType));
            }
            //如果不是默认排序，则在原有搜索结果基础上加上商品优先级排序
            if (!"priority".equals(sortId)) {
                // 防止 UnsupportedOperationException
                List<SortField> sortFieldList = new ArrayList<>(Arrays.asList(sort.getSort()));
                sortFieldList.add(new SortField("priorityOrder", SortField.Type.LONG, false));
                SortField[] sortFields = new SortField[sortFieldList.size()];
                SortField[] resultFields = sortFieldList.toArray(sortFields);
                sort = new Sort(resultFields);
            }
        }
        return sort;
    }

    /**
     * 将doc文档转化为goodsIndex
     *
     * @param document
     * @return
     */
    public GoodsIndex docmentToGoodsIndex(Document document) {
        GoodsIndex goodsIndex = new GoodsIndex();
        goodsIndex.setGoodsId(Long.valueOf(document.get("goodsId")));
        goodsIndex.setName(document.get("name"));
        goodsIndex.setThumbnail(document.get("thumbnail"));
        goodsIndex.setSmall(document.get("small"));
        goodsIndex.setBuyCount(Integer.valueOf(document.get("buyCount")));
        goodsIndex.setSellerId(Long.valueOf(document.get("sellerId")));
        goodsIndex.setSellerName(document.get("sellerName"));
        goodsIndex.setShopCatId(Long.valueOf(document.get("shopCatId")));
        goodsIndex.setShopCatPath(document.get("shopCatPath"));
        goodsIndex.setCommentNum(StringUtil.toInt(document.get("commentNum")));
        goodsIndex.setGrade(Double.parseDouble(document.get("grade")));
        goodsIndex.setDiscountPrice(Double.parseDouble(document.get("discountPrice")));
        goodsIndex.setPrice(Double.parseDouble(document.get("price")));
        goodsIndex.setBrand(StringUtil.toLong(document.get("brand"), 0));
        goodsIndex.setCategoryId(Long.valueOf(document.get("categoryId")));
        goodsIndex.setCategoryPath(document.get("categoryPath"));
        goodsIndex.setDisabled(StringUtil.toInt(document.get("disabled")));
        goodsIndex.setMarketEnable(StringUtil.toInt(document.get("marketEnable"), 0));
        goodsIndex.setIsAuth(StringUtil.toInt(document.get("isAuth"), 0));
        goodsIndex.setIntro(document.get("intro"));
        goodsIndex.setPriority(StringUtil.toInt(document.get("priority"), 0));
        goodsIndex.setSelfOperated(StringUtil.toInt(document.get("selfOperated"), 0));
        return goodsIndex;
    }


}
