/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.deposite;

import cn.shoptnt.model.member.dto.DepositeParamDTO;
import cn.shoptnt.model.trade.deposite.DepositeLogDO;
import cn.shoptnt.framework.database.WebPage;

/**
*
* @description: 预存款日志业务类
* @author: liuyulei
* @create: 2019/12/30 17:41
* @version:1.0
* @since:7.1.5
**/
public interface DepositeLogManager {


    /**
     * 添加日志
     * @param logDO  日志实体
     */
    void add(DepositeLogDO logDO);


    /**
     * 获取日志列表  分页
     * @param paramDTO
     * @return 日志分页数据
     */
    WebPage list(DepositeParamDTO paramDTO);
}
