/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.converter;

import cn.shoptnt.model.goods.vo.CacheGoods;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.model.trade.cart.vo.TradeConvertGoodsSkuVO;
import cn.shoptnt.model.trade.cart.vo.TradeConvertGoodsVO;
import cn.shoptnt.framework.util.BeanUtil;


/**
 * 转换goods包下，此包使用到的model及字段
 *
 * @author Snow create in 2018/3/20
 * @version v2.0
 * @since v7.0.0
 */
public class TradeGoodsConverter {

    /**
     * 转换goodsVO对象
     * @return
     */
    public static TradeConvertGoodsVO goodsVOConverter(CacheGoods goods){
        TradeConvertGoodsVO goodsVO = new TradeConvertGoodsVO();
        goodsVO.setIsAuth(goods.getIsAuth());
        goodsVO.setTemplateId(goods.getTemplateId());
        goodsVO.setLastModify(goods.getLastModify());
        return goodsVO;
    }

    /**
     * 转换goodsSkuVO对象
     * @return
     */
    public static TradeConvertGoodsSkuVO goodsSkuVOConverter(GoodsSkuVO goodsSkuVO){
        TradeConvertGoodsSkuVO skuVO = new TradeConvertGoodsSkuVO();
        BeanUtil.copyProperties(goodsSkuVO,skuVO);
        return skuVO;
    }
}
