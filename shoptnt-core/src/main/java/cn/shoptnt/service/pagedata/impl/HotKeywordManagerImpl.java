/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.pagedata.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.mapper.system.HotKeywordMapper;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.pagedata.HotKeyword;
import cn.shoptnt.service.pagedata.HotKeywordManager;
import cn.shoptnt.model.errorcode.SystemErrorCode;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 热门关键字业务类
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-04 10:43:23
 */
@Service
public class HotKeywordManagerImpl implements HotKeywordManager {

    @Autowired
    private DirectMessageSender messageSender;
    @Autowired
    private Cache cache;
    @Autowired
    private HotKeywordMapper hotKeywordMapper;

    @Override
    public WebPage list(long page, long pageSize) {

        QueryWrapper<HotKeyword> wrapper = new QueryWrapper<>();
        IPage<HotKeyword> iPage = hotKeywordMapper.selectPage(new Page<>(page,pageSize), wrapper);
        return PageConvert.convert(iPage);
    }

    @Override
    @Transactional(value = "systemTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public HotKeyword add(HotKeyword hotKeyword) {

        hotKeywordMapper.insert(hotKeyword);

        this.sendMessage();
        return hotKeyword;
    }

    @Override
    @Transactional(value = "systemTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public HotKeyword edit(HotKeyword hotKeyword, Long id) {

        HotKeyword keyword = this.getModel(id);
        if (keyword == null) {
            throw new ServiceException(SystemErrorCode.E954.code(), "该记录不存在，请正确操作");
        }

        hotKeywordMapper.updateById(hotKeyword);

        this.sendMessage();

        return hotKeyword;
    }

    @Override
    @Transactional(value = "systemTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(Long id) {
        hotKeywordMapper.deleteById(id);
        this.sendMessage();
    }

    @Override
    public HotKeyword getModel(Long id) {
        return hotKeywordMapper.selectById(id);
    }

    @Override
    public List<HotKeyword> listByNum(Integer num) {
        List<HotKeyword> result = (List<HotKeyword>) cache.get(CachePrefix.HOT_KEYWORD.getPrefix() + num);

        if (result == null || result.isEmpty()) {
            if (num == null) {
                num = 5;
            }

            result = hotKeywordMapper.queryForLimit(num);

            cache.put(CachePrefix.HOT_KEYWORD.getPrefix() + num, result);
        }

        return result;
    }

    /**
     * 发送首页变化消息
     */
    private void sendMessage() {

        //使用模糊删除
        this.cache.vagueDel(CachePrefix.HOT_KEYWORD.getPrefix());
    }
}
