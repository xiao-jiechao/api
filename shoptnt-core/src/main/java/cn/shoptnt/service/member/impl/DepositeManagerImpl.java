/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.member.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import cn.shoptnt.client.trade.DepositeLogClient;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.exception.SystemErrorCodeV1;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.member.MemberWalletMapper;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.SceneType;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.goods.dos.GoodsSkuDO;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberWalletDO;
import cn.shoptnt.model.member.vo.MemberDepositeVO;
import cn.shoptnt.model.trade.deposite.DepositeLogDO;
import cn.shoptnt.service.member.DepositeManager;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.passport.PassportManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * 会员预存款业务类
 * @author liuyulei
 * @version v1.0
 * @since v7.2.0
 * 2019-12-30 16:24:51
 */
@Service
public class DepositeManagerImpl implements DepositeManager {

	@Autowired
	private MemberWalletMapper memberWalletMapper;

	@Autowired
	private MemberManager memberManager;

	@Autowired
	private DepositeLogClient depositeLogClient;

	@Autowired
	private PassportManager passportManager;

	@Autowired
	private Cache cache;

	@Override
	@Transactional(value = "memberTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public Boolean increase(Double money, Long memberId, String detail) {
		//根据会员ID获取会员预存款信息
		MemberWalletDO wallet = this.getModel(memberId);

		//新建修改条件包装器
		UpdateWrapper<MemberWalletDO> updateWrapper = new UpdateWrapper<>();
		//设置要修改的内容
		updateWrapper.setSql("pre_deposite = pre_deposite + " + money);
		//以会员ID为修改条件
		updateWrapper.eq("member_id", memberId);
		//修改会员预存款信息并获取修改成功数量
		Integer count = memberWalletMapper.update(new MemberWalletDO(), updateWrapper);
		//如果充值成功，则添加充值日志
		if(count == 1){
			DepositeLogDO logDO = new DepositeLogDO(wallet.getMemberId(),wallet.getMemberName(),money,detail);
			depositeLogClient.add(logDO);

			return true;
		}
		return false;

	}

	@Override
	@Transactional(value = "memberTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public Boolean reduce(Double money, Long memberId, String detail) {
		//根据会员ID获取会员预存款信息
		MemberWalletDO wallet = this.getModel(memberId);
        double preDeposite = CurrencyUtil.sub(wallet.getPreDeposite(), money);
        //修改会员预存款余额并获取修改成功数量
		boolean result = new UpdateChainWrapper<>(memberWalletMapper)
				.set("pre_deposite", preDeposite)
				.eq("member_id", memberId)
				.update();
		//如果扣减成功，则添加扣减日志
		if(result){
			DepositeLogDO logDO = new DepositeLogDO(wallet.getMemberId(),wallet.getMemberName(),-money,detail);
			depositeLogClient.add(logDO);

			return true;
		}
		return false;
	}

	@Override
	@Transactional(value = "memberTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public void setDepositePwd(Long memberId,String password) {
		//根据会员ID获取会员信息
		Member member = this.memberManager.getModel(memberId);
		//校验当前会员是否存在
		if (member == null) {
			throw new ResourceNotFoundException("当前会员不存在");
		}

		//校验当前会员是否可用
		member.checkDisable();

		//将余额支付密码进行MD5加密
		String pwd = StringUtil.md5(password.toLowerCase() + member.getUname().toLowerCase());
		//新建修改条件包装器
		UpdateChainWrapper<MemberWalletDO> wrapper = new UpdateChainWrapper<>(memberWalletMapper);
		//设置修改密码
		wrapper.set("deposite_password", pwd);
		//以会员ID为修改条件
		wrapper.eq("member_id", memberId);
		//修改会员预存款信息
		wrapper.update();

		//清除步骤标记缓存
		passportManager.clearSign(member.getMobile(), SceneType.SET_PAY_PWD.name());
	}

	@Override
	public void checkPwd(Long memberId,String password) {
		//根据会员ID获取会员信息
		Member member = this.memberManager.getModel(memberId);
		//密码设置方式  为会员用户名+md5(密码)
		String pwd = StringUtil.md5(password.toLowerCase() + member.getUname().toLowerCase());
		//根据会员ID获取会员预存款信息
		MemberWalletDO walletDO = this.getModel(memberId);
		if(!walletDO.getDepositePassword().equals(pwd)){
			throw new ServiceException(MemberErrorCode.E107.name(),"支付密码错误！");
		}
	}


	@Override
	@Transactional(value = "memberTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public MemberWalletDO add(MemberWalletDO wallet)	{
		memberWalletMapper.insert(wallet);
		return wallet;
	}

	@Override
	@Transactional(value = "memberTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public MemberWalletDO edit(MemberWalletDO memberWallet, Long id){
		memberWallet.setId(id);
		memberWalletMapper.updateById(memberWallet);
		return memberWallet;
	}

	@Override
	@Transactional(value = "memberTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public	void delete( Long id)	{
		memberWalletMapper.deleteById(id);
	}

	@Override
	public MemberWalletDO getModel(Long memberId)	{
		//新建查询条件包装器
		QueryWrapper<MemberWalletDO> wrapper = new QueryWrapper<>();
		//以会员ID为查询条件
		wrapper.eq("member_id", memberId);
		return memberWalletMapper.selectOne(wrapper);
	}

	@Override
	public Double getDeposite(Long memberId) {
		MemberWalletDO walletDO = this.getModel(memberId);
		//获取会员预存款余额
		return walletDO.getPreDeposite();

	}

	/**
	 * 获取预存款抵扣参数
	 * @param memberId 会员ID
	 * @return
	 */
	@Override
	public MemberDepositeVO getDepositeVO(Long memberId) {
		MemberDepositeVO memberDepositeVO = new MemberDepositeVO();
		MemberWalletDO memberWalletDO =  this.getModel(memberId);
		if (memberWalletDO == null) {
			Member member = memberManager.getModel(memberId);

			memberWalletDO = new MemberWalletDO();
			memberWalletDO.setMemberId(memberId);
			memberWalletDO.setMemberName(member.getUname());

			this.add(memberWalletDO);
		}
		memberDepositeVO.setBalance(memberWalletDO.getPreDeposite());
		memberDepositeVO.setIsPwd(!"-1".equals(memberWalletDO.getDepositePassword()));
		memberDepositeVO.setIsUsed(false);
		return memberDepositeVO;
	}
}
