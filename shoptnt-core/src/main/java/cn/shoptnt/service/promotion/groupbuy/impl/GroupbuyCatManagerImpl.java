/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.promotion.groupbuy.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.mapper.promotion.groupbuy.GroupbuyCatMapper;
import cn.shoptnt.model.errorcode.PromotionErrorCode;
import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyCatDO;
import cn.shoptnt.service.promotion.groupbuy.GroupbuyCatManager;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.shoptnt.framework.database.WebPage;

import java.util.List;

/**
 * 团购分类业务类
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 16:08:03
 */
@Service
public class GroupbuyCatManagerImpl implements GroupbuyCatManager {

    @Autowired
    private GroupbuyCatMapper groupbuyCatMapper;

    /**
     * 读取团购分类——分页
     * @param pageNo 页数
     * @param pageSize 每页数量
     * @return
     */
    @Override
    public WebPage list(Long pageNo, Long pageSize) {
        //获取团购分类分页列表数据
        IPage<GroupbuyCatDO> iPage = groupbuyCatMapper.selectPage(new Page<>(pageNo, pageSize), new QueryWrapper<>());
        return PageConvert.convert(iPage);
    }

    /**
     * 查询团购分类列表
     * @param parentId 分类父id
     * @return WebPage
     */
    @Override
    public List<GroupbuyCatDO> getList(Long parentId) {
        //新建查询条件包装器
        QueryWrapper<GroupbuyCatDO> wrapper = new QueryWrapper<>();
        //以分类父ID为条件查询
        wrapper.eq("parent_id", parentId);
        //以分类排序正序查询
        wrapper.orderByAsc("cat_order");
        //获取团购分类信息集合
        List<GroupbuyCatDO> list = groupbuyCatMapper.selectList(wrapper);
        return list;
    }

    /**
     * 添加团购分类
     * @param groupbuyCat 团购分类
     * @return GroupbuyCat 团购分类
     */
    @Override
    public GroupbuyCatDO add(GroupbuyCatDO groupbuyCat) {
        //新建查询条件包装器
        QueryWrapper<GroupbuyCatDO> wrapper = new QueryWrapper<>();
        //以分类名称为条件查询
        wrapper.eq("cat_name", groupbuyCat.getCatName());
        //获取团购分类信息集合
        List list = groupbuyCatMapper.selectList(wrapper);
        //如果结果集长度大于0，证明当前这个分类已存在
        if (list.size() > 0) {
            throw new ServiceException(PromotionErrorCode.E408.code(), "团购分类名称重复");
        }
        //如果分类父id为空，默认设置为0
        if (groupbuyCat.getParentId() == null) {
            groupbuyCat.setParentId(0L);
        }
        //团购分类信息入库
        groupbuyCatMapper.insert(groupbuyCat);
        return groupbuyCat;
    }

    /**
     * 修改团购分类
     * @param groupbuyCat 团购分类
     * @param id 团购分类主键
     * @return GroupbuyCat 团购分类
     */
    @Override
    public GroupbuyCatDO edit(GroupbuyCatDO groupbuyCat, Long id) {
        //新建查询条件包装器
        QueryWrapper<GroupbuyCatDO> wrapper = new QueryWrapper<>();
        //以分类名称为条件查询
        wrapper.eq("cat_name", groupbuyCat.getCatName());
        //排除当前这个分类的ID
        wrapper.ne("cat_id", id);
        //获取团购分类信息集合
        List list = groupbuyCatMapper.selectList(wrapper);
        //如果结果集长度大于0，证明当前这个分类已存在
        if (list.size() > 0) {
            throw new ServiceException(PromotionErrorCode.E408.code(), "团购分类名称重复");
        }

        //设置主键ID
        groupbuyCat.setCatId(id);
        //修改团购分类信息
        groupbuyCatMapper.updateById(groupbuyCat);
        return groupbuyCat;
    }

    /**
     * 删除团购分类
     * @param id 团购分类主键
     */
    @Override
    public void delete(Long id) {
        //检查团购分类是否可以被删除
        if (!this.checkCat(id)) {
            throw new ServiceException(PromotionErrorCode.E408.code(), "当前有正在进行或还未开始的团购活动商品关联了此分类，不可删除");
        }
        //删除团购分类信息
        groupbuyCatMapper.deleteById(id);
    }

    /**
     * 获取团购分类
     * @param id 团购分类主键
     * @return GroupbuyCat  团购分类
     */
    @Override
    public GroupbuyCatDO getModel(Long id) {
        //根据ID获取团购分类信息并返回
        return groupbuyCatMapper.selectById(id);
    }

    /**
     * 检查团购分类是否可以被删除
     * @param catId 分类id
     * @return
     */
    private boolean checkCat(Long catId) {
        //查询团购分类已被团购活动或者团购商品占用的数量
        int count = groupbuyCatMapper.selectCatCount(catId, DateUtil.getDateline());
        boolean flag = count == 0 ? true : false;
        return  flag;
    }
}
