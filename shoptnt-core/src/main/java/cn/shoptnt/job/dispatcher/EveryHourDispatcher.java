package cn.shoptnt.job.dispatcher;

import cn.shoptnt.job.EveryHourExecute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 每小时执行定时任务
 *
 * @author LiuXT
 * @date 2022-10-23
 * @since v7.3.0
 */
@Service
public class EveryHourDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<EveryHourExecute> everyHourExecutes;

    public Boolean dispatch() {
        boolean result = true;
        if (everyHourExecutes != null) {
            for (EveryHourExecute everyHourExecute : everyHourExecutes) {
                try {
                    everyHourExecute.everyHour();
                } catch (Exception e) {
                    logger.error("每小时任务异常：", e);
                    result = false;
                }
            }
        }
        return result;
    }
}
