/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.standlone;

import cn.shoptnt.job.dispatcher.EveryHourDispatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 每小时执行定时任务
 *
 * @author LiuXT
 * @date 2022-10-23
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class EveryHourSchedule {

    @Autowired
    private EveryHourDispatcher everyHourDispatcher;

    /**
     * 定时器定义，设置执行时间
     * cron: 每小时整点执行
     */
    @Scheduled(cron = "0 0 */1 * * ?")
    public void execute() {
        everyHourDispatcher.dispatch();
    }
}
