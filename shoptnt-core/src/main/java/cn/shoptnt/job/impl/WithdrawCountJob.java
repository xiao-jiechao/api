/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.impl;

import cn.shoptnt.job.EveryDayExecute;
import cn.shoptnt.client.distribution.WithdrawCountClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 可提现金额计算
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/23 上午7:46
 */
@Service
public class WithdrawCountJob implements EveryDayExecute{


    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private WithdrawCountClient withdrawCountClient;


    /**
     * 每天执行结算
     */
    @Override
    public void everyDay() {
        try {
            withdrawCountClient.everyDay();
        } catch (Exception e) {
            logger.error("每日将解锁金额自动添加到可提现金额异常：",e);
        }
    }
}
