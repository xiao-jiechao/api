/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.impl;

import cn.shoptnt.client.system.SignScanClient;
import cn.shoptnt.job.EveryDayExecute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 数据签名扫描任务
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/23 17:20
 **/
@Component
public class DataSignScanJob implements EveryDayExecute {

    @Autowired
    private SignScanClient signScanClient;

    @Override
    public void everyDay() {
        signScanClient.scan();
    }
}
