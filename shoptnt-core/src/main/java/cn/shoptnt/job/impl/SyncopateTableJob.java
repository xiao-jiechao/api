/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.impl;

import cn.shoptnt.job.EveryDayExecute;
import cn.shoptnt.client.statistics.SyncopateTableClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 分表任务每日执行
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-16 下午4:17
 */
@Component
public class SyncopateTableJob implements EveryDayExecute {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SyncopateTableClient syncopateTableClient;

    /**
     * 每年执行
     */
    @Override
    public void everyDay() {
        try {
            syncopateTableClient.everyDay();
        } catch (Exception e) {
            logger.error("分表业务执行异常：", e);
        }

    }
}
