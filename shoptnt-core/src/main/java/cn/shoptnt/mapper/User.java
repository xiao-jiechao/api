/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2020/7/1
 */
public class User  implements Serializable {

    private static final long serialVersionUID = 8855072521423508063L;

    @TableId(type= IdType.ASSIGN_ID)
    private Long id;


    private String name;
    private Integer age;

    private String email;



    protected String scanRounds;



    public String getScanRounds() {
        return scanRounds;
    }

    public void setScanRounds(String scanRounds) {
        this.scanRounds = scanRounds;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }

    public Long getId() {
//        MybatisPlusAutoConfiguration
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
