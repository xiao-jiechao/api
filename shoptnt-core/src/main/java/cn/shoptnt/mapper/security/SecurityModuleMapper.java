/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.security;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.security.SecurityModule;

public interface SecurityModuleMapper extends BaseMapper<SecurityModule> {

}
