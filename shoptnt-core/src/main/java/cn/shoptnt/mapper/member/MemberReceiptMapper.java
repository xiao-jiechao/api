/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.member;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.member.dos.MemberReceipt;

/**
 * 会员发票信息缓存Mapper接口
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.2
 * 2020-07-27
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface MemberReceiptMapper extends BaseMapper<MemberReceipt> {
}
