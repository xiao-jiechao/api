/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.member;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.member.dos.MemberShopScore;
import cn.shoptnt.model.member.dto.MemberShopScoreDTO;

import java.util.List;

/**
 * 店铺评分实体Mapper接口
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.2
 * 2020-07-23
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface MemberShopScoreMapper extends BaseMapper<MemberShopScore> {

    /**
     * 查询每个店铺的评分集合
     * @return
     */
    List<MemberShopScoreDTO> selectScoreDto();

}
