/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.distribution;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.distribution.dos.UpgradeLogDO;

/**
 * 升级日志的Mapper
 * @author zhanghao
 * @version v1.0
 * @since v7.2.2
 * 2020/8/5
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface UpgradeLogMapper extends BaseMapper<UpgradeLogDO> {
}
