/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.trade.pintuan;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.promotion.pintuan.PintuanChildOrder;
import cn.shoptnt.model.promotion.pintuan.PintuanOrder;

/**
 * PintuanChildOrderMapper接口
 * @author fk
 * @version v1.0
 * @since v7.2.2
 * 2020-08-10
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface PintuanChildOrderMapper extends BaseMapper<PintuanChildOrder> {
}
