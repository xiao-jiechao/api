/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.trade.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.trade.order.dos.OrderDO;

/**
 * 订单mapper
 * @author zs
 * @version v1.0
 * @since v7.2.2
 * 2020-08-08
 */
//不做二级缓存：因为要做会话级别的加解密，用的是会话秘钥，会导致多个秘钥污染同一片缓存数据
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface OrderMapper extends BaseMapper<OrderDO> {
}
