/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.trade.aftersale;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.aftersale.dos.AfterSaleRefundDO;

/**
 * 售后退款/退货信息Mapper接口
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.2
 * 2020-08-08
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface AfterSaleRefundMapper extends BaseMapper<AfterSaleRefundDO> {
}
