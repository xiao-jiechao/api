/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.goods;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.goods.vo.BackendGoodsVO;
import cn.shoptnt.model.goods.vo.BuyCountVO;
import cn.shoptnt.model.goods.vo.GoodsSelectLine;
import cn.shoptnt.model.goods.vo.GoodsSelectorSkuVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * GoodsMapper的Mapper
 * @author fk
 * @version 1.0
 * @since 7.1.0
 * 2020/7/21
 */
//不做二级缓存：因为要做会话级别的加解密，用的是会话秘钥，会导致多个秘钥污染同一片缓存数据
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface GoodsMapper extends BaseMapper<GoodsDO> {

    /**
     * 获取新赠商品
     *
     * @param length 获取的查询数量
     * @return
     */
    List<BackendGoodsVO> newGoods(@Param("length") Integer length);

    /**
     * 更新商品库存
     * @param param 条件
     */
    void updateQuantity(@Param("param") Map param);

    /**
     * 根据条件查询多个商品信息
     * @param queryWrapper 查询条件
     * @return
     */
    List<GoodsSelectLine> queryGoodsLines(@Param("ew") QueryWrapper<GoodsDO> queryWrapper);

    /**
     * 查询购买数量
     * @param goodsIds 商品id集合
     * @return
     */
    List<BuyCountVO> queryBuyCount(@Param("params")List<Long> goodsIds);

    /**
     * 查询sku集合的信息
     * @param skuIds skuid集合
     * @return
     */
    List<GoodsSelectLine> querySkus(@Param("params")List<Long> skuIds);

    /**
     * 查询sku分页
     * @param iPage 分页参数
     * @param wrapper 条件
     * @return
     */
    IPage<GoodsSelectorSkuVO> querySkusPage(IPage iPage,@Param("ew")QueryWrapper<GoodsDO> wrapper);
}
