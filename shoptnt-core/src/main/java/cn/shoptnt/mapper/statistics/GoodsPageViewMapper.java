/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.statistics;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.statistics.dos.GoodsPageView;

/**
 * 商品访问量统计Mapper
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/07/31
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface GoodsPageViewMapper extends BaseMapper<GoodsPageView> {

}
