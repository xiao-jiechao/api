/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade.impl;

import cn.shoptnt.client.trade.BillClient;
import cn.shoptnt.model.orderbill.dos.BillItem;
import cn.shoptnt.service.orderbill.BillItemManager;
import cn.shoptnt.service.orderbill.BillManager;
import cn.shoptnt.framework.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author fk
 * @version v1.0
 * @Description: 结算单对外接口实现
 * @date 2018/7/26 11:22
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class BillClientDefaultImpl implements BillClient {

    @Autowired
    private BillManager billManager;

    @Autowired
    private BillItemManager billItemManager;

    @Override
    public void createBills(Long startTime,Long endTime) {

        this.billManager.createBills(startTime,endTime);
    }

    @Override
    public BillItem add(BillItem billItem) {

        return this.billItemManager.add(billItem);
    }


    public static void main(String[] args) {
        Long[] time = DateUtil.getLastMonth();
        
        
    }

}
