/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade;

import cn.shoptnt.model.orderbill.dos.BillItem;

/**
 * @author fk
 * @version v2.0
 * @Description: 结算单对外接口
 * @date 2018/7/26 11:21
 * @since v7.0.0
 */
public interface BillClient {

    /**
     * 生成结算单
     * @param startTime
     * @param endTime
     */
    void createBills(Long startTime,Long endTime);

    /**
     * 添加结算单项表
     * @param billItem 结算单项表
     * @return BillItem 结算单项表
     */
    BillItem add(BillItem billItem);


}
