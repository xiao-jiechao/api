/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade.impl;

import cn.shoptnt.client.trade.PintuanGoodsClient;
import cn.shoptnt.service.trade.pintuan.PintuanGoodsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 拼团默认实现类
 *
 * @author zh
 * @version v7.0
 * @date 19/3/5 下午2:22
 * @since v7.0
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class PintuanGoodsClientImpl implements PintuanGoodsClient {

    @Autowired
    private PintuanGoodsManager pintuanGoodsManager;

    @Override
    public void delete(Long goodsId) {
        this.pintuanGoodsManager.delete(goodsId);
    }

    @Override
    public void deletePinTuanGoods(List<Long> delSkuIds) {
        pintuanGoodsManager.deletePinTuanGoods(delSkuIds);
    }
}
