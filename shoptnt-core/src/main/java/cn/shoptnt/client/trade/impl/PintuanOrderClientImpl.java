/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade.impl;

import cn.shoptnt.client.trade.PintuanOrderClient;
import cn.shoptnt.model.promotion.pintuan.PintuanChildOrder;
import cn.shoptnt.model.promotion.pintuan.PintuanOrderDetailVo;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.service.trade.pintuan.PintuanOrderManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class PintuanOrderClientImpl implements PintuanOrderClient {

    @Autowired
    private PintuanOrderManager pintuanOrderManager;


    @Override
    public PintuanOrderDetailVo getMainOrderBySn(String orderSn) {
        return pintuanOrderManager.getMainOrderBySn(orderSn);
    }

    @Override
    public void cancelOrder(String orderSn) {
        pintuanOrderManager.cancelOrder(orderSn);
    }

    @Override
    public void payOrder(OrderDO order) {
        pintuanOrderManager.payOrder(order);
    }

    @Override
    public List<PintuanChildOrder> queryChildOrderByOrderId(Long pintuanOrderId) {
        return pintuanOrderManager.getPintuanChild(pintuanOrderId);
    }
}
