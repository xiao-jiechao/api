/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.distribution.impl;

import cn.shoptnt.client.distribution.BillMemberClient;
import cn.shoptnt.model.distribution.dos.DistributionOrder;
import cn.shoptnt.model.distribution.dos.DistributionOrderDO;
import cn.shoptnt.service.distribution.BillMemberManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * BillMemberCliendDefaultImpl
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-14 下午2:00
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class BillMemberClientDefaultImpl implements BillMemberClient {

    @Autowired
    private BillMemberManager billMemberManager;

    /**
     * 购买商品产生的结算
     *
     * @param order
     */
    @Override
    public void buyShop(DistributionOrderDO order) {
        billMemberManager.buyShop(order);
    }

    /**
     * 退货商品产生的结算
     *
     * @param distributionOrder
     */
    @Override
    public void returnShop(DistributionOrder distributionOrder) {
        billMemberManager.returnShop(distributionOrder);
    }
}
