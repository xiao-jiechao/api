/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.client.system.LogiCompanyClient;
import cn.shoptnt.model.system.dos.LogisticsCompanyDO;
import cn.shoptnt.service.system.LogisticsCompanyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version v7.0
 * @Description:
 * @Author: zjp
 * @Date: 2018/7/26 14:18
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class LogiCompanyClientDefaultImpl implements LogiCompanyClient {

    @Autowired
    private LogisticsCompanyManager logisticsCompanyManager;

    @Override
    public LogisticsCompanyDO getLogiByCode(String code) {
        return logisticsCompanyManager.getLogiByCode(code);
    }

    @Override
    public LogisticsCompanyDO getModel(Long id) {
        return logisticsCompanyManager.getModel(id);
    }

    @Override
    public List<LogisticsCompanyDO> list() {
        return logisticsCompanyManager.list();
    }

    @Override
    public List<LogisticsCompanyDO> listAllNormal() {
        return logisticsCompanyManager.listAllNormal();
    }
}
