/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.goods;

/**
 * 商品标签对外接口
 *
 * @author zh
 * @version v7.0
 * @date 19/3/21 下午3:27
 * @since v7.0
 */
public interface TagClient {
    /**
     * 增加店铺标签
     *
     * @param sellerId 商家id
     */
    void addShopTags(Long sellerId);
}
