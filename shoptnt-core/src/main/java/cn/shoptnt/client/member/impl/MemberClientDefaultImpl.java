/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.client.member.MemberClient;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberCoupon;
import cn.shoptnt.model.member.dos.MemberPointHistory;
import cn.shoptnt.model.member.vo.BackendMemberVO;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.service.member.MemberCouponManager;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.member.MemberPointManager;
import cn.shoptnt.model.security.ScanResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员业务默认实现
 *
 * @author zh
 * @version v7.0
 * @date 18/7/27 上午11:52
 * @since v7.0
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class MemberClientDefaultImpl implements MemberClient {

    @Autowired
    private MemberManager memberManager;

    @Autowired
    private MemberPointManager memberPointManager;

    @Autowired
    private MemberCouponManager memberCouponManager;

    @Override
    public Member getModel(Long memberId) {
        return memberManager.getModel(memberId);
    }

    @Override
    public void loginNumToZero() {
        memberManager.loginNumToZero();
    }

    @Override
    public Member edit(Member member, Long id) {

        return memberManager.edit(member, id);
    }

    /**
     * 更新登录次数
     *
     * @param memberId
     * @param now
     * @return
     */
    @Override
    public void updateLoginNum(Long memberId, Long now) {
        memberManager.updateLoginNum(memberId, now);
    }

    @Override
    public void pointOperation(MemberPointHistory memberPointHistory) {

        memberPointManager.pointOperation(memberPointHistory);
    }

    @Override
    public List<String> queryAllMemberIds() {

        return memberManager.queryAllMemberIds();
    }

    @Override
    public MemberCoupon getModel(Long memberId, Long mcId) {
        return memberCouponManager.getModel(memberId, mcId);
    }

    @Override
    public void usedCoupon(Long mcId, String orderSn) {
        memberCouponManager.usedCoupon(mcId, orderSn);
    }

    @Override
    public void receiveBonus(Long memberId, String memberName, Long couponId) {

        memberCouponManager.receiveBonus(memberId, memberName, couponId);

    }

    @Override
    public List<BackendMemberVO> newMember(Integer length) {
        return memberManager.newMember(length);
    }

    @Override
    public ScanResult scanModule(ScanModuleDTO scanModuleDTO) {
        return memberManager.scanModule(scanModuleDTO);
    }

    @Override
    public void reSign() {
        memberManager.reSign();
    }

    @Override
    public void repair(Long memberId) {
        memberManager.repair(memberId);
    }

    @Override
    public void sleepMember(long timeStamp) {
        memberManager.sleepMember(timeStamp);
    }

    @Override
    public Integer getCoupon(Long memberId, Long couponId) {
        return memberCouponManager.getCoupon(memberId,couponId);
    }
}
