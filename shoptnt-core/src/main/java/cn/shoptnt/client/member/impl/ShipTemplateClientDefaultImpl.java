/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import cn.shoptnt.client.member.ShipTemplateClient;
import cn.shoptnt.model.shop.vo.ShipTemplateVO;
import cn.shoptnt.service.shop.ShipTemplateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version v7.0
 * @Description:
 * @Author: zjp
 * @Date: 2018/7/25 16:24
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class ShipTemplateClientDefaultImpl implements ShipTemplateClient {

    @Autowired
    private ShipTemplateManager shipTemplateManager;

    @Override
    public ShipTemplateVO get(Long id) {
        return shipTemplateManager.getFromCache(id);
    }

    @Override
    public List<String> getScripts(Long id) {


        return shipTemplateManager.getScripts(id);
    }

    @Override
    public void cacheShipTemplateScript(ShipTemplateVO shipTemplateVO) {

        this.shipTemplateManager.cacheShipTemplateScript(shipTemplateVO);
    }
}
