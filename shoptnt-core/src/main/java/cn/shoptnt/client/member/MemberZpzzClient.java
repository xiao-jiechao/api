/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member;

import cn.shoptnt.model.member.dos.MemberZpzzDO;

/**
 * 会员赠票资质
 *
 * @author zs
 * @version v7.0
 * @date 2021-12-21
 * @since v5.2.3
 */

public interface MemberZpzzClient {
    /**
     * 根据获取当前登录会员增票资质信息
     * @return
     */
    MemberZpzzDO get();
}
