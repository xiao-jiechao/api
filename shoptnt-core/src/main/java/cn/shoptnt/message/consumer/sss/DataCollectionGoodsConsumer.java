/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.sss;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.client.statistics.GoodsDataClient;
import cn.shoptnt.model.statistics.dto.GoodsData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 商品收藏更新
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-23 下午5:50
 */
@Component
public class DataCollectionGoodsConsumer {


    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private GoodsDataClient goodsDataClient;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.GOODS_COLLECTION_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.GOODS_COLLECTION_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void goodsCollectionChange(GoodsData goodsData) {
        try {
            //修改商品收藏数量
            goodsDataClient.updateCollection(goodsData);
        } catch (Exception e) {
            logger.error("商品收藏数量更新失败：", e);
        }
    }

}
