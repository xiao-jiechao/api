/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.message;

import cn.shoptnt.message.event.SmsSendMessageEvent;
import cn.shoptnt.model.base.vo.SmsSendVO;
import cn.shoptnt.client.system.SmsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 发送短信
 *
 * @author zjp
 * @version v7.0
 * @since v7.0
 * 2018年3月25日 下午3:15:01
 */
@Component
public class SmsMessageConsumer implements SmsSendMessageEvent {

    @Autowired
    private SmsClient smsClient;

    @Override
    public void send(SmsSendVO smsSendVO) {
        smsClient.send(smsSendVO);
    }
}
