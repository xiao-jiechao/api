/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.aftersale;

import cn.shoptnt.message.event.OrderStatusChangeEvent;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.client.member.DepositeClient;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.model.trade.order.enums.PayStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @description: 预存款消费者
 * @author: liuyulei
 * @create: 2020-02-24 18:07
 * @version:1.0
 * @since:7.1.4
 **/
@Component
public class DepositeConsumer implements OrderStatusChangeEvent {


    @Autowired
    private DepositeClient depositeClient;


    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {
        if(OrderStatusEnum.CANCELLED.equals(orderMessage.getNewStatus())){
            OrderDO order = orderMessage.getOrderDO();
            //如果使用了预存款支付
            if(order.getBalance() > 0 && PayStatusEnum.PAY_NO.name().equals(order.getPayStatus())){
                depositeClient.increase(order.getBalance(),order.getMemberId(),"商品订单取消，退还预存款,订单号:" + order.getSn());
            }
        }
    }
}
