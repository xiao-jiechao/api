/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.distribution;

import cn.shoptnt.message.event.OrderStatusChangeEvent;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.client.distribution.DistributionOrderClient;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 分销商订单处理
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/6/14 上午7:13
 */

@Component
public class DistributionOrderChangeConsumer implements OrderStatusChangeEvent{

    @Autowired
    private DistributionOrderClient distributionOrderClient;

    private final Logger logger = LoggerFactory.getLogger(getClass());



    @Override
    public void orderChange(OrderStatusChangeMsg orderStatusChangeMsg) {
        OrderDO order = orderStatusChangeMsg.getOrderDO();
        try {
            //判断订单状态为已发货
            if (orderStatusChangeMsg.getNewStatus().equals(OrderStatusEnum.ROG)) {
                //结算订单
                distributionOrderClient.confirm(order);
            }
        } catch (Exception e) {
            logger.error("订单收款分销计算返利异常：",e);
        }
    }

}
