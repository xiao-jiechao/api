/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.goodssearch;

import cn.shoptnt.message.event.GoodsWordsChangeEvent;
import cn.shoptnt.client.goods.GoodsWordsClient;
import cn.shoptnt.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author liuyulei
 * @version 1.0
 * @Description: 变更商品提示词中商品数量
 * @date 2019/6/13 17:18
 * @since v7.0
 */
@Service
public class GoodsWordsChangeConsumer implements GoodsWordsChangeEvent {

    @Autowired
    private GoodsWordsClient goodsWordsClient;


    @Override
    public void wordsChange(String words) {

        if(!StringUtil.isEmpty(words)){
            //根据提示词更新提示词下商品数量
            this.goodsWordsClient.updateGoodsNum(words);
        }
    }
}
