///*
// * SHOPTNT 版权所有。
// * 未经许可，您不得使用此文件。
// * 官方地址：www.shoptnt.cn
// */
//package cn.shoptnt.consumer.shop.promotion;
//
//import cn.shoptnt.consumer.core.event.GoodsChangeEvent;
//import cn.shoptnt.model.base.message.GoodsChangeMsg;
//import cn.shoptnt.client.trade.PintuanGoodsClient;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * 拼团索引同步消费者
// * @author 妙贤
// * @version 1.0
// * @since 7.1.0
// * 2019-03-08
// */
//@Component
//public class PinTuanIndexConsumer implements GoodsChangeEvent {
//
//
//    @Autowired
//    private PintuanGoodsClient pintuanGoodsClient;
//
//
//    @Override
//    public void goodsChange(GoodsChangeMsg goodsChangeMsg) {
//
//       Long[] goodsIdAr= goodsChangeMsg.getGoodsIds();
//        int operationType = goodsChangeMsg.getOperationType();
//
//        //修改商品：同步此商品的所有sku
//        if( GoodsChangeMsg.UPDATE_OPERATION==operationType){
//            for (Long goodsId : goodsIdAr) {
//                pintuanGoodsClient.syncIndexByGoodsId(goodsId);
//            }
//
//        }
//
//        //删除商品 则删除这个商品的所有sku的索引
//        if( GoodsChangeMsg.INRECYCLE_OPERATION==operationType  ||GoodsChangeMsg.UNDER_OPERATION==operationType  ){
//            for (Long goodsId : goodsIdAr) {
//                pintuanGoodsClient.deleteIndexByGoodsId(goodsId);
//            }
//        }
//
//
//    }
//}
