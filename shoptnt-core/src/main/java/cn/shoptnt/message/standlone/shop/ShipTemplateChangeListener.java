package cn.shoptnt.message.standlone.shop;

import cn.shoptnt.message.dispatcher.shop.ShipTemplateChangeDispatcher;
import cn.shoptnt.model.goods.vo.ShipTemplateMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 运费模板变化 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class ShipTemplateChangeListener {

    @Autowired
    private ShipTemplateChangeDispatcher shipTemplateChangeDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void shipTemplateChange(ShipTemplateMsg shipTemplateMsg) {
        shipTemplateChangeDispatcher.dispatch(shipTemplateMsg);
    }
}
