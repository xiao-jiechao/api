package cn.shoptnt.message.standlone.shop;

import cn.shoptnt.message.dispatcher.shop.ShopChangeDispatcher;
import cn.shoptnt.model.shop.vo.ShopChangeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 店铺信息变更 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class ShopChangeListener {

    @Autowired
    private ShopChangeDispatcher shopChangeDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void shopChange(ShopChangeMsg shopNameChangeMsg) {
        shopChangeDispatcher.dispatch(shopNameChangeMsg);
    }
}
