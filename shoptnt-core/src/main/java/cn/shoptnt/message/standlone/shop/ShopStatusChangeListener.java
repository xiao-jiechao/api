package cn.shoptnt.message.standlone.shop;

import cn.shoptnt.message.dispatcher.shop.ShopStatusChangeDispatcher;
import cn.shoptnt.model.base.message.ShopStatusChangeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;


/**
 * 店铺状态变更 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class ShopStatusChangeListener {

    @Autowired
    private ShopStatusChangeDispatcher shopStatusChangeDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void changeStatus(ShopStatusChangeMsg shopStatusChangeMsg) {
        shopStatusChangeDispatcher.dispatch(shopStatusChangeMsg);
    }
}
