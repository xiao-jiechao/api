package cn.shoptnt.message.standlone.trade.aftersale;

import cn.shoptnt.message.dispatcher.trade.aftersale.AfterSaleChangeDispatcher;
import cn.shoptnt.model.base.message.AfterSaleChangeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 售后服务单状态变化 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class AfterSaleChangeListener {

    @Autowired
    private AfterSaleChangeDispatcher afterSaleChangeDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void listener(AfterSaleChangeMessage message) {
        afterSaleChangeDispatcher.dispatch(message);
    }
}
