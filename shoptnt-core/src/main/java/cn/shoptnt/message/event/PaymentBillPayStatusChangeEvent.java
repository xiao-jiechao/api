/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.event;

import cn.shoptnt.model.base.message.PaymentBillStatusChangeMsg;

/**
 * 支付账单状态变化事件
 *
 * @author fk
 * @version v2.0
 * @since v7.2.1
 * 2020年3月11日 上午9:52:32
 */
public interface PaymentBillPayStatusChangeEvent {

    /**
     * 支付账单状态改变，需要执行的操作
     *
     * @param paymentBillMessage
     */
    void billChange(PaymentBillStatusChangeMsg paymentBillMessage);
}
