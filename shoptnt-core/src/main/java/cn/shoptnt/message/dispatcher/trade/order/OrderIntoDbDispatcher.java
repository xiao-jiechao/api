package cn.shoptnt.message.dispatcher.trade.order;

import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.message.event.TradeIntoDbEvent;
import cn.shoptnt.model.base.message.TradeCreateMessage;
import cn.shoptnt.model.trade.order.vo.TradeVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单入库 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class OrderIntoDbDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<TradeIntoDbEvent> events;

    @Autowired
    private Cache cache;

    public void dispatch(TradeCreateMessage tradeCreateMessage) {
        String tradeVOKey = tradeCreateMessage.getCacheKey();
        TradeVO tradeVO = (TradeVO) this.cache.get(tradeVOKey);
        if (events != null) {
            for (TradeIntoDbEvent event : events) {
                try {
                    event.onTradeIntoDb(tradeVO);
                } catch (Exception e) {
                    logger.error("交易入库消息出错", e);
                } finally {
                    cache.remove(tradeVOKey);
                }
            }
        }
    }
}
