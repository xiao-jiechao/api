package cn.shoptnt.message.dispatcher.shop;

import cn.shoptnt.message.event.ShopCollectionEvent;
import cn.shoptnt.model.base.message.ShopChangeRegisterMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 店铺变更 调度器
 *
 * @author zs
 * @since 2024-03-13
 */
@Service
public class ShopChangeRegisterDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<ShopCollectionEvent> events;

    public void dispatch(ShopChangeRegisterMessage message) {
        if (events != null) {
            for (ShopCollectionEvent event : events) {
                try {
                    event.shopChange(message.getShopId());
                } catch (Exception e) {
                    logger.error("店铺信息变更消息出错", e);
                }
            }
        }
    }
}
