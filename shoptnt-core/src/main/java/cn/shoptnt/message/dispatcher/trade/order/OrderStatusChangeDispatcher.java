package cn.shoptnt.message.dispatcher.trade.order;

import cn.shoptnt.message.event.OrderStatusChangeEvent;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单状态改变 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class OrderStatusChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<OrderStatusChangeEvent> events;

    public void dispatch(OrderStatusChangeMsg orderMessage) {
        if (events != null) {
            for (OrderStatusChangeEvent event : events) {
                try {
                    event.orderChange(orderMessage);
                } catch (Exception e) {
                    logger.error("处理订单状态变化消息出错", e);
                }
            }
        }
    }
}
