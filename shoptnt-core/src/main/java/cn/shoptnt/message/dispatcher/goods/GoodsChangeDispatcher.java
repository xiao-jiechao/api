package cn.shoptnt.message.dispatcher.goods;

import cn.shoptnt.message.event.GoodsChangeEvent;
import cn.shoptnt.model.base.message.GoodsChangeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品信息变化 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 **/
@Service
public class GoodsChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<GoodsChangeEvent> events;

    public void dispatch(GoodsChangeMsg goodsChangeMsg) {
        if (events != null) {
            for (GoodsChangeEvent event : events) {
                try {
                    event.goodsChange(goodsChangeMsg);
                } catch (Exception e) {
                    logger.error("处理商品变化消息出错", e);
                }
            }
        }
    }
}
