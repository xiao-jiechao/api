package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.message.event.MemberAskSendMessageEvent;
import cn.shoptnt.model.base.message.MemberAskMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员咨询 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class MemberAskSendMessageDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberAskSendMessageEvent> events;

    public void dispatch(MemberAskMessage memberAskMessage) {
        if (events != null) {
            for (MemberAskSendMessageEvent event : events) {
                try {
                    event.goodsAsk(memberAskMessage);
                } catch (Exception e) {
                    logger.error("处理会员商品咨询出错", e);
                }
            }
        }
    }
}
