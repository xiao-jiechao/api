/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import cn.shoptnt.model.goods.SkuNameUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 商品选择器，sku单位
 *
 * @author fk
 * @version v2.0
 * @since v7.2.0
 * 2020年2月6日
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GoodsSelectorSkuVO {

    /**
     * 主键
     */
    @Schema(hidden = true)
    private Long skuId;
    /**
     * 商品id
     */
    @Schema(name = "goods_id", description =  "商品id", hidden = true)
    private Long goodsId;
    /**
     * 商品名称
     */
    @Schema(name = "goods_name", description =  "商品名称", hidden = true)
    private String goodsName;
    /**
     * 商品编号
     */
    @Schema(name = "sn", description =  "商品编号")
    private String sn;
    /**
     * 库存
     */
    @Schema(name = "quantity", description =  "库存")
    private Integer quantity;
    /**
     * 可用库存
     */
    @Schema(name = "enable_quantity", description =  "可用库存")
    private Integer enableQuantity;
    /**
     * 商品价格
     */
    @Schema(name = "price", description =  "商品价格")
    private Double price;
    /**
     * 规格信息json
     */
    @Schema(name = "specs", description =  "规格信息json", hidden = true)
    private String specs;
    /**
     * 成本价格
     */
    @Schema(name = "cost", description = "成本价格", required = true)
    private Double cost;
    /**
     * 重量
     */
    @Schema(name = "weight", description = "重量", required = true)
    private Double weight;
    /**
     * 卖家id
     */
    @Schema(name = "seller_id", description = "卖家id")
    private Long sellerId;
    /**
     * 卖家名称
     */
    @Schema(name = "seller_name", description = "卖家名称")
    private String sellerName;
    /**
     * 分类id
     */
    @Schema(name = "category_id", description = "分类id")
    private Long categoryId;
    /**
     * 缩略图
     */
    @Schema(name = "thumbnail", description = "缩略图", hidden = true)
    private String thumbnail;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getEnableQuantity() {
        return enableQuantity;
    }

    public void setEnableQuantity(Integer enableQuantity) {
        this.enableQuantity = enableQuantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }
}
