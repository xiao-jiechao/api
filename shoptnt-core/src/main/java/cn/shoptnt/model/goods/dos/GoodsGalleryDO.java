/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import cn.shoptnt.framework.validation.annotation.SafeDomain;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;


/**
 * 商品相册实体
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-21 11:39:54
 */
@TableName("es_goods_gallery")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GoodsGalleryDO implements Serializable {

    private static final long serialVersionUID = 8150217189133447L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(name = "img_id", description = "图片的主键，添加时-1", required = true)
    private Long imgId;
    /**
     * 商品主键
     */
    @Schema(name = "goods_id", description = "商品主键", hidden = true)
    private Long goodsId;
    /**
     * 缩略图路径
     */
    @Schema(name = "thumbnail", description = "缩略图路径", hidden = true)
    private String thumbnail;
    /**
     * 小图路径
     */
    @Schema(name = "small", description = "小图路径", hidden = true)
    private String small;
    /**
     * 大图路径
     */
    @Schema(name = "big", description = "大图路径", hidden = true)
    private String big;
    /**
     * 原图路径
     */
    @SafeDomain
    @Schema(name = "original", description = "原图路径", required = true)
    private String original;
    /**
     * 极小图路径
     */
    @Schema(name = "tiny", description = "极小图路径", hidden = true)
    private String tiny;
    /**
     * 是否是默认图片1   0没有默认
     */
    @Schema(name = "isdefault", description = "是否是默认图片1   0没有默认", hidden = true)
    private Integer isdefault;
    /**
     * 排序
     */
    @Schema(name = "sort", description = "排序", required = true)
    private Integer sort;

    @PrimaryKeyField
    public Long getImgId() {
        return imgId;
    }

    public void setImgId(Long imgId) {
        this.imgId = imgId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getBig() {
        return big;
    }

    public void setBig(String big) {
        this.big = big;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getTiny() {
        return tiny;
    }

    public void setTiny(String tiny) {
        this.tiny = tiny;
    }

    public Integer getIsdefault() {
        return isdefault;
    }

    public void setIsdefault(Integer isdefault) {
        this.isdefault = isdefault;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "GoodsGalleryDO [imgId=" + imgId + ", goodsId=" + goodsId + ", thumbnail=" + thumbnail + ", small="
                + small + ", big=" + big + ", original=" + original + ", tiny=" + tiny + ", isdefault=" + isdefault
                + ", sort=" + sort + "]";
    }


}
