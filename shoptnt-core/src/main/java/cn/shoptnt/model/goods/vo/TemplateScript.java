/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 运费模板对象
 *
 * @author zh
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2019/9/25 上午12:07
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TemplateScript implements Serializable {


    @Schema(description =  "sku_id")
    private Long skuId;

    @Schema(name = "template_id", description =  "运费模板id")
    private Integer templateId;

    @Schema(name = "script", description = "脚本内容")
    private String script;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }



    @Override
    public String toString() {
        return "TemplateScript{" +
                "skuId=" + skuId +
                ", templateId=" + templateId +
                ", script='" + script + '\'' +
                '}';
    }
}


