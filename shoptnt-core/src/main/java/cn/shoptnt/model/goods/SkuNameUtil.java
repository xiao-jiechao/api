/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.goods;

import cn.shoptnt.model.goods.vo.SpecValueVO;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.framework.util.StringUtil;

import java.util.List;

/**
 * sku 名称生成工具
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-03-05
 */

public class SkuNameUtil {

    public  static  String createSkuName(String specs) {
        if (StringUtil.isEmpty(specs)) {
            return "";
        }
        List<SpecValueVO> specList  = JsonUtil.jsonToList(specs, SpecValueVO.class);

        StringBuffer skuName = new StringBuffer();
        specList.forEach(specValueVO -> {
            skuName.append("/");
            skuName.append(specValueVO.getSpecValue());
        });
        return skuName.toString().substring(1);
    }

}
