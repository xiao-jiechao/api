/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base;

/**
 * 系统设置分组枚举
 * Created by 妙贤 on 2018/3/19.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/19
 */
public enum SettingGroup {

    /**
     * 系统设置
     */
    SYSTEM,

    /**
     * 站点设置
     */
    SITE,

    /**
     * 商品设置
     */
    GOODS,
    /**
     * 商品图片尺寸
     */
    PHOTO_SIZE,
    /**
     * 交易设置
     */
    TRADE,

    /**
     * 积分设置
     */
    POINT,
    /**
     * 分销设置
     */
    DISTRIBUTION,
    /**
     * 测试设置
     */
    TEST,
    /**
     * app推送设置
     */
    PUSH,
    /**
     * page
     */
    PAGE,
    /**
     * ES_SIGN 分词词库秘钥
     */
    ES_SIGN,
    /**
     *  账号安全设置（临时）
     */
    ACCOUNT,
    /**
    * INDEX_PIC 首页欢迎图
     */
    INDEX_PIC
}
