/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.message;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;
import cn.shoptnt.model.shop.enums.ShopStatusEnum;

import java.io.Serializable;

/**
 * 店铺状态修改消息
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018/9/9 下午11:01
 * @Description:
 *
 */
public class ShopStatusChangeMsg implements Serializable, DirectMessage {


	private static final long serialVersionUID = 958600762323161940L;
	/**
	 * 店铺id
	 */
	private Long sellerId;

	/**
	 * 操作类型
	 */
	private ShopStatusEnum statusEnum;


	public ShopStatusChangeMsg(Long sellerId, ShopStatusEnum statusEnum) {
		super();
		this.sellerId = sellerId;
		this.statusEnum = statusEnum;
	}

	public Long getSellerId() {
		return sellerId;
	}

	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}

	public ShopStatusEnum getStatusEnum() {
		return statusEnum;
	}

	public void setStatusEnum(ShopStatusEnum statusEnum) {
		this.statusEnum = statusEnum;
	}

	@Override
	public String getExchange() {
		return AmqpExchange.CLOSE_STORE;
	}
}
