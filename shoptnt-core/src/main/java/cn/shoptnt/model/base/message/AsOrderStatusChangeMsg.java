/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.dto.OrderDTO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;

import java.io.Serializable;


/**
 * 换货补发订单变化消息
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午9:52:13
 */
public class AsOrderStatusChangeMsg implements Serializable, DirectMessage {

    private static final long serialVersionUID = 8915428082431868648L;

    /**
     * 变化的订单
     */
    private OrderDO orderDO;

    /**
     * 原状态
     */
    private OrderStatusEnum oldStatus;

    /**
     * 新状态
     */
    private OrderStatusEnum newStatus;

    /**
     * 刚入库的订单(暂时只做赠品入库操作)
     */
    private OrderDTO orderDTO;

    public OrderDO getOrderDO() {
        return orderDO;
    }

    public void setOrderDO(OrderDO orderDO) {
        this.orderDO = orderDO;
    }

    public OrderStatusEnum getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(OrderStatusEnum oldStatus) {
        this.oldStatus = oldStatus;
    }

    public OrderStatusEnum getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(OrderStatusEnum newStatus) {
        this.newStatus = newStatus;
    }

    public OrderDTO getOrderDTO() {
        return orderDTO;
    }

    public void setOrderDTO(OrderDTO orderDTO) {
        this.orderDTO = orderDTO;
    }

    public AsOrderStatusChangeMsg() {
    }

    @Override
    public String toString() {
        return "OrderStatusChangeMsg{" +
                "orderDO=" + orderDO +
                ", oldStatus=" + oldStatus +
                ", newStatus=" + newStatus +
                '}';
    }

    @Override
    public String getExchange() {
        return AmqpExchange.AS_SELLER_CREATE_ORDER;
    }
}
