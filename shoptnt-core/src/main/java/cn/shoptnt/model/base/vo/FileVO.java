/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.vo;


import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 文件上传返回值封装
 *
 * @author zh
 * @version v2.0
 * @since v7.0
 * 2018年3月19日 下午4:42:51
 */
@Schema
public class FileVO {
    /**
     * 文件名称
     */
    @Schema(name = "name", description =  "文件名称", required = true)
    private String name;
    /**
     * 文件后缀
     */
    @Schema(name = "ext", description =  "文件后缀", required = true)
    private String ext;
    /**
     * url
     */
    @Schema(name = "url", description =  "图片地址", required = true)
    private String url;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FileVO{" +
                "name='" + name + '\'' +
                ", ext='" + ext + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
