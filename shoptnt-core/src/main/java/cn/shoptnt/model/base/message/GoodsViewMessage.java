package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;
import cn.shoptnt.model.statistics.dos.GoodsPageView;

import java.io.Serializable;
import java.util.List;

/**
 * 商品访问消息
 *
 * @author zs
 * @since 2024-03-08
 **/
public class GoodsViewMessage implements Serializable, DirectMessage {

    private static final long serialVersionUID = -6978542323509463579L;

    private final List<GoodsPageView> goodsPageViews;

    public List<GoodsPageView> getGoodsPageViews() {
        return goodsPageViews;
    }

    public GoodsViewMessage(List<GoodsPageView> goodsPageViews) {
        this.goodsPageViews = goodsPageViews;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.GOODS_VIEW_COUNT;
    }
}
