/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.vo;

import cn.shoptnt.model.goods.vo.BackendGoodsVO;
import cn.shoptnt.model.member.vo.BackendMemberVO;
import cn.shoptnt.model.statistics.vo.SalesTotal;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.List;

/**
 * 后台首页模型
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-06-29 上午9:03
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BackendIndexModelVO {

    // ================= 总量统计 =================

    @Schema(name = "goods_count", description = "商品数量")
    private Integer goodsCount;

    @Schema(name = "order_count", description = "订单数量")
    private Integer orderCount;

    @Schema(name = "member_count", description = "会员数量")
    private Integer memberCount;

    @Schema(name = "shop_count", description = "店铺数量")
    private Integer shopCount;

    // ================= 待办统计 =================

    @Schema(name = "wait_audit_goods_count", description = "待审核商品数量")
    private Integer waitAuditGoodsCount;

    @Schema(name = "wait_audit_shop_count", description = "待审核店铺数量")
    private Integer waitAuditShopCount;

    @Schema(name = "wait_handle_complain_count", description = "待审核交易投诉数量")
    private Integer waitHandleComplainCount;

    @Schema(name = "wait_handle_service_count", description = "待处理售后数量")
    private Integer waitHandleServiceCount;

    @Schema(name = "wait_audit_withdraw_count", description = "待审核分销提现数量")
    private Integer waitAuditWithdrawCount;

    // ================= 流量统计 =================

    @Schema(name = "today_visit_count", description = "今日访客数量")
    private Integer todayVisitCount;

    @Schema(name = "yesterday_visit_count", description = "昨日访客数量")
    private Integer yesterdayVisitCount;

    @Schema(name = "seven_day_visit_count", description = "前7日访客数量")
    private Integer sevenDayVisitCount;

    @Schema(name = "thirty_day_visit_count", description = "前30日访客数量")
    private Integer thirtyDayVisitCount;

    // ================= 今日统计 =================

    @Schema(name = "today_order_count", description = "今日订单数")
    private Integer todayOrderCount;

    @Schema(name = "today_order_amount", description = "今日交易额")
    private Double todayOrderAmount;

    @Schema(name = "today_shop_count", description = "今日新增店铺")
    private Integer todayShopCount;

    @Schema(name = "today_member_count", description = "今日新增会员")
    private Integer todayMemberCount;

    @Schema(name = "today_goods_count", description = "今日新增商品数量")
    private Integer todayGoodsCount;

    @Schema(name = "today_comment_count", description = "今日新增评论")
    private Integer todayCommentCount;

    public Integer getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(Integer goodsCount) {
        this.goodsCount = goodsCount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Integer getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Integer memberCount) {
        this.memberCount = memberCount;
    }

    public Integer getShopCount() {
        return shopCount;
    }

    public void setShopCount(Integer shopCount) {
        this.shopCount = shopCount;
    }

    public Integer getWaitAuditGoodsCount() {
        return waitAuditGoodsCount;
    }

    public void setWaitAuditGoodsCount(Integer waitAuditGoodsCount) {
        this.waitAuditGoodsCount = waitAuditGoodsCount;
    }

    public Integer getWaitAuditShopCount() {
        return waitAuditShopCount;
    }

    public void setWaitAuditShopCount(Integer waitAuditShopCount) {
        this.waitAuditShopCount = waitAuditShopCount;
    }

    public Integer getWaitHandleComplainCount() {
        return waitHandleComplainCount;
    }

    public void setWaitHandleComplainCount(Integer waitHandleComplainCount) {
        this.waitHandleComplainCount = waitHandleComplainCount;
    }

    public Integer getWaitHandleServiceCount() {
        return waitHandleServiceCount;
    }

    public void setWaitHandleServiceCount(Integer waitHandleServiceCount) {
        this.waitHandleServiceCount = waitHandleServiceCount;
    }

    public Integer getWaitAuditWithdrawCount() {
        return waitAuditWithdrawCount;
    }

    public void setWaitAuditWithdrawCount(Integer waitAuditWithdrawCount) {
        this.waitAuditWithdrawCount = waitAuditWithdrawCount;
    }

    public Integer getTodayVisitCount() {
        return todayVisitCount;
    }

    public void setTodayVisitCount(Integer todayVisitCount) {
        this.todayVisitCount = todayVisitCount;
    }

    public Integer getYesterdayVisitCount() {
        return yesterdayVisitCount;
    }

    public void setYesterdayVisitCount(Integer yesterdayVisitCount) {
        this.yesterdayVisitCount = yesterdayVisitCount;
    }

    public Integer getSevenDayVisitCount() {
        return sevenDayVisitCount;
    }

    public void setSevenDayVisitCount(Integer sevenDayVisitCount) {
        this.sevenDayVisitCount = sevenDayVisitCount;
    }

    public Integer getThirtyDayVisitCount() {
        return thirtyDayVisitCount;
    }

    public void setThirtyDayVisitCount(Integer thirtyDayVisitCount) {
        this.thirtyDayVisitCount = thirtyDayVisitCount;
    }

    public Integer getTodayOrderCount() {
        return todayOrderCount;
    }

    public void setTodayOrderCount(Integer todayOrderCount) {
        this.todayOrderCount = todayOrderCount;
    }

    public Double getTodayOrderAmount() {
        return todayOrderAmount;
    }

    public void setTodayOrderAmount(Double todayOrderAmount) {
        this.todayOrderAmount = todayOrderAmount;
    }

    public Integer getTodayShopCount() {
        return todayShopCount;
    }

    public void setTodayShopCount(Integer todayShopCount) {
        this.todayShopCount = todayShopCount;
    }

    public Integer getTodayMemberCount() {
        return todayMemberCount;
    }

    public void setTodayMemberCount(Integer todayMemberCount) {
        this.todayMemberCount = todayMemberCount;
    }

    public Integer getTodayGoodsCount() {
        return todayGoodsCount;
    }

    public void setTodayGoodsCount(Integer todayGoodsCount) {
        this.todayGoodsCount = todayGoodsCount;
    }

    public Integer getTodayCommentCount() {
        return todayCommentCount;
    }

    public void setTodayCommentCount(Integer todayCommentCount) {
        this.todayCommentCount = todayCommentCount;
    }

    @Override
    public String toString() {
        return "BackendIndexModelVO{" +
                "goodsCount=" + goodsCount +
                ", orderCount=" + orderCount +
                ", memberCount=" + memberCount +
                ", shopCount=" + shopCount +
                ", waitAuditGoodsCount=" + waitAuditGoodsCount +
                ", waitAuditShopCount=" + waitAuditShopCount +
                ", waitHandleComplainCount=" + waitHandleComplainCount +
                ", waitHandleServiceCount=" + waitHandleServiceCount +
                ", waitAuditWithdrawCount=" + waitAuditWithdrawCount +
                ", todayVisitCount=" + todayVisitCount +
                ", yesterdayVisitCount=" + yesterdayVisitCount +
                ", sevenDayVisitCount=" + sevenDayVisitCount +
                ", thirtyDayVisitCount=" + thirtyDayVisitCount +
                ", todayOrderCount=" + todayOrderCount +
                ", todayOrderAmount=" + todayOrderAmount +
                ", todayShopCount=" + todayShopCount +
                ", todayMemberCount=" + todayMemberCount +
                ", todayGoodsCount=" + todayGoodsCount +
                ", todayCommentCount=" + todayCommentCount +
                '}';
    }
}
