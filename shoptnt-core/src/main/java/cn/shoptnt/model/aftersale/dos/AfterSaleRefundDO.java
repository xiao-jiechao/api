/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * 售后退款/退货信息实体
 * 用于用户新申请退货、退款存放的退款账户相关信息
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-15
 */
@TableName(value = "es_as_refund")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AfterSaleRefundDO implements Serializable {

    private static final long serialVersionUID = -1374141503656012554L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long id;
    /**
     * 售后服务单号
     */
    @Schema(name = "service_sn", description =  "售后服务单号")
    private String serviceSn;
    /**
     * 申请退款金额
     */
    @Schema(name = "refund_price", description =  "申请退款金额")
    private Double refundPrice;
    /**
     * 商家同意退款金额
     */
    @Schema(name = "agree_price", description =  "商家同意退款金额")
    private Double agreePrice;
    /**
     * 实际退款金额
     */
    @Schema(name = "actual_price", description =  "实际退款金额")
    private Double actualPrice;
    /**
     * 退款方式 ORIGINAL：原路退回，OFFLINE：线下支付
     */
    @Schema(name = "refund_way", description =  "退款方式 ORIGINAL：原路退回，OFFLINE：线下支付")
    private String refundWay;
    /**
     * 账号类型
     */
    @Schema(name = "account_type", description =  "账号类型")
    private String accountType;
    /**
     * 退款账号
     */
    @Schema(name = "return_account", description =  "退款账号")
    private String returnAccount;
    /**
     * 银行名称
     */
    @Schema(name = "bank_name", description =  "银行名称")
    private String bankName;
    /**
     * 银行账户
     */
    @Schema(name = "bank_account_number", description =  "银行账户")
    private String bankAccountNumber;
    /**
     * 银行开户名
     */
    @Schema(name = "bank_account_name", description =  "银行开户名")
    private String bankAccountName;
    /**
     * 银行开户行
     */
    @Schema(name = "bank_deposit_name", description =  "银行开户行")
    private String bankDepositName;
    /**
     * 订单支付方式返回的交易号
     */
    @Schema(name = "pay_order_no", description =  "订单支付方式返回的交易号")
    private String payOrderNo;
    /**
     * 退款时间
     */
    @Schema(name = "refund_time", description =  "退款时间")
    private Long refundTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceSn() {
        return serviceSn;
    }

    public void setServiceSn(String serviceSn) {
        this.serviceSn = serviceSn;
    }

    public Double getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(Double refundPrice) {
        this.refundPrice = refundPrice;
    }

    public Double getAgreePrice() {
        return agreePrice;
    }

    public void setAgreePrice(Double agreePrice) {
        this.agreePrice = agreePrice;
    }

    public Double getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Double actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getRefundWay() {
        return refundWay;
    }

    public void setRefundWay(String refundWay) {
        this.refundWay = refundWay;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getReturnAccount() {
        return returnAccount;
    }

    public void setReturnAccount(String returnAccount) {
        this.returnAccount = returnAccount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankDepositName() {
        return bankDepositName;
    }

    public void setBankDepositName(String bankDepositName) {
        this.bankDepositName = bankDepositName;
    }

    public String getPayOrderNo() {
        return payOrderNo;
    }

    public void setPayOrderNo(String payOrderNo) {
        this.payOrderNo = payOrderNo;
    }

    public Long getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(Long refundTime) {
        this.refundTime = refundTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AfterSaleRefundDO that = (AfterSaleRefundDO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(serviceSn, that.serviceSn) &&
                Objects.equals(refundPrice, that.refundPrice) &&
                Objects.equals(agreePrice, that.agreePrice) &&
                Objects.equals(actualPrice, that.actualPrice) &&
                Objects.equals(refundWay, that.refundWay) &&
                Objects.equals(accountType, that.accountType) &&
                Objects.equals(returnAccount, that.returnAccount) &&
                Objects.equals(bankName, that.bankName) &&
                Objects.equals(bankAccountNumber, that.bankAccountNumber) &&
                Objects.equals(bankAccountName, that.bankAccountName) &&
                Objects.equals(bankDepositName, that.bankDepositName) &&
                Objects.equals(payOrderNo, that.payOrderNo) &&
                Objects.equals(refundTime, that.refundTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, serviceSn, refundPrice, agreePrice, actualPrice, refundWay, accountType, returnAccount, bankName, bankAccountNumber, bankAccountName, bankDepositName, payOrderNo, refundTime);
    }

    @Override
    public String toString() {
        return "AfterSaleRefundDO{" +
                "id=" + id +
                ", serviceSn='" + serviceSn + '\'' +
                ", refundPrice=" + refundPrice +
                ", agreePrice=" + agreePrice +
                ", actualPrice=" + actualPrice +
                ", refundWay='" + refundWay + '\'' +
                ", accountType='" + accountType + '\'' +
                ", returnAccount='" + returnAccount + '\'' +
                ", bankName='" + bankName + '\'' +
                ", bankAccountNumber='" + bankAccountNumber + '\'' +
                ", bankAccountName='" + bankAccountName + '\'' +
                ", bankDepositName='" + bankDepositName + '\'' +
                ", payOrderNo='" + payOrderNo + '\'' +
                ", refundTime=" + refundTime +
                '}';
    }
}
