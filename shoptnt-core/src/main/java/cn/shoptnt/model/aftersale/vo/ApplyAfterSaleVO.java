/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.vo;

import cn.hutool.core.util.ObjectUtil;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.validation.impl.SafeDomainValidator;
import cn.shoptnt.model.aftersale.dos.*;
import cn.shoptnt.model.aftersale.dto.ServiceOperateAllowable;
import cn.shoptnt.model.aftersale.enums.ServiceStatusEnum;
import cn.shoptnt.model.aftersale.enums.ServiceTypeEnum;
import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountGiftDO;
import cn.shoptnt.model.system.dos.LogisticsCompanyDO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 售后申请VO
 * 存放的是一个售后服务所有相关的信息，主要用于查看售后服务详情
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-15
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ApplyAfterSaleVO extends AfterSaleServiceDO implements Serializable {

    private static final long serialVersionUID = -96586965589627807L;

    @Schema(name = "service_type_text", description = "售后类型 退货，换货，补发货品，取消订单")
    private String serviceTypeText;

    @Schema(name = "service_status_text", description = "售后单状态 申请，审核通过，审核拒绝，待人工处理，入库，退款中，退款失败，完成")
    private String serviceStatusText;

    @Schema(name = "sku_id", description = "申请售后商品skuID")
    private Long skuId;

    @Schema(name = "return_num", description = "申请售后商品数量")
    private Integer returnNum;

    @Schema(name = "refund_info", description = "售后退款/退货信息")
    private AfterSaleRefundDO refundInfo;

    @Schema(name = "change_info", description = "售后换货/补发商品信息")
    private AfterSaleChangeDO changeInfo;

    @Schema(name = "express_info", description = "售后物流信息")
    private AfterSaleExpressDO expressInfo;

    @Schema(name = "goods_list", description = "申请售后商品信息集合")
    private List<AfterSaleGoodsDO> goodsList;

    @Schema(name = "images",description = "售后图片集合")
    private List<AfterSaleGalleryDO> images;

    @Schema(name = "logs",description = "售后日志信息集合")
    private List<AfterSaleLogDO> logs;

    @Schema(name = "logi_list",description = "物流公司集合")
    private List<LogisticsCompanyDO> logiList;

    @Schema(name = "order_ship_status",description = "订单发货状态")
    private String orderShipStatus;

    @Schema(name = "allowable",description = "售后服务单运行操作情况")
    private ServiceOperateAllowable allowable;

    @Schema(name = "order_payment_type",description = "订单付款类型 ONLINE：在线支付，COD：货到付款")
    private String orderPaymentType;

    @Schema(name = "gift_list",description = "赠品信息集合")
    private List<FullDiscountGiftDO> giftList;

    public String getServiceTypeText() {
        if (this.getServiceType() != null) {
            ServiceTypeEnum serviceTypeEnum = ServiceTypeEnum.valueOf(this.getServiceType());
            serviceTypeText = serviceTypeEnum.description();
        }
        return serviceTypeText;
    }

    public void setServiceTypeText(String serviceTypeText) {
        this.serviceTypeText = serviceTypeText;
    }

    public String getServiceStatusText() {
        if (this.getServiceStatus() != null) {
            ServiceStatusEnum serviceStatusEnum = ServiceStatusEnum.valueOf(this.getServiceStatus());
            serviceStatusText = serviceStatusEnum.description();
        }
        return serviceStatusText;
    }

    public void setServiceStatusText(String serviceStatusText) {
        this.serviceStatusText = serviceStatusText;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getReturnNum() {
        return returnNum;
    }

    public void setReturnNum(Integer returnNum) {
        this.returnNum = returnNum;
    }

    public AfterSaleRefundDO getRefundInfo() {
        return refundInfo;
    }

    public void setRefundInfo(AfterSaleRefundDO refundInfo) {
        this.refundInfo = refundInfo;
    }

    public AfterSaleChangeDO getChangeInfo() {
        return changeInfo;
    }

    public void setChangeInfo(AfterSaleChangeDO changeInfo) {
        this.changeInfo = changeInfo;
    }

    public AfterSaleExpressDO getExpressInfo() {
        return expressInfo;
    }

    public void setExpressInfo(AfterSaleExpressDO expressInfo) {
        this.expressInfo = expressInfo;
    }

    public List<AfterSaleGoodsDO> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<AfterSaleGoodsDO> goodsList) {
        this.goodsList = goodsList;
    }

    public List<AfterSaleGalleryDO> getImages() {
        return images;
    }

    public void setImages(List<AfterSaleGalleryDO> images) {
        this.images = images;
    }

    public List<AfterSaleLogDO> getLogs() {
        return logs;
    }

    public void setLogs(List<AfterSaleLogDO> logs) {
        this.logs = logs;
    }

    public List<LogisticsCompanyDO> getLogiList() {
        return logiList;
    }

    public void setLogiList(List<LogisticsCompanyDO> logiList) {
        this.logiList = logiList;
    }

    public String getOrderShipStatus() {
        return orderShipStatus;
    }

    public void setOrderShipStatus(String orderShipStatus) {
        this.orderShipStatus = orderShipStatus;
    }

    public ServiceOperateAllowable getAllowable() {
        return allowable;
    }

    public void setAllowable(ServiceOperateAllowable allowable) {
        this.allowable = allowable;
    }

    public String getOrderPaymentType() {
        return orderPaymentType;
    }

    public void setOrderPaymentType(String orderPaymentType) {
        this.orderPaymentType = orderPaymentType;
    }

    public List<FullDiscountGiftDO> getGiftList() {
        return giftList;
    }

    public void setGiftList(List<FullDiscountGiftDO> giftList) {
        this.giftList = giftList;
    }

    public ApplyAfterSaleVO() {
    }

    public ApplyAfterSaleVO(ReturnGoodsVO returnGoodsVO) {
        if (ObjectUtil.isNotEmpty(returnGoodsVO) && ObjectUtil.isNotEmpty(returnGoodsVO.getImages())){
            if (!SafeDomainValidator.checkDomainParam(returnGoodsVO.getImages())){
                throw new ServiceException("","参数涉及非法域名");
            }
        }
        this.setOrderSn(returnGoodsVO.getOrderSn());
        this.setReason(returnGoodsVO.getReason());
        this.setProblemDesc(returnGoodsVO.getProblemDesc());
        this.setApplyVouchers(returnGoodsVO.getApplyVouchers());
        this.setSkuId(returnGoodsVO.getSkuId());
        this.setReturnNum(returnGoodsVO.getReturnNum());

        AfterSaleChangeDO afterSaleChangeDO = new AfterSaleChangeDO();
        BeanUtil.copyProperties(returnGoodsVO.getRegion(), afterSaleChangeDO);
        afterSaleChangeDO.setShipName(returnGoodsVO.getShipName());
        afterSaleChangeDO.setShipMobile(returnGoodsVO.getShipMobile());
        afterSaleChangeDO.setShipAddr(returnGoodsVO.getShipAddr());
        this.setChangeInfo(afterSaleChangeDO);


        List<String> images = returnGoodsVO.getImages();
        if (images != null && images.size() != 0) {
            List<AfterSaleGalleryDO> galleryDOS = new ArrayList<>();
            for (String image : images) {
                AfterSaleGalleryDO galleryDO = new AfterSaleGalleryDO();
                galleryDO.setImg(image);
                galleryDOS.add(galleryDO);
            }
            this.setImages(galleryDOS);
        }

        AfterSaleRefundDO refundDO = new AfterSaleRefundDO();
        refundDO.setAccountType(returnGoodsVO.getAccountType());
        refundDO.setReturnAccount(returnGoodsVO.getReturnAccount());
        refundDO.setBankAccountName(returnGoodsVO.getBankAccountName());
        refundDO.setBankAccountNumber(returnGoodsVO.getBankAccountNumber());
        refundDO.setBankDepositName(returnGoodsVO.getBankDepositName());
        refundDO.setBankName(returnGoodsVO.getBankName());
        this.setRefundInfo(refundDO);


    }

    public ApplyAfterSaleVO(AfterSaleApplyVO applyVO) {
        if (ObjectUtil.isNotEmpty(applyVO) && ObjectUtil.isNotEmpty(applyVO.getImages())){
            if (!SafeDomainValidator.checkDomainParam(applyVO.getImages())) {
                throw new ServiceException("","参数涉及非法域名");
            }
        }
        this.setOrderSn(applyVO.getOrderSn());
        this.setReason(applyVO.getReason());
        this.setProblemDesc(applyVO.getProblemDesc());
        this.setApplyVouchers(applyVO.getApplyVouchers());
        this.setSkuId(applyVO.getSkuId());
        this.setReturnNum(applyVO.getReturnNum());

        AfterSaleChangeDO afterSaleChangeDO = new AfterSaleChangeDO();
        BeanUtil.copyProperties(applyVO.getRegion(), afterSaleChangeDO);
        afterSaleChangeDO.setShipName(applyVO.getShipName());
        afterSaleChangeDO.setShipMobile(applyVO.getShipMobile());
        afterSaleChangeDO.setShipAddr(applyVO.getShipAddr());
        this.setChangeInfo(afterSaleChangeDO);

        List<String> images = applyVO.getImages();
        if (images != null && images.size() != 0) {
            List<AfterSaleGalleryDO> galleryDOS = new ArrayList<>();
            for (String image : images) {
                AfterSaleGalleryDO galleryDO = new AfterSaleGalleryDO();
                galleryDO.setImg(image);
                galleryDOS.add(galleryDO);
            }
            this.setImages(galleryDOS);
        }

    }

    @Override
    public String toString() {
        return "ApplyAfterSaleVO{" +
                "serviceTypeText='" + serviceTypeText + '\'' +
                ", serviceStatusText='" + serviceStatusText + '\'' +
                ", skuId=" + skuId +
                ", returnNum=" + returnNum +
                ", refundInfo=" + refundInfo +
                ", changeInfo=" + changeInfo +
                ", expressInfo=" + expressInfo +
                ", goodsList=" + goodsList +
                ", images=" + images +
                ", logs=" + logs +
                ", logiList=" + logiList +
                ", orderShipStatus='" + orderShipStatus + '\'' +
                ", allowable=" + allowable +
                ", orderPaymentType='" + orderPaymentType + '\'' +
                ", giftList=" + giftList +
                '}';
    }
}
