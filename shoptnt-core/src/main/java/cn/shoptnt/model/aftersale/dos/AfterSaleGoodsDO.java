/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Objects;

/**
 * 售后商品实体
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-15
 */
@TableName(value = "es_as_goods")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AfterSaleGoodsDO implements Serializable {

    private static final long serialVersionUID = 2333537241953078549L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long id;
    /**
     * 售后服务单号
     */
    @Schema(name = "service_sn", description =  "售后服务单号")
    private String serviceSn;
    /**
     * 商品ID
     */
    @Schema(name = "goods_id", description =  "商品ID")
    private Long goodsId;
    /**
     * 商品SKUID
     */
    @Schema(name = "sku_id", description =  "商品SKUID")
    private Long skuId;
    /**
     * 发货数量
     */
    @Schema(name = "ship_num", description =  "发货数量")
    private Integer shipNum;
    /**
     * 商品成交价
     */
    @Schema(name = "price", description =  "商品成交价")
    private Double price;
    /**
     * 退还数量
     */
    @Schema(name = "return_num", description =  "退还数量")
    private Integer returnNum;
    /**
     * 入库数量
     */
    @Schema(name = "storage_num", description =  "入库数量")
    private Integer storageNum;
    /**
     * 商品名称
     */
    @Schema(name = "goods_name", description =  "商品名称")
    private String goodsName;
    /**
     * 商品编号
     */
    @Schema(name = "goods_sn", description =  "商品编号")
    private String goodsSn;
    /**
     * 商品缩略图
     */
    @Schema(name = "goods_image", description =  "商品缩略图")
    private String goodsImage;
    /**
     * 商品规格信息
     */
    @Schema(name = "spec_json", description =  "商品规格信息")
    private String specJson;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceSn() {
        return serviceSn;
    }

    public void setServiceSn(String serviceSn) {
        this.serviceSn = serviceSn;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getShipNum() {
        return shipNum;
    }

    public void setShipNum(Integer shipNum) {
        this.shipNum = shipNum;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getReturnNum() {
        return returnNum;
    }

    public void setReturnNum(Integer returnNum) {
        this.returnNum = returnNum;
    }

    public Integer getStorageNum() {
        return storageNum;
    }

    public void setStorageNum(Integer storageNum) {
        this.storageNum = storageNum;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsSn() {
        return goodsSn;
    }

    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    public String getSpecJson() {
        return specJson;
    }

    public void setSpecJson(String specJson) {
        this.specJson = specJson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AfterSaleGoodsDO that = (AfterSaleGoodsDO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(serviceSn, that.serviceSn) &&
                Objects.equals(goodsId, that.goodsId) &&
                Objects.equals(skuId, that.skuId) &&
                Objects.equals(shipNum, that.shipNum) &&
                Objects.equals(price, that.price) &&
                Objects.equals(returnNum, that.returnNum) &&
                Objects.equals(storageNum, that.storageNum) &&
                Objects.equals(goodsName, that.goodsName) &&
                Objects.equals(goodsSn, that.goodsSn) &&
                Objects.equals(goodsImage, that.goodsImage) &&
                Objects.equals(specJson, that.specJson);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, serviceSn, goodsId, skuId, shipNum, price, returnNum, storageNum, goodsName, goodsSn, goodsImage, specJson);
    }

    @Override
    public String toString() {
        return "AfterSaleGoodsDO{" +
                "id=" + id +
                ", serviceSn='" + serviceSn + '\'' +
                ", goodsId=" + goodsId +
                ", skuId=" + skuId +
                ", shipNum=" + shipNum +
                ", price=" + price +
                ", returnNum=" + returnNum +
                ", storageNum=" + storageNum +
                ", goodsName='" + goodsName + '\'' +
                ", goodsSn='" + goodsSn + '\'' +
                ", goodsImage='" + goodsImage + '\'' +
                ", specJson='" + specJson + '\'' +
                '}';
    }
}
