/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.security;

/**
 * 扫描结果
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/20 16:31
 **/
public class ScanResult {

    /**
     * 是否全部验签成功
     */
    private boolean success;

    /**
     * 是否扫描完成
     */
    private boolean complete;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }
}
