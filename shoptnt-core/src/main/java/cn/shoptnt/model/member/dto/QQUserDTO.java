/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;


import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * QQ登陆用户信息实体
 *
 * @author cs
 * @version v1.0
 * @since v7.2.2
 * 2020-09-28
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class QQUserDTO implements Serializable {

    private static final long serialVersionUID = -1232483319436590972L;

    @Schema(name = "openid", description =  "openid", required = true)
    private String openid;

    @Schema(name = "unionid", description =  "unionid", required = true)
    private String unionid;

    @Schema(name = "headimgurl", description =  "头像",hidden = true)
    private String headimgurl;

    @Schema(name = "accessToken", description =  "app端登陆传入access_token", required = true)
    private String accesstoken;

    @Schema(name = "nickName", description =  "用户昵称")
    private String nickname;

    @Schema(name = "gender", description =  "性别")
    private String gender;

    @Schema(name = "province", description =  "省份")
    private String province;

    @Schema(name = "city", description =  "城市")
    private String city;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAccesstoken() {
        return accesstoken;
    }

    public void setAccesstoken(String accesstoken) {
        this.accesstoken = accesstoken;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }
}
