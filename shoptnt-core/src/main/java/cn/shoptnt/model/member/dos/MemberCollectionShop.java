/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;


import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Min;

/**
 * 会员收藏店铺表实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-18 14:23:58
 */
@TableName(value = "es_member_collection_shop")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberCollectionShop implements Serializable {

    private static final long serialVersionUID = 1827422941070835L;

    /**
     * 收藏id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;
    /**
     * 会员id
     */
    @Min(message = "必须为数字", value = 0)
    @Schema(name = "member_id", description = "会员id")
    private Long memberId;
    /**
     * 店铺id
     */
    @Schema(name = "shop_id", description = "店铺id")
    private Long shopId;
    /**
     * 店铺名称
     */
    @Schema(name = "shop_name", description = "店铺名称")
    private String shopName;
    /**
     * 收藏时间
     */
    @Schema(name = "create_time", description = "收藏时间")
    private Long createTime;
    /**
     * 店铺logo
     */
    @Schema(name = "logo", description = "店铺logo")
    private String logo;
    /**
     * 店铺所在省
     */
    @Schema(name = "shop_province", description = "店铺所在省")
    private String shopProvince;
    /**
     * 店铺所在市
     */
    @Schema(name = "shop_city", description =  "店铺所在市")
    private String shopCity;
    /**
     * 店铺所在县
     */
    @Schema(name = "shop_region", description =  "店铺所在县")
    private String shopRegion;
    /**
     * 店铺所在镇
     */
    @Schema(name = "shop_town", description =  "店铺所在镇")
    private String shopTown;
    /**
     * 店铺好评率
     */
    @Schema(name = "shop_praise_rate", description =  "店铺好评率")
    private Double shopPraiseRate;
    /**
     * 店铺描述相符度
     */
    @Schema(name = "shop_description_credit", description =  "店铺描述相符度")
    private Double shopDescriptionCredit;
    /**
     * 服务态度分数
     */
    @Schema(name = "shop_service_credit", description =  "服务态度分数")
    private Double shopServiceCredit;
    /**
     * 发货速度分数
     */
    @Schema(name = "shop_delivery_credit", description =  "发货速度分数")
    private Double shopDeliveryCredit;

    @PrimaryKeyField
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getShopProvince() {
        return shopProvince;
    }

    public void setShopProvince(String shopProvince) {
        this.shopProvince = shopProvince;
    }

    public String getShopCity() {
        return shopCity;
    }

    public void setShopCity(String shopCity) {
        this.shopCity = shopCity;
    }

    public String getShopRegion() {
        return shopRegion;
    }

    public void setShopRegion(String shopRegion) {
        this.shopRegion = shopRegion;
    }

    public String getShopTown() {
        return shopTown;
    }

    public void setShopTown(String shopTown) {
        this.shopTown = shopTown;
    }

    public Double getShopPraiseRate() {
        return shopPraiseRate;
    }

    public void setShopPraiseRate(Double shopPraiseRate) {
        this.shopPraiseRate = shopPraiseRate;
    }

    public Double getShopDescriptionCredit() {
        return shopDescriptionCredit;
    }

    public void setShopDescriptionCredit(Double shopDescriptionCredit) {
        this.shopDescriptionCredit = shopDescriptionCredit;
    }

    public Double getShopServiceCredit() {
        return shopServiceCredit;
    }

    public void setShopServiceCredit(Double shopServiceCredit) {
        this.shopServiceCredit = shopServiceCredit;
    }

    public Double getShopDeliveryCredit() {
        return shopDeliveryCredit;
    }

    public void setShopDeliveryCredit(Double shopDeliveryCredit) {
        this.shopDeliveryCredit = shopDeliveryCredit;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MemberCollectionShop that = (MemberCollectionShop) o;
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (memberId != null ? !memberId.equals(that.memberId) : that.memberId != null) {
            return false;
        }
        if (shopId != null ? !shopId.equals(that.shopId) : that.shopId != null) {
            return false;
        }
        if (shopName != null ? !shopName.equals(that.shopName) : that.shopName != null) {
            return false;
        }
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) {
            return false;
        }
        if (logo != null ? !logo.equals(that.logo) : that.logo != null) {
            return false;
        }
        if (shopProvince != null ? !shopProvince.equals(that.shopProvince) : that.shopProvince != null) {
            return false;
        }
        if (shopCity != null ? !shopCity.equals(that.shopCity) : that.shopCity != null) {
            return false;
        }
        if (shopRegion != null ? !shopRegion.equals(that.shopRegion) : that.shopRegion != null) {
            return false;
        }
        if (shopTown != null ? !shopTown.equals(that.shopTown) : that.shopTown != null) {
            return false;
        }
        if (shopPraiseRate != null ? !shopPraiseRate.equals(that.shopPraiseRate) : that.shopPraiseRate != null) {
            return false;
        }
        if (shopDescriptionCredit != null ? !shopDescriptionCredit.equals(that.shopDescriptionCredit) : that.shopDescriptionCredit != null) {
            return false;
        }
        if (shopServiceCredit != null ? !shopServiceCredit.equals(that.shopServiceCredit) : that.shopServiceCredit != null) {
            return false;
        }
        return shopDeliveryCredit != null ? shopDeliveryCredit.equals(that.shopDeliveryCredit) : that.shopDeliveryCredit == null;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (memberId != null ? memberId.hashCode() : 0);
        result = 31 * result + (shopId != null ? shopId.hashCode() : 0);
        result = 31 * result + (shopName != null ? shopName.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (logo != null ? logo.hashCode() : 0);
        result = 31 * result + (shopProvince != null ? shopProvince.hashCode() : 0);
        result = 31 * result + (shopCity != null ? shopCity.hashCode() : 0);
        result = 31 * result + (shopRegion != null ? shopRegion.hashCode() : 0);
        result = 31 * result + (shopTown != null ? shopTown.hashCode() : 0);
        result = 31 * result + (shopPraiseRate != null ? shopPraiseRate.hashCode() : 0);
        result = 31 * result + (shopDescriptionCredit != null ? shopDescriptionCredit.hashCode() : 0);
        result = 31 * result + (shopServiceCredit != null ? shopServiceCredit.hashCode() : 0);
        result = 31 * result + (shopDeliveryCredit != null ? shopDeliveryCredit.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MemberCollectionShop{" +
                "id=" + id +
                ", memberId=" + memberId +
                ", shopId=" + shopId +
                ", shopName='" + shopName + '\'' +
                ", createTime=" + createTime +
                ", logo='" + logo + '\'' +
                ", shopProvince='" + shopProvince + '\'' +
                ", shopCity='" + shopCity + '\'' +
                ", shopRegion='" + shopRegion + '\'' +
                ", shopTown='" + shopTown + '\'' +
                ", shopPraiseRate=" + shopPraiseRate +
                ", shopDescriptionCredit=" + shopDescriptionCredit +
                ", shopServiceCredit=" + shopServiceCredit +
                ", shopDeliveryCredit=" + shopDeliveryCredit +
                '}';
    }


}
