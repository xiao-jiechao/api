/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zjp
 * @version v7.0
 * @Description 信任登录DO
 * @ClassName ConnectDO
 * @since v7.0 下午2:43 2018/6/20
 */
@TableName(value = "es_connect")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ConnectDO  implements Serializable {

    /**Id*/
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long id;

    /**
     * 会员ID
     */
    @Schema(name = "member_id",description = "会员id")
    private Long memberId;
    /**
     * 唯一标示union_id
     */
    @Schema(name = "union_id", description = "唯一标示")
    private String unionId;
    /**
     * 信任登录类型
     */
    @Schema(name = "union_type", description = "信任登录类型")
    private String unionType;
    /**
     * 解绑时间
     */
    @Schema(name = "unbound_time",description = "解绑时间")
    private Long unboundTime;

    public ConnectDO(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getUnionType() {
        return unionType;
    }

    public void setUnionType(String unionType) {
        this.unionType = unionType;
    }

    public Long getUnboundTime() {
        return unboundTime;
    }

    public void setUnboundTime(Long unboundTime) {
        this.unboundTime = unboundTime;
    }

    @Override
    public String toString() {
        return "ConnectDO{" +
                "id=" + id +
                ", memberId=" + memberId +
                ", unionId='" + unionId + '\'' +
                ", unionType='" + unionType + '\'' +
                ", unboundTime=" + unboundTime +
                '}';
    }
}
