/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;


/**
 * 评论图片实体
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-03 14:11:46
 */
@TableName(value =  "es_comment_gallery")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CommentGallery implements Serializable {

    private static final long serialVersionUID = 8048552718399969L;

    /**主键*/
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden=true)
    private Long imgId;
    /**主键*/
    @Schema(name="comment_id",description = "主键")
    private Long commentId;
    /**图片路径*/
    @Schema(name="original",description = "图片路径")
    private String original;
    /**排序*/
    @Schema(name="sort",description = "排序")
    private Integer sort;

    public Long getImgId() {
        return imgId;
    }
    public void setImgId(Long imgId) {
        this.imgId = imgId;
    }

    public Long getCommentId() {
        return commentId;
    }
    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getOriginal() {
        return original;
    }
    public void setOriginal(String original) {
        this.original = original;
    }

    public Integer getSort() {
        return sort;
    }
    public void setSort(Integer sort) {
        this.sort = sort;
    }


    @Override
    public String toString() {
        return "CommentGallery{" +
                "imgId=" + imgId +
                ", commentId=" + commentId +
                ", original='" + original + '\'' +
                ", sort=" + sort +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommentGallery that = (CommentGallery) o;

        return new EqualsBuilder()
                .append(imgId, that.imgId)
                .append(commentId, that.commentId)
                .append(original, that.original)
                .append(sort, that.sort)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(imgId)
                .append(commentId)
                .append(original)
                .append(sort)
                .toHashCode();
    }
}
