/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.vo;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Min;

/**
 *
 * 会员积分查询
 * @author zh
 * @version v70
 * @since v7.0
 * 2018-04-004 15:44:12
 */
public class MemberPointVO {

    /**
     * 等级积分
     */
    @Min(message="必须为数字", value = 0)
    @Schema(name="grade_point",description="等级积分")
    private Long gradePoint;

    /**消费积分*/
    @Min(message="必须为数字", value = 0)
    @Schema(name="consum_point",description="消费积分")
    private Long consumPoint;

    public Long getGradePoint() {
        return gradePoint;
    }

    public void setGradePoint(Long gradePoint) {
        this.gradePoint = gradePoint;
    }

    public Long getConsumPoint() {
        return consumPoint;
    }

    public void setConsumPoint(Long consumPoint) {
        this.consumPoint = consumPoint;
    }

    @Override
    public String toString() {
        return "MemberPointVO{" +
                "gradePoint=" + gradePoint +
                ", consumPoint=" + consumPoint +
                '}';
    }
}
