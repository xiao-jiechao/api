/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;


/**
 * 会员足迹DTO
 *
 * @author zh
 * @version v7.1.4
 * @since vv7.1
 * 2019-06-18 15:18:56
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class HistoryDTO implements Serializable, DirectMessage {

    public HistoryDTO() {

    }

    public HistoryDTO(Long goodsId, Long memberId) {
        this.goodsId = goodsId;
        this.memberId = memberId;
    }

    private static final long serialVersionUID = 5890914765373676945L;
    /**
     * 商品id
     */
    @Schema(name = "goods_id",description =  "商品id")
    private Long goodsId;
    /**
     * 会员id
     */
    @Schema(name = "member_id",description =  "商品名称", required = true)
    private Long memberId;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    @Override
    public String toString() {
        return "HistoryDTO{" +
                "goodsId=" + goodsId +
                ", memberId=" + memberId +
                '}';
    }

    @Override
    public String getExchange() {
        return AmqpExchange.MEMBER_HISTORY;
    }
}
