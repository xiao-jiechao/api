/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.orderbill.enums;

/**
 * @author fk
 * @version v1.0
 * @Description: 结算单项类型
 * @date 2018/4/26 15:47
 * @since v7.0.0
 */
public enum BillType {

    /**
     * 退款
     */
    REFUND,
    /**
     * 收款
     */
    PAYMENT

}
