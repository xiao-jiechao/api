/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.pagedata.vo;

import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import cn.shoptnt.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;


/**
 * 文章实体
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-06-12 10:43:18
 */
@Table(name = "es_article")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ArticleVO implements Serializable {

    private static final long serialVersionUID = 5105404520203401L;

    /**
     * 主键
     */
    @Id(name = "article_id")
    @Schema(hidden = true)
    private Long articleId;
    /**
     * 文章名称
     */
    @Column(name = "article_name")
    @Schema(name = "article_name", description = "文章名称")
    private String articleName;

    /**
     * 分类id
     */
    @Column(name = "category_id")
    @Schema(name = "category_id", description = "分类id")
    private Long categoryId;

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "ArticleVO{" +
                "articleId=" + articleId +
                ", articleName='" + articleName + '\'' +
                ", categoryId=" + categoryId +
                '}';
    }
}
