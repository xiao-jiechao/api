/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.vo;

import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.validation.annotation.SafeDomain;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 申请开店第二步VO
 *
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 下午3:42:23
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ApplyStep2VO {
	/**
	 * 法人姓名
	 */
	@Column(name = "legal_name")
	@Schema(name = "legal_name", description =  "法人姓名", required = true)
	@NotEmpty(message = "法人姓名必填")
	private String legalName;
	/**
	 * 法人身份证
	 */
	@Column(name = "legal_id")
	@Schema(name = "legal_id", description =  "法人身份证", required = true)
	private String legalId;
	/**
	 * 法人身份证照片
	 */
	@SafeDomain
	@Column(name = "legal_img")
	@Schema(name = "legal_img", description =  "法人身份证照片", required = true)
	@NotEmpty(message = "法人身份证照片必填")
	private String legalImg;
	/**
	 * 营业执照号
	 */
	@Column(name = "license_num")
	@Schema(name = "license_num", description =  "营业执照号", required = true)
	@NotEmpty(message = "营业执照号必填")
	private String licenseNum;
	/**
	 * 营业执照所在省id
	 */
	@Column(name = "license_province_id")
	@Schema(name = "license_province_id", description =  "营业执照所在省id", hidden = true)
	private Long licenseProvinceId;
	/**
	 * 营业执照所在市id
	 */
	@Column(name = "license_city_id")
	@Schema(name = "license_city_id", description =  "营业执照所在市id", hidden = true)
	private Long licenseCityId;
	/**
	 * 营业执照所在县id
	 */
	@Column(name = "license_county_id")
	@Schema(name = "license_county_id", description =  "营业执照所在县id", hidden = true)
	private Long licenseCountyId;
	/**
	 * 营业执照所在镇id
	 */
	@Column(name = "license_town_id")
	@Schema(name = "license_town_id", description =  "营业执照所在镇id", hidden = true)
	private Long licenseTownId;
	/**
	 * 营业执照所在省
	 */
	@Column(name = "license_province")
	@Schema(name = "license_province", description =  "营业执照所在省", hidden = true)
	private String licenseProvince;
	/**
	 * 营业执照所在市
	 */
	@Column(name = "license_city")
	@Schema(name = "license_city", description =  "营业执照所在市", hidden = true)
	private String licenseCity;
	/**
	 * 营业执照所在县
	 */
	@Column(name = "license_county")
	@Schema(name = "license_county", description =  "营业执照所在县", hidden = true)
	private String licenseCounty;
	/**
	 * 营业执照所在镇
	 */
	@Column(name = "license_town",allowNullUpdate = true)
	@Schema(name = "license_town", description =  "营业执照所在镇", hidden = true)
	private String licenseTown;
	/**
	 * 营业执照详细地址
	 */
	@Column(name = "license_add")
	@Schema(name = "license_add", description =  "营业执照详细地址", required = true)
	@NotEmpty(message = "营业执照详细地址必填")
	private String licenseAdd;
	/**
	 * 成立日期
	 */
	@Column(name = "establish_date")
	@Schema(name = "establish_date", description =  "成立日期", required = true)
	@NotNull(message = "成立日期必填")
	private Long establishDate;
	/**
	 * 营业执照有效期开始
	 */
	@Column(name = "licence_start")
	@Schema(name = "licence_start", description =  "营业执照有效期开始", required = true)
	@NotNull(message = "营业执照有效期开始时间必填")
	private Long licenceStart;
	/**
	 * 营业执照有效期结束
	 */
	@Column(name = "licence_end")
	@Schema(name = "licence_end", description =  "营业执照有效期结束", required = true)
	private Long licenceEnd;
	/**
	 * 法定经营范围
	 */
	@Column(name = "scope")
	@Schema(name = "scope", description =  "法定经营范围", required = true)
	@NotEmpty(message = "法定经营范围必填")
	private String scope;
	/**
	 * 营业执照电子版
	 */
	@SafeDomain
	@Column(name = "licence_img")
	@Schema(name = "licence_img", description =  "营业执照电子版", required = true)
	@NotEmpty(message = "营业执照电子版必填")
	private String licenceImg;
	/**
	 * 组织机构代码
	 */
	@Column(name = "organization_code")
	@Schema(name = "organization_code", description =  "组织机构代码")
	private String organizationCode;
	/**
	 * 组织机构电子版
	 */
	@SafeDomain
	@Column(name = "code_img")
	@Schema(name = "code_img", description =  "组织机构电子版")
	private String codeImg;
	/**
	 * 一般纳税人证明电子版
	 */
	@SafeDomain
	@Column(name = "taxes_img")
	@Schema(name = "taxes_img", description =  "一般纳税人证明电子版")
	private String taxesImg;
	/**
	 * 申请开店进度
	 */
	@Column(name = "step")
	@Schema(name = "step", description =  "申请开店进度：1,2,3,4", hidden = true)
	private Integer step;

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getLegalId() {
		return legalId;
	}

	public void setLegalId(String legalId) {
		this.legalId = legalId;
	}

	public String getLegalImg() {
		return legalImg;
	}

	public void setLegalImg(String legalImg) {
		this.legalImg = legalImg;
	}

	public String getLicenseNum() {
		return licenseNum;
	}

	public void setLicenseNum(String licenseNum) {
		this.licenseNum = licenseNum;
	}

	public Long getLicenseProvinceId() {
		return licenseProvinceId;
	}

	public void setLicenseProvinceId(Long licenseProvinceId) {
		this.licenseProvinceId = licenseProvinceId;
	}

	public Long getLicenseCityId() {
		return licenseCityId;
	}

	public void setLicenseCityId(Long licenseCityId) {
		this.licenseCityId = licenseCityId;
	}

	public Long getLicenseCountyId() {
		return licenseCountyId;
	}

	public void setLicenseCountyId(Long licenseCountyId) {
		this.licenseCountyId = licenseCountyId;
	}

	public Long getLicenseTownId() {
		return licenseTownId;
	}

	public void setLicenseTownId(Long licenseTownId) {
		this.licenseTownId = licenseTownId;
	}

	public String getLicenseProvince() {
		return licenseProvince;
	}

	public void setLicenseProvince(String licenseProvince) {
		this.licenseProvince = licenseProvince;
	}

	public String getLicenseCity() {
		return licenseCity;
	}

	public void setLicenseCity(String licenseCity) {
		this.licenseCity = licenseCity;
	}

	public String getLicenseCounty() {
		return licenseCounty;
	}

	public void setLicenseCounty(String licenseCounty) {
		this.licenseCounty = licenseCounty;
	}

	public String getLicenseTown() {
		return licenseTown;
	}

	public void setLicenseTown(String licenseTown) {
		this.licenseTown = licenseTown;
	}

	public String getLicenseAdd() {
		return licenseAdd;
	}

	public void setLicenseAdd(String licenseAdd) {
		this.licenseAdd = licenseAdd;
	}

	public Long getEstablishDate() {
		return establishDate;
	}

	public void setEstablishDate(Long establishDate) {
		this.establishDate = establishDate;
	}

	public Long getLicenceStart() {
		return licenceStart;
	}

	public void setLicenceStart(Long licenceStart) {
		this.licenceStart = licenceStart;
	}

	public Long getLicenceEnd() {
		return licenceEnd;
	}

	public void setLicenceEnd(Long licenceEnd) {
		this.licenceEnd = licenceEnd;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getLicenceImg() {
		return licenceImg;
	}

	public void setLicenceImg(String licenceImg) {
		this.licenceImg = licenceImg;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getCodeImg() {
		return codeImg;
	}

	public void setCodeImg(String codeImg) {
		this.codeImg = codeImg;
	}

	public String getTaxesImg() {
		return taxesImg;
	}

	public void setTaxesImg(String taxesImg) {
		this.taxesImg = taxesImg;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	@Override
	public String toString() {
		return "ApplyStep2VO{" +
				"legalName='" + legalName + '\'' +
				", legalId='" + legalId + '\'' +
				", legalImg='" + legalImg + '\'' +
				", licenseNum='" + licenseNum + '\'' +
				", licenseProvinceId=" + licenseProvinceId +
				", licenseCityId=" + licenseCityId +
				", licenseCountyId=" + licenseCountyId +
				", licenseTownId=" + licenseTownId +
				", licenseProvince='" + licenseProvince + '\'' +
				", licenseCity='" + licenseCity + '\'' +
				", licenseCounty='" + licenseCounty + '\'' +
				", licenseTown='" + licenseTown + '\'' +
				", licenseAdd='" + licenseAdd + '\'' +
				", establishDate=" + establishDate +
				", licenceStart=" + licenceStart +
				", licenceEnd=" + licenceEnd +
				", scope='" + scope + '\'' +
				", licenceImg='" + licenceImg + '\'' +
				", organizationCode='" + organizationCode + '\'' +
				", codeImg='" + codeImg + '\'' +
				", taxesImg='" + taxesImg + '\'' +
				", step=" + step +
				'}';
	}
}
