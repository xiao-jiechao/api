/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.vo;

import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.text.DecimalFormat;

/**
 * 店铺vo
 *
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 上午10:32:52
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ShopInfoVO {

    /**
     * 店铺Id
     */
    @Schema(name = "shop_id", description =  "店铺Id")
    private Long shopId;

    /**
     * 店铺logo
     */
    @Schema(name = "shop_logo", description =  "店铺logo")
    private String shopLogo;
    /**
     * 店铺名称
     */
    @Schema(name = "shop_name", description =  "店铺名称", required = true)
    @NotEmpty(message = "店铺名称必填")
    private String shopName;


    /**
     * 法人身份证
     */
    @Schema(name = "legal_id", description =  "法人身份证", required = true)
    private String legalId;

    /**
     * 店铺详细地址
     */
    @Schema(name = "shop_add", description =  "店铺详细地址")
    private String shopAdd;


    /**
     * 联系人电话
     */
    @Schema(name = "link_phone", description =  "联系人电话", required = true)
    @NotEmpty(message = "联系人电话必填")
    private String linkPhone;


    /**
     * 店铺客服qq
     */
    @Schema(name = "shop_qq", description =  "店铺客服qq")
    private String shopQq;

    /**
     * 店铺简介
     */
    @Schema(name = "shop_desc", description =  "店铺简介")
    private String shopDesc;


    /**
     * 是否自营
     */
   @Schema(name = "self_operated", description =  "是否自营 1:是 0:否", required = true)
    private Integer selfOperated;


    /**
     * 店铺信用
     */
    @Schema(name = "shop_credit", description =  "店铺信用")
    private Double shopCredit;


    /**
     * 店铺所在省
     */
    @Schema(name = "shop_province", description =  "店铺所在省")
    private String shopProvince;
    /**
     * 店铺所在市
     */
    @Schema(name = "shop_city", description =  "店铺所在市")
    private String shopCity;
    /**
     * 店铺所在县
     */
    @Schema(name = "shop_county", description =  "店铺所在县")
    private String shopCounty;
    /**
     * 店铺所在镇
     */
    @Schema(name = "shop_town", description =  "店铺所在镇")
    private String shopTown;


    /**
     * 店铺所在省id
     */
    @Schema(name = "shop_province_id", description =  "店铺所在省id")
    private Long shopProvinceId;
    /**
     * 店铺所在市id
     */
    @Schema(name = "shop_city_id", description =  "店铺所在市id")
    private Long shopCityId;
    /**
     * 店铺所在县id
     */
    @Schema(name = "shop_county_id", description =  "店铺所在县id")
    private Long shopCountyId;
    /**
     * 店铺所在镇id
     */
    @Schema(name = "shop_town_id", description =  "店铺所在镇id")
    private Long shopTownId;

    /**
     * 店铺描述相符度
     */
    @Schema(name = "shop_description_credit", description =  "店铺描述相符度")
    private Double shopDescriptionCredit;


    /**
     * 服务态度分数
     */
    @Schema(name = "shop_service_credit", description =  "服务态度分数")
    private Double shopServiceCredit;

    /**
     * 发货速度分数
     */
    @Schema(name = "shop_delivery_credit", description =  "发货速度分数")
    private Double shopDeliveryCredit;


    /**
     * 货品预警数
     */
    @Schema(name = "goods_warning_count", description =  "货品预警数")
    private Integer goodsWarningCount;

    /**
     * 是否允许开具增值税普通发票 0：否，1：是
     */
    @Schema(name = "ordin_receipt_status", description =  "是否允许开具增值税普通发票 0：否，1：是", allowableValues = "0,1")
    private Integer ordinReceiptStatus;
    /**
     * 是否允许开具电子普通发票 0：否，1：是
     */
   @Schema(name = "elec_receipt_status", description =  "是否允许开具电子普通发票 0：否，1：是", allowableValues = "0,1")
    private Integer elecReceiptStatus;
    /**
     * 是否允许开具增值税专用发票 0：否，1：是
     */
    @Schema(name = "tax_receipt_status", description =  "是否允许开具增值税专用发票 0：否，1：是", allowableValues = "0,1")
    private Integer taxReceiptStatus;


    public ShopInfoVO(ShopVO shopVO) {
        this.setGoodsWarningCount(shopVO.getGoodsWarningCount());
        this.setLegalId(shopVO.getLegalId());
        this.setLinkPhone(shopVO.getLinkPhone());
        this.setSelfOperated(shopVO.getSelfOperated());
        this.setShopAdd(shopVO.getShopAdd());
        this.setShopCity(shopVO.getShopCity());
        this.setShopCityId(shopVO.getShopCityId());
        this.setShopCounty(shopVO.getShopCounty());
        this.setShopCountyId(shopVO.getShopCountyId());
        this.setShopCredit(shopVO.getShopCredit());
        this.setShopDeliveryCredit(shopVO.getShopDeliveryCredit());
        this.setShopDesc(shopVO.getShopDesc());
        this.setShopDescriptionCredit(shopVO.getShopDescriptionCredit());
        this.setShopId(shopVO.getShopId());
        this.setShopLogo(shopVO.getShopLogo());
        this.setShopName(shopVO.getShopName());
        this.setShopProvince(shopVO.getShopProvince());
        this.setShopProvinceId(shopVO.getShopProvinceId());
        this.setShopQq(shopVO.getShopQq());
        this.setShopServiceCredit(shopVO.getShopServiceCredit());
        this.setShopTown(shopVO.getShopTown());
        this.setShopTownId(shopVO.getShopTownId());
        this.setOrdinReceiptStatus(shopVO.getOrdinReceiptStatus());
        this.setElecReceiptStatus(shopVO.getElecReceiptStatus());
        this.setTaxReceiptStatus(shopVO.getTaxReceiptStatus());

    }

    public ShopInfoVO() {

    }


    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopLogo() {
        return shopLogo;
    }

    public void setShopLogo(String shopLogo) {
        this.shopLogo = shopLogo;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getLegalId() {
        return legalId;
    }

    public void setLegalId(String legalId) {
        this.legalId = legalId;
    }

    public String getShopAdd() {
        return shopAdd;
    }

    public void setShopAdd(String shopAdd) {
        this.shopAdd = shopAdd;
    }

    public String getLinkPhone() {
        return linkPhone;
    }

    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
    }

    public String getShopQq() {
        return shopQq;
    }

    public void setShopQq(String shopQq) {
        this.shopQq = shopQq;
    }

    public String getShopDesc() {
        return shopDesc;
    }

    public void setShopDesc(String shopDesc) {
        this.shopDesc = shopDesc;
    }

    public Integer getSelfOperated() {
        return selfOperated;
    }

    public void setSelfOperated(Integer selfOperated) {
        this.selfOperated = selfOperated;
    }

    public Double getShopCredit() {
        return shopCredit;
    }

    public void setShopCredit(Double shopCredit) {
        this.shopCredit = shopCredit;
    }

    public String getShopProvince() {
        return shopProvince;
    }

    public void setShopProvince(String shopProvince) {
        this.shopProvince = shopProvince;
    }

    public String getShopCity() {
        return shopCity;
    }

    public void setShopCity(String shopCity) {
        this.shopCity = shopCity;
    }

    public String getShopCounty() {
        return shopCounty;
    }

    public void setShopCounty(String shopCounty) {
        this.shopCounty = shopCounty;
    }

    public String getShopTown() {
        return shopTown;
    }

    public void setShopTown(String shopTown) {
        this.shopTown = shopTown;
    }

    public Long getShopProvinceId() {
        return shopProvinceId;
    }

    public void setShopProvinceId(Long shopProvinceId) {
        this.shopProvinceId = shopProvinceId;
    }

    public Long getShopCityId() {
        return shopCityId;
    }

    public void setShopCityId(Long shopCityId) {
        this.shopCityId = shopCityId;
    }

    public Long getShopCountyId() {
        return shopCountyId;
    }

    public void setShopCountyId(Long shopCountyId) {
        this.shopCountyId = shopCountyId;
    }

    public Long getShopTownId() {
        return shopTownId;
    }

    public void setShopTownId(Long shopTownId) {
        this.shopTownId = shopTownId;
    }

    public Double getShopDescriptionCredit() {
        return shopDescriptionCredit;
    }

    public void setShopDescriptionCredit(Double shopDescriptionCredit) {
        this.shopDescriptionCredit = shopDescriptionCredit;
    }

    public Double getShopServiceCredit() {
        return shopServiceCredit;
    }

    public void setShopDeliveryCredit(Double shopDeliveryCredit) {
        this.shopDeliveryCredit = shopDeliveryCredit;
    }

    public Integer getGoodsWarningCount() {
        return goodsWarningCount;
    }

    public void setGoodsWarningCount(Integer goodsWarningCount) {
        this.goodsWarningCount = goodsWarningCount;
    }

    public String getShopDescriptionCreditText() {
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        return decimalFormat.format(shopDescriptionCredit == null ? 0 : shopDescriptionCredit);
    }

    public String getShopServiceCreditText() {
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        return decimalFormat.format(shopServiceCredit == null ? 0 : shopServiceCredit);
    }

    public void setShopServiceCredit(Double shopServiceCredit) {
        this.shopServiceCredit = shopServiceCredit;
    }

    public Double getShopDeliveryCredit() {
        return shopDeliveryCredit;
    }

    public String getShopDeliveryCreditText() {
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        return decimalFormat.format(shopDeliveryCredit == null ? 0 : shopDeliveryCredit);
    }

    public Integer getOrdinReceiptStatus() {
        return ordinReceiptStatus;
    }

    public void setOrdinReceiptStatus(Integer ordinReceiptStatus) {
        this.ordinReceiptStatus = ordinReceiptStatus;
    }

    public Integer getElecReceiptStatus() {
        return elecReceiptStatus;
    }

    public void setElecReceiptStatus(Integer elecReceiptStatus) {
        this.elecReceiptStatus = elecReceiptStatus;
    }

    public Integer getTaxReceiptStatus() {
        return taxReceiptStatus;
    }

    public void setTaxReceiptStatus(Integer taxReceiptStatus) {
        this.taxReceiptStatus = taxReceiptStatus;
    }
}
