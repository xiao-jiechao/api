/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.distribution.vo;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Arrays;
import java.util.Objects;

/**
 * 提现申请审核和设置已转账参数实体
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 */
public class WithdrawAuditPaidVO {
    @Schema(name = "apply_ids", description = "提现申请id组", required = true)
    private Long[] applyIds;

    @Schema(name = "remark", description = "审核或转账备注")
    private String remark;

    public Long[] getApplyIds() {
        return applyIds;
    }

    public void setApplyIds(Long[] applyIds) {
        this.applyIds = applyIds;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WithdrawAuditPaidVO that = (WithdrawAuditPaidVO) o;
        return Arrays.equals(applyIds, that.applyIds) &&
                Objects.equals(remark, that.remark);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(remark);
        result = 31 * result + Arrays.hashCode(applyIds);
        return result;
    }

    @Override
    public String toString() {
        return "WithdrawAuditPaidVO{" +
                "applyIds=" + Arrays.toString(applyIds) +
                ", remark='" + remark + '\'' +
                '}';
    }
}
