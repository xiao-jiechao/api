/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.vo;

import cn.shoptnt.framework.validation.annotation.Mobile;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


/**
 * 账户安全设置实体
 *
 * @author shen
 * @version v7.0.0
 * @since v7.0.0
 * 2021-11-19 14:27:48
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AccountSetting {
    /**
     * 密码重试累加周期
     */
    @Min(value = 1, message = "密码重试累加周期必须大于0")
    @Schema(name = " retry_cycle", description =  "密码重试累加周期")
    private Integer  retryCycle;
    /**
     * 密码重试次数
     */
    @Min(value = 1, message = "密码重试次数必须大于0")
    @Schema(name = "retry_times", description =  "密码重试次数")
    private Integer retryTimes;
    /**
     * 锁定时长
     */
    @Min(value = 1, message = "锁定时长必须大于0")
    @Schema(name = " lock_duration", description =  "锁定时长")
    private Long  lockDuration;
    /**
     * 休眠门槛
     */
    @Min(value = 1, message = "休眠门槛必须大于0")
    @Schema(name = "sleep_threshold", description =  "休眠门槛")
    private Integer sleepThreshold;

    /**
     * 密码验证
     */
    @Schema(name = "password_auth", description =  "是否开启密码验证,1为开启,0为关闭")
    @Min(message = "必须为数字且,1为开启,0为关闭", value = 0)
    @Max(message = "必须为数字且,1为开启,0为关闭", value = 1)
    @NotNull(message = "是否开启密码验证不能为空")
    private Integer passwordAuth;
    /**
     * 短信验证
     */
    @Schema(name = "message_auth", description =  "是否开启短信验证,1为开启,0为关闭")
    @Min(message = "必须为数字且,1为开启,0为关闭", value = 0)
    @Max(message = "必须为数字且,1为开启,0为关闭", value = 1)
    @NotNull(message = "是否开启短信验证不能为空")
    private Integer messageAuth;

    /**
     * 每天扫描时长
     */
    @Min(value = 1, message = "每天扫描时长必须大于0")
    @Max(message = "每天扫描最大时长不能超过24小时", value = 24)
    @Schema(name = "scanning_time", description =  "每天扫描时长")
    private Integer scanningTime;

    /**
     * 报警手机号
     */
    @Schema(name = "mobile", description =  "报警手机号")
    @Mobile
    private String mobile;

    public Integer getRetryCycle() {
        return retryCycle;
    }

    public void setRetryCycle(Integer retryCycle) {
        this.retryCycle = retryCycle;
    }

    public Integer getRetryTimes() {
        return retryTimes;
    }

    public void setRetryTimes(Integer retryTimes) {
        this.retryTimes = retryTimes;
    }

    public Long getLockDuration() {
        return lockDuration;
    }

    public void setLockDuration(Long lockDuration) {
        this.lockDuration = lockDuration;
    }

    public Integer getSleepThreshold() {
        return sleepThreshold;
    }

    public void setSleepThreshold(Integer sleepThreshold) {
        this.sleepThreshold = sleepThreshold;
    }

    public Integer getPasswordAuth() {
        return passwordAuth;
    }

    public void setPasswordAuth(Integer passwordAuth) {
        this.passwordAuth = passwordAuth;
    }

    public Integer getMessageAuth() {
        return messageAuth;
    }

    public void setMessageAuth(Integer messageAuth) {
        this.messageAuth = messageAuth;
    }

    public Integer getScanningTime() {
        return scanningTime;
    }

    public void setScanningTime(Integer scanningTime) {
        this.scanningTime = scanningTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "AccountSetting{" +
                "retryCycle=" + retryCycle +
                ", retryTimes=" + retryTimes +
                ", lockDuration=" + lockDuration +
                ", sleepThreshold=" + sleepThreshold +
                ", passwordAuth=" + passwordAuth +
                ", messageAuth=" + messageAuth +
                ", scanningTime=" + scanningTime +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
