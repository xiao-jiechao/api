/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.enums;

/**
 * 快递鸟参数枚举
 *
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2019/5/28 下午4:44
 */

public enum KDNParamsEnum {


    /**
     * 以下五个参数为快递鸟所需的枚举值
     */
    customer_name,
    customer_pwd,
    month_code,
    send_site,
    send_staff;

}
