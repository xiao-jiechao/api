/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.vo;

import cn.shoptnt.model.base.vo.ConfigItem;
import cn.shoptnt.model.system.dos.SmsPlatformDO;
import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;


/**
 * 短信网关表实体
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-23 11:31:05
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SmsPlatformVO implements Serializable{
	private static final long serialVersionUID = 1568078050994075591L;
	/**id*/
	@Schema(hidden=true)
	private Long id;
	/**平台名称*/
	@Column(name = "name")
	@NotEmpty(message="平台名称不能为空")
	@Schema(name="name",description = "平台名称",required=true)
	private String name;
	/**是否开启*/
	@Column(name = "open")
	@Min(message="必须为数字", value = 0)
	@Schema(name="open",description = "是否开启")
	private Integer open;
	/**配置*/
	@Column(name = "config")
	@Schema(name="config",description = "配置")
	private String config;
	/**编码*/
	@Column(name = "bean")
	@NotEmpty(message="插件id")
	@Schema(name="bean",description = "编码",required=true)
	private String bean;
	@Schema(name="configItems" ,description = "短信配置项",required=true)
	private List<ConfigItem> configItems;

	public SmsPlatformVO(SmsPlatformDO smsPlatformDO) {
		this.id = smsPlatformDO.getId();
		this.name = smsPlatformDO.getName();
		this.open = smsPlatformDO.getOpen();
		this.config = smsPlatformDO.getConfig();
		this.bean = smsPlatformDO.getBean();
		Gson gson = new Gson();
		this.configItems = gson.fromJson(smsPlatformDO.getConfig(),  new TypeToken< List<ConfigItem> >() {  }.getType());
	}
	public SmsPlatformVO() {

	}


	@Override
	public String toString() {
		return "PlatformVO [id=" + id + ", name=" + name + ", open=" + open + ", config=" + config + ", bean=" + bean
				+ ", configItems=" + configItems + "]";
	}




	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Integer getOpen() {
		return open;
	}
	public void setOpen(Integer open) {
		this.open = open;
	}

	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}

	public String getBean() {
		return bean;
	}
	public void setBean(String bean) {
		this.bean = bean;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public List<ConfigItem> getConfigItems() {
		return configItems;
	}


	public void setConfigItems(List<ConfigItem> configItems) {
		this.configItems = configItems;
	}





}
