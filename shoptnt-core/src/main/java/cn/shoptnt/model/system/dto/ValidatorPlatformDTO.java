/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dto;

import cn.shoptnt.model.system.vo.AliyunAfsVO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 验证平台DTO
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.6
 * 2019-12-18
 */
@Schema(name = "验证平台DTO")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ValidatorPlatformDTO implements Serializable {

    private static final long serialVersionUID = -5875562388711541719L;

    @Schema(description = "验证平台类型", name = "validator_type IMAGE：图片验证码，ALIYUN：阿里云滑动验证")
    private String validatorType;

    @Schema(description = "阿里云滑动验证参数", name = "aliyun_afs")
    private AliyunAfsVO aliyunAfs;

    public String getValidatorType() {
        return validatorType;
    }

    public void setValidatorType(String validatorType) {
        this.validatorType = validatorType;
    }

    public AliyunAfsVO getAliyunAfs() {
        return aliyunAfs;
    }

    public void setAliyunAfs(AliyunAfsVO aliyunAfs) {
        this.aliyunAfs = aliyunAfs;
    }

    @Override
    public String toString() {
        return "ValidatorPlatformDTO{" +
                "validatorType='" + validatorType + '\'' +
                ", aliyunAfs=" + aliyunAfs +
                '}';
    }
}
