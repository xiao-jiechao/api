/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.vo;

import cn.shoptnt.model.base.vo.ConfigItem;
import cn.shoptnt.model.system.dos.ExpressPlatformDO;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.List;


/**
 * 快递平台实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-11 14:42:50
 */

public class ExpressPlatformVO implements Serializable {


    private static final long serialVersionUID = -6909967652948921476L;
    /**
     * 快递平台id
     */
    @Id(name = "id")
    @Schema(hidden = true)
    private Long id;
    /**
     * 快递平台名称
     */
    @Column(name = "name")
    @Schema(name = "name", description = "快递平台名称")
    private String name;
    /**
     * 是否开启快递平台,1开启，0未开启
     */
    @Column(name = "open")
    @Schema(name = "open", description = "是否开启快递平台,1开启，0未开启")
    private Integer open;
    /**
     * 快递平台配置
     */
    @Column(name = "config")
    @Schema(name = "config", description = "快递平台配置")
    private String config;
    /**
     * 快递平台beanid
     */
    @Column(name = "bean")
    @Schema(name = "bean", description = "快递平台beanid")
    private String bean;
    /**
     * 快递平台配置项
     */
    @Schema(name = "configItems", description = "快递平台配置项", required = true)
    private List<ConfigItem> configItems;

    public ExpressPlatformVO(ExpressPlatformDO expressPlatformDO) {
        this.id = expressPlatformDO.getId();
        this.name = expressPlatformDO.getName();
        this.open = expressPlatformDO.getOpen();
        this.config = expressPlatformDO.getConfig();
        this.bean = expressPlatformDO.getBean();
        Gson gson = new Gson();
        this.configItems = gson.fromJson(expressPlatformDO.getConfig(), new TypeToken<List<ConfigItem>>() {
        }.getType());
    }

    public ExpressPlatformVO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOpen() {
        return open;
    }

    public void setOpen(Integer open) {
        this.open = open;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getBean() {
        return bean;
    }

    public void setBean(String bean) {
        this.bean = bean;
    }

    public List<ConfigItem> getConfigItems() {
        return configItems;
    }

    public void setConfigItems(List<ConfigItem> configItems) {
        this.configItems = configItems;
    }

    @Override
    public String toString() {
        return "ExpressPlatformVO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", open=" + open +
                ", config='" + config + '\'' +
                ", bean='" + bean + '\'' +
                ", configItems=" + configItems +
                '}';
    }
}
