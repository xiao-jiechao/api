/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.groupbuy.vo;

import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyGoodsDO;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;

/**
 * 团购商品VO
 *
 * @author Snow create in 2018/6/11
 * @version v2.0
 * @since v7.0.0
 */
@Schema
public class GroupbuyGoodsVO extends GroupbuyGoodsDO {

    @Schema(name = "gb_status_text", description = "审核状态值")
    private String gbStatusText;

    @Schema(name = "start_time", description = "活动开始时间")
    private Long startTime;

    @Schema(name = "end_time", description =  "活动截止时间")
    private Long endTime;

    @Schema(name = "title", description =  "活动标题")
    private String title;

    @Schema(name = "enable_quantity", description =  "可用库存")
    private Integer enableQuantity;

    @Schema(name = "quantity", description =  "库存")
    private Integer quantity;

    @Schema(name = "isEnable", description =  "活动是否在进行中")
    private Integer isEnable;

    @Schema(name="delete_status",description = "是否删除 DELETED：已删除，NORMAL：正常")
    private String deleteStatus;

    @Schema(name="delete_reason",description = "删除原因")
    private String deleteReason;

    @Schema(name="delete_time",description = "删除日期")
    private Long deleteTime;

    @Schema(name="delete_name",description = "删除操作人")
    private String deleteName;

    public String getGbStatusText() {
        return gbStatusText;
    }

    public void setGbStatusText(String gbStatusText) {
        this.gbStatusText = gbStatusText;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getEnableQuantity() {
        return enableQuantity;
    }

    public void setEnableQuantity(Integer enableQuantity) {
        this.enableQuantity = enableQuantity;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }

    public String getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(String deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    public Long getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Long deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getDeleteName() {
        return deleteName;
    }

    public void setDeleteName(String deleteName) {
        this.deleteName = deleteName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        GroupbuyGoodsVO that = (GroupbuyGoodsVO) o;
        return Objects.equals(gbStatusText, that.gbStatusText) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime) &&
                Objects.equals(title, that.title) &&
                Objects.equals(enableQuantity, that.enableQuantity) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(isEnable, that.isEnable) &&
                Objects.equals(deleteStatus, that.deleteStatus) &&
                Objects.equals(deleteReason, that.deleteReason) &&
                Objects.equals(deleteTime, that.deleteTime) &&
                Objects.equals(deleteName, that.deleteName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), gbStatusText, startTime, endTime, title, enableQuantity, quantity, isEnable, deleteStatus, deleteReason, deleteTime, deleteName);
    }

    @Override
    public String toString() {
        return "GroupbuyGoodsVO{" +
                "gbStatusText='" + gbStatusText + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", title='" + title + '\'' +
                ", enableQuantity=" + enableQuantity +
                ", quantity=" + quantity +
                ", isEnable=" + isEnable +
                ", deleteStatus='" + deleteStatus + '\'' +
                ", deleteReason='" + deleteReason + '\'' +
                ", deleteTime=" + deleteTime +
                ", deleteName='" + deleteName + '\'' +
                '}';
    }
}
