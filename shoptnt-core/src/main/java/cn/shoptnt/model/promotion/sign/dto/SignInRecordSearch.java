package cn.shoptnt.model.promotion.sign.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title SignInRecordSearchVO
 * @description 签到记录查询对象
 * @program: api
 * 2024/3/12 14:24
 */
public class SignInRecordSearch implements Serializable {
    private static final long serialVersionUID = 1777456563089971570L;

    @Schema(name = "member_id", description =  "会员id")
    private Long memberId;

    @Schema(name = "sign_activity_id", description =  "活动id")
    private Long signActivityId;


    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getSignActivityId() {
        return signActivityId;
    }

    public void setSignActivityId(Long signActivityId) {
        this.signActivityId = signActivityId;
    }

    @Override
    public String toString() {
        return "SignInRecordSearchVO{" +
                "memberId=" + memberId +
                ", signActivityId=" + signActivityId +
                '}';
    }
}
