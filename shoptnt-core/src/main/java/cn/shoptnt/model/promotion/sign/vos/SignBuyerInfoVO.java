package cn.shoptnt.model.promotion.sign.vos;

import cn.shoptnt.model.promotion.sign.dos.SignInRecord;
import cn.shoptnt.model.promotion.sign.dos.SignInReward;
import cn.shoptnt.model.promotion.sign.dto.SignDay;
import cn.shoptnt.model.promotion.sign.dto.SignGoodsDTO;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title SignBuyerInfoVO
 * @description 会员端展示签到信息
 * @program: api
 * 2024/3/13 14:48
 */
public class SignBuyerInfoVO implements Serializable {
    private static final long serialVersionUID = -5930527488091439538L;

    @Schema(description =  "活动标题")
    private String title;

    @Schema(description =  "活动规则")
    private String description;

    @Schema(description =  "推荐商品")
    private List<SignGoodsDTO> goods;

    @Schema(description =  "签到记录")
    private List<SignInRecord> records;
    @Schema(description =  "签到详细信息")
    private List<SignDay> signDays;
    @Schema(description = "签到奖品")
    private List<SignInReward> rewards;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SignGoodsDTO> getGoods() {
        return goods;
    }

    public void setGoods(List<SignGoodsDTO> goods) {
        this.goods = goods;
    }

    public List<SignInRecord> getRecords() {
        return records;
    }

    public void setRecords(List<SignInRecord> records) {
        this.records = records;
    }

    public List<SignDay> getSignDays() {
        return signDays;
    }

    public void setSignDays(List<SignDay> signDays) {
        this.signDays = signDays;
    }

    public List<SignInReward> getRewards() {
        return rewards;
    }

    public void setRewards(List<SignInReward> rewards) {
        this.rewards = rewards;
    }

    @Override
    public String toString() {
        return "SignBuyerInfoVO{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", goods=" + goods +
                ", records=" + records +
                ", signDays=" + signDays +
                '}';
    }
}
