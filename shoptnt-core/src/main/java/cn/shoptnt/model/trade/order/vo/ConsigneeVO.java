/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.vo;

import cn.shoptnt.model.member.dos.MemberAddress;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * 收货人实体
 * @author 妙贤
 * @version 1.0
 * @created 2017年08月03日14:39:25
 */
@SuppressWarnings("AlibabaPojoMustOverrideToString")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ConsigneeVO implements Serializable{

	private static final long serialVersionUID = 2499675140677613044L;
	@Schema( description = "id")
	private Long consigneeId;

	@Schema( description = "收货人姓名")
	private String name;

	@Schema( description = "省")
	private String province;

	@Schema( description = "市")
	private String city;

	@Schema( description = "区")
	private String county;

	@Schema( description = "街道")
	private String town;

	@Schema( description = "详细地址")
	private String address;

	@Schema( description = "手机号")
	private String mobile;

	@Schema( description = "电话")
	private String telephone;

	@Schema( description = "省ID")
	private Long provinceId;

	@Schema( description = "市ID")
	private Long countyId;

	@Schema( description = "区ID")
	private Long cityId;

	@Schema( description = "街道ID")
	private Long townId;


	public ConsigneeVO() {
	}

	public ConsigneeVO(MemberAddress memberAddress) {

		this.setConsigneeId(memberAddress.getAddrId());
		this.setAddress(memberAddress.getAddr());

		this.setProvince(memberAddress.getProvince());
		this.setCity(memberAddress.getCity());
		this.setCounty(memberAddress.getCounty());
		this.setTown(memberAddress.getTown());

		this.setProvinceId(memberAddress.getProvinceId());
		this.setCityId(memberAddress.getCityId());
		this.setCountyId(memberAddress.getCountyId());
		if (memberAddress.getTownId() != null) {
			this.setTownId(memberAddress.getTownId());
		}
		this.setMobile(memberAddress.getMobile());
		this.setTelephone(memberAddress.getTel());
		this.setName(memberAddress.getName());
	}
	public ConsigneeVO(String name,String mobile){
		this.setName(name);
		this.setName(mobile);
	}

	@Override
	public String toString() {
		return "ConsigneeVO{" +
				"consigneeId=" + consigneeId +
				", name='" + name + '\'' +
				", county='" + county + '\'' +
				", province='" + province + '\'' +
				", city='" + city + '\'' +
				", town='" + town + '\'' +
				", address='" + address + '\'' +
				", mobile='" + mobile + '\'' +
				", telephone='" + telephone + '\'' +
				", countyId=" + countyId +
				", provinceId=" + provinceId +
				", cityId=" + cityId +
				", townId=" + townId +
				'}';
	}

	public Long getConsigneeId() {
		return consigneeId;
	}

	public void setConsigneeId(Long consigneeId) {
		this.consigneeId = consigneeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Long getCountyId() {
		return countyId;
	}

	public void setCountyId(Long countyId) {
		this.countyId = countyId;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getTownId() {
		return townId;
	}

	public void setTownId(Long townId) {
		this.townId = townId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o){
			return true;
		}

		if (o == null || getClass() != o.getClass()){
			return false;
		}

		ConsigneeVO that = (ConsigneeVO) o;

		return new EqualsBuilder()
				.append(consigneeId, that.consigneeId)
				.append(name, that.name)
				.append(county, that.county)
				.append(province, that.province)
				.append(city, that.city)
				.append(town, that.town)
				.append(address, that.address)
				.append(mobile, that.mobile)
				.append(telephone, that.telephone)
				.append(countyId, that.countyId)
				.append(provinceId, that.provinceId)
				.append(cityId, that.cityId)
				.append(townId, that.townId)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(consigneeId)
				.append(name)
				.append(county)
				.append(province)
				.append(city)
				.append(town)
				.append(address)
				.append(mobile)
				.append(telephone)
				.append(countyId)
				.append(provinceId)
				.append(cityId)
				.append(townId)
				.toHashCode();
	}
}
