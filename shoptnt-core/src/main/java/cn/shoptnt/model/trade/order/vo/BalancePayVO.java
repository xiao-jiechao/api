/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.vo;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @description: 预存款支付VO
 * @author: liuyulei
 * @create: 2020-01-01 11:58
 * @version:1.0
 * @since:7.1.5
 **/
@Schema(description = "预存款支付参数")
public class BalancePayVO implements Serializable {
    private static final long serialVersionUID = 974160879230069347L;

    @Schema(description = "订单/交易编号")
    private String sn;
    @Schema(name = "need_pay", description = "待在线支付金额")
    private Double needPay;

    @Schema(name = "balance", description = "预存款余额")
    private Double balance;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Double getNeedPay() {
        return needPay;
    }

    public void setNeedPay(Double needPay) {
        this.needPay = needPay;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "BalancePayVO{" +
                "sn='" + sn + '\'' +
                ", needPay=" + needPay +
                ", balance=" + balance +
                '}';
    }
}
