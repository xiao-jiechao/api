/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.complain.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 交易投诉对象
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2019-11-27 16:48:27
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ComplainDTO implements Serializable {


    /**
     * 投诉主题
     */
    @Schema(name = "complain_topic", description =  "投诉主题", required = true)
    private String complainTopic;
    /**
     * 投诉内容
     */
    @Schema(name = "content", description =  "投诉内容", required = true)
    private String content;

    /**
     * 投诉凭证图片
     */
    @Schema(name = "images", description =  "投诉凭证图片，多图以逗号分隔")
    private String images;

    /**
     * 订单号
     */
    @Schema(name = "order_sn", description =  "订单号", required = true)
    private String orderSn;

    /**
     * skuid
     */
    @Schema(name = "sku_id", description =  "sku主键", required = true)
    private Long skuId;

    public String getComplainTopic() {
        return complainTopic;
    }

    public void setComplainTopic(String complainTopic) {
        this.complainTopic = complainTopic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
}
