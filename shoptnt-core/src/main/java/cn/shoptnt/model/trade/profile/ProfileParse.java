/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.profile;

/**
 * @author ：liuyulei
 * @date ：Created in 2019/9/11 16:13
 * @description：配置文件解析器
 * @version: v1.0
 * @since: v7.1.4
 */
public interface ProfileParse {

    /**
     * 解析配置文件
     * @return
     */
    void parseProfile();
}
