/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.broadcast;

import cn.shoptnt.framework.message.broadcast.BroadcastMessage;
import cn.shoptnt.framework.message.broadcast.BroadcastChannel;
import cn.shoptnt.model.util.sensitiveutil.SensitiveFilter;
import cn.shoptnt.framework.message.broadcast.BroadcastMessageReceiver;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

/**
 * redis敏感词消费者
 *
 * @author fk
 * @version 2.0
 * @since 7.1.5
 * 2019-09-07 18：00
 */
@Component
@ConditionalOnMissingBean(name = "systemServiceApplication")
public class SensitiveReceiver implements BroadcastMessageReceiver {


    @Override
    public String getChannelName() {
        return BroadcastChannel.SENSITIVE_WORDS;
    }

    @Override
    public void receiveMsg(BroadcastMessage message) {
        SensitiveWordsMsg msg = (SensitiveWordsMsg) message;

        //添加操作
        if(SensitiveWordsMsg.ADD.equals(msg.getOperation())){

            SensitiveFilter.put(msg.getWord());
            return;
        }
        //删除操作
        if(SensitiveWordsMsg.DELETE.equals(msg.getOperation())){

            SensitiveFilter.remove(msg.getWord());
            return;
        }
    }
}
