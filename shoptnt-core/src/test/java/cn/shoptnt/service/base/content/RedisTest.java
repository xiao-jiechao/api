/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.base.content;

import cn.shoptnt.service.goods.impl.util.StockCacheKeyUtil;
import cn.shoptnt.framework.redis.configure.JedisSetting;
import cn.shoptnt.framework.redis.configure.RedisConnectionConfig;
import cn.shoptnt.framework.redis.configure.builders.RedisStandaloneBuilder;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.scripting.ScriptSource;
import org.springframework.scripting.support.ResourceScriptSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 妙贤 on 2019-01-11.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2019-01-11
 */
@SuppressWarnings("Duplicates")
public class RedisTest {


    private  StringRedisTemplate getRedisTemplate(){

        RedisConnectionConfig config = new RedisConnectionConfig();

        JedisSetting.loadPoolConfig(config);
        RedisStandaloneBuilder standaloneBuilder = new RedisStandaloneBuilder();

        config.setConfigType("manual");
        config.setType("standalone");
        config.setHost("192.168.2.5");
        config.setPassword("");
        config.setPort(6379);

        RedisConnectionFactory lettuceConnectionFactory = standaloneBuilder.buildConnectionFactory(config);

        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setConnectionFactory(lettuceConnectionFactory);
        redisTemplate.afterPropertiesSet();

        return redisTemplate;
    }

    @Test
    public void testGet() {
        StringRedisTemplate redisTemplate = this.getRedisTemplate();
        List keys = new ArrayList<>();
        Long[] ar = new Long[]{1366l, 1371l, 1387l};

        int j=0;
        for (int i = 0; i < ar.length; i++) {
            redisTemplate.opsForValue().set("test_"+ StockCacheKeyUtil.skuEnableKey(ar[i]),""+j);
            j++;
            redisTemplate.opsForValue().set("test_"+ StockCacheKeyUtil.skuActualKey(ar[i]),""+j);
            j++;
        }
        for (int i = 0; i < ar.length; i++) {
            keys.add("test_"+StockCacheKeyUtil.skuEnableKey(ar[i]));
            keys.add("test_"+StockCacheKeyUtil.skuActualKey(ar[i]));

        }

        List values = redisTemplate.opsForValue().multiGet(keys);
        

          j = 0;
        for (int i = 0; i < ar.length; i++) {
            
            j++;
            
            j++;
        }
    }

    @Test
    public void test1() throws IOException {
        StringRedisTemplate redisTemplate = this.getRedisTemplate();
        ScriptSource scriptSource = new ResourceScriptSource(new ClassPathResource("test.lua"));
        String str =scriptSource.getScriptAsString();

        RedisScript<Boolean> redisScript = RedisScript.of(str, Boolean.class);
        List keys = new ArrayList<>();
        List values =  new ArrayList<>();
        Boolean result  = redisTemplate.execute(redisScript,keys,values.toArray());
        
        String value = redisTemplate.opsForValue().get("mytest");
        


    }

    @Test
    public void test() throws IOException {
        StringRedisTemplate redisTemplate = this.getRedisTemplate();
        ScriptSource scriptSource = new ResourceScriptSource(new ClassPathResource("sku_quantity.lua"));
        String str =scriptSource.getScriptAsString();

        RedisScript<Boolean> redisScript = RedisScript.of(str, Boolean.class);

        List keys = new ArrayList<>();
        List values =  new ArrayList<>();


        //初始化库存，2个商品，每个商品10个库存
        for (int i = 1; i <=20; i++){
//            redisTemplate.opsForValue().set("sku_quantity_"+i,""+10);
            keys.add("sku_quantity_"+i);
            //每商品减1
            values.add("-1");
        }

        Boolean result  = redisTemplate.execute(redisScript,keys,values.toArray());

        if (!result) {
            
        }

        view(redisTemplate);
    }

    private  void view( StringRedisTemplate redisTemplate ){

        for (int i = 1; i <=20; i++){
            String value  = redisTemplate.opsForValue().get("sku_quantity_"+i );
            

        }
    }


}
