/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.order;

import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.model.trade.order.dos.OrderLogDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 订单日志业务层测试
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/08/10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class OrderLogManagerTest {

    @Autowired
    private OrderLogManager orderLogManager;

    @Test
    public void list() {

        
    }

    @Test
    public void listAll() {

        
    }

    @Test
    public void add() {

        OrderLogDO orderLog = new OrderLogDO();
        orderLog.setOrderSn("xxxxxxxxxxxxxxxxxxxx");

        orderLogManager.add(orderLog);
    }

    @Test
    public void edit() {

        OrderLogDO orderLog = new OrderLogDO();
        orderLog.setOrderSn("xxxxxxxxxxxxxxxxxxxx222");

        orderLogManager.edit(orderLog, 1l);
    }

    @Test
    public void delete() {

        orderLogManager.delete(2l);
    }

    @Test
    public void getModel() {

        

    }


}
