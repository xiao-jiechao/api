/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 商家中心，商品收藏统计业务层测试
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/07/31
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class CollectFrontStatisticsManagerTest {

    @Autowired
    private CollectFrontStatisticsManager collectFrontStatisticsManager;

    @Test
    public void getChart() {

        SimpleChart chart = collectFrontStatisticsManager.getChart(1l);

        
    }

    @Test
    public void getPage() {

        WebPage page = collectFrontStatisticsManager.getPage(1l, 3l, 1l);

        
    }

}
